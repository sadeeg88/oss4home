//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.bc;

import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import de.oss4home.bc.business.ControllerBusinessUnit;
import de.oss4home.ws.ControllerVersionWS;
import de.oss4home.ws.DevicesWS;
import de.oss4home.util.OSS4HomeResourceConfig;
import de.oss4home.util.Webserver;



public class ApplicationBC {

	private static final URI BASE_URI = URI.create("http://0.0.0.0:5550/");

	
    /*
     * Main Funktion des 1-Wire-Controllers
     */
	public static void main(String[] args) {
		
		boolean stop = false;
		
		
		/*
		 * Configuration Init
		 */
		
		/*
		 * Thread zu abruf der Daten der Daten
		 */
		
			ControllerBusinessUnit.getInstance().configSync();
		
		

		
	
		
		/*Webserver Konfigurieren und Starten*/
		Webserver webserver = new Webserver(BASE_URI, initWebservice());
		try {
			webserver.start();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		/*Gewollte Endlosschleife*/
		while(!stop)
		{
			try {
				ControllerBusinessUnit.getInstance().sync();
				Thread.sleep(5);
			} catch (InterruptedException e) {
			
			}
	
		}
		


	}
	
	/*
	 * Konfiguration des Webservers
	 * Hinzufügen der einzelnen Webservices
	 * */
	private static OSS4HomeResourceConfig initWebservice()
	{
		Set<Class<?>> webservices = new HashSet<Class<?>>();
		webservices.add(ControllerVersionWS.class);
		webservices.add(DevicesWS.class);
		return OSS4HomeResourceConfig.init(webservices);
	}

}
