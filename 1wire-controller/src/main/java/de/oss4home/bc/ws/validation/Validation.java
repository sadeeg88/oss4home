//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.bc.ws.validation;

import de.oss4home.bc.business.ControllerBusinessUnit;
import de.oss4home.bc.dto.BcDeviceConfigDTO;
import de.oss4home.bc.dto.BcDeviceValueDTO;
import de.oss4home.cc.dto.DeviceTyp;

public class Validation {
	
	public boolean isUpdateDeviceValueValid(BcDeviceValueDTO value)
	{
		BcDeviceConfigDTO config = ControllerBusinessUnit.getInstance().getDeviceConfig(value.getId());
		if(config != null)
		{
			if(config.getType() == DeviceTyp.RELAIS
			    && config.getActive() == true)
			{
				return true;
			}
			
		}
		return false;
		
	}
	
	public boolean isUpdateDeviceConfigValid(BcDeviceConfigDTO newconfig)
	{
		BcDeviceConfigDTO config = ControllerBusinessUnit.getInstance().getDeviceConfig(newconfig.getId());
		if(config != null)
		{
			if((config.getType() == DeviceTyp.RELAIS
			    || config.getType() == DeviceTyp.SWITCH)
					&&
					(newconfig.getType() == DeviceTyp.RELAIS
				    || newconfig.getType() == DeviceTyp.SWITCH))
			{
				return true;
			}
			else if(config.getType() == DeviceTyp.SENSOR
					&& newconfig.getType() == DeviceTyp.SENSOR)
				
			{
				return true;
			}
			
		}
		return false;
		
	}

}
