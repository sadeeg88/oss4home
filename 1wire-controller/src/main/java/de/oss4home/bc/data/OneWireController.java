//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.bc.data;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.Level;

import com.dalsemi.onewire.OneWireAccessProvider;
import com.dalsemi.onewire.OneWireException;
import com.dalsemi.onewire.adapter.DSPortAdapter;
import com.dalsemi.onewire.adapter.OneWireIOException;
import com.dalsemi.onewire.container.*;

import de.oss4home.bc.Interfaces.IDeviceConfig;
import de.oss4home.bc.Interfaces.IDeviceValue;
import de.oss4home.bc.business.ControllerBusinessUnit;
import de.oss4home.bc.dto.BcDeviceConfigDTO;
import de.oss4home.bc.dto.BcDeviceValueDTO;
import de.oss4home.cc.dto.DeviceTyp;

/*
 * Ermitteln und Weiterverarbeiten der Daten des 1-Wire-Systems
 */
public class OneWireController {

	private IDeviceConfig configContainer = null;
	private IDeviceValue valueContainer = null;
	private DSPortAdapter adapter;
	private Calendar senRelaiLastwork = null ;

	public OneWireController(IDeviceValue valueContainer, IDeviceConfig configContainer) {
		this.configContainer = configContainer;
		this.valueContainer = valueContainer;

		boolean ok = true;

		try {
			adapter = OneWireAccessProvider.getDefaultAdapter();
			adapter.setSpeed(3);
		} catch (OneWireIOException e) {
			ok = false;
		} catch (OneWireException e) {
			ok = false;
		}

		if (!ok) {
			try {
				@SuppressWarnings("rawtypes")
				Class adapter_class = Class.forName("com.dalsemi.onewire.adapter.PDKAdapterUSB");
				adapter = (DSPortAdapter) adapter_class.newInstance();
				adapter.selectPort("USB1");

			} catch (java.lang.NoClassDefFoundError e) {
				System.err.println("Warning: Could not load PDKAdapter: " + e);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	/**
	 * Daten Syncronisierung bzw Ermittlung der aktuellen der Werte
	 */
	public void sync() {
		readValues();
	}

	/*
	 * Pr�ft anhand des Objektes ob es sich um einen Switch handelt
	 */
	private boolean isSwitch(Object curObj) {
		return curObj instanceof OneWireContainer05 | curObj instanceof OneWireContainer12
				| curObj instanceof OneWireContainer1C | curObj instanceof OneWireContainer1F
				| curObj instanceof OneWireContainer29 | curObj instanceof OneWireContainer3A;
	}

	/*
	 * Pr�ft anhand des Objektes ob es sich um einen Sensor handelt
	 */
	private boolean isSensor(Object curObj) {
		return curObj instanceof OneWireContainer21 | curObj instanceof OneWireContainer41
				| curObj instanceof OneWireContainer10 | curObj instanceof OneWireContainer30
				| curObj instanceof OneWireContainer28 | curObj instanceof OneWireContainer22
				| curObj instanceof OneWireContainer26;
	}

	/**
	 * Ermittelt die Adresse des Device
	 * 
	 * @param sensor
	 * @return
	 */
	private String getAdress(OneWireSensor sensor) {
		String[] split = sensor.toString().split(" ");
		if (split.length == 2) {
			return split[0];
		}
		return "";
	}

	/**
	 * Ermittelt den Chip des Device
	 * 
	 * @param sensor
	 * @return
	 */
	private String getChipName(OneWireSensor sensor) {
		String[] split = sensor.toString().split(" ");
		if (split.length == 2) {
			return split[1];
		}
		return "";
	}

	/**
	 * Ermittelt �ber die Harware den Wert des �bergebenen device
	 * 
	 * @param device
	 * @throws OneWireIOException
	 * @throws OneWireException
	 */

	private void readValue(BcDeviceConfigDTO device) throws OneWireIOException, OneWireException {

		if (device.getType() == DeviceTyp.RELAIS || device.getType() == DeviceTyp.SWITCH) {
			String id = "";
			int channel = -1;
			String[] data = device.getId().split("#");
			id = data[0];
			if (data.length == 2) {
				channel = Integer.parseInt(data[1]);
			}
			Object curObj;
			synchronized (DSPortAdapter.class) {
				curObj = adapter.getDeviceContainer(id);
			}
			if (isSwitch(curObj)) {
				byte[] devicedata;
				SwitchContainer switchCon = (SwitchContainer) curObj;
				synchronized (DSPortAdapter.class) {
					devicedata = switchCon.readDevice();
				}

				BcDeviceValueDTO value = new BcDeviceValueDTO();
				value.setId(device.getId());
				if (device.getType() == DeviceTyp.RELAIS)
					value.setValue(switchCon.getLatchState(channel, devicedata) ? "OFF" : "ON");
				else
					value.setValue(switchCon.getLevel(channel, devicedata) ? "OFF" : "ON");
				valueContainer.addUpdateValue(value);
			}
		} else if (device.getType() == DeviceTyp.SENSOR) {
			Object curObj = adapter.getDeviceContainer(device.getId());
			if (isSensor(curObj)) {
				byte[] devicedata;
				TemperatureContainer switchCon = (TemperatureContainer) curObj;
				synchronized (DSPortAdapter.class) {
					devicedata = switchCon.readDevice();
				}
				switchCon.doTemperatureConvert(devicedata);
				BcDeviceValueDTO value = new BcDeviceValueDTO();
				value.setId(device.getId());
				value.setValue(new Double(switchCon.getTemperature(devicedata)).toString());
				valueContainer.addUpdateValue(value);

			}

		}

	}

	/**
	 * Ermittelt den Wert aller Devices
	 */
	public void readValues() {
		boolean sensorsRelaisAb = false;
		if(senRelaiLastwork == null)
		{
			senRelaiLastwork = Calendar.getInstance();
			sensorsRelaisAb = true;
		}
		else if((Calendar.getInstance().getTimeInMillis() - senRelaiLastwork.getTimeInMillis()) > 60000
				||(Calendar.getInstance().getTimeInMillis() - senRelaiLastwork.getTimeInMillis()) < 0)
		{
			senRelaiLastwork = Calendar.getInstance();
			sensorsRelaisAb = true;
			
		}
		
		
		
		
		
		for (BcDeviceConfigDTO device : configContainer.getAllDevicesConfig()) {
			if((device.getType() == DeviceTyp.SWITCH || sensorsRelaisAb))
			{
			if (device.getActive() && device.getConnected()) {
				try {
					readValue(device);
				} catch (OneWireIOException e) {
					BcDeviceValueDTO value = new BcDeviceValueDTO();
					value.setId(device.getId());
					value.setValue("N/A");
					valueContainer.addUpdateValue(value);
					ControllerBusinessUnit.getInstance().getLogger().log(Level.ERROR, e);
				} catch (OneWireException e) {
					BcDeviceValueDTO value = new BcDeviceValueDTO();
					value.setId(device.getId());
					value.setValue("N/A");
					valueContainer.addUpdateValue(value);
					ControllerBusinessUnit.getInstance().getLogger().log(Level.ERROR, e);
				}
			} else
				valueContainer.deleteValue(device.getId());
			}
		}

	}

	public void setValue(BcDeviceValueDTO value) throws OneWireIOException, OneWireException {
		Boolean setValue = value.getValue().equals("OFF");
		String id = "";
		int channel = -1;
		String[] data = value.getId().split("#");
		id = data[0];
		if (data.length == 2) {
			channel = Integer.parseInt(data[1]);
		}
		Object curObj;
		synchronized (DSPortAdapter.class) {
			curObj = adapter.getDeviceContainer(id);
		}
		if (isSwitch(curObj)) {
			SwitchContainer switchCon = (SwitchContainer) curObj;
			byte[] devicedata;
			synchronized (DSPortAdapter.class) {
				devicedata = switchCon.readDevice();
			}
			switchCon.setLatchState(channel, setValue, false, devicedata);
			synchronized (DSPortAdapter.class) {
				switchCon.writeDevice(devicedata);
			}
			readValue(configContainer.getDeviceConfig(value.getId()));
		}

	}

	/**
	 * Ermittelt alle angeschlossenen Devices und deren Konfiguration
	 */
	public void deviceKonfiguration() {
		synchronized (DSPortAdapter.class) {
			try {

				List<BcDeviceConfigDTO> curDevices = configContainer.getAllDevicesConfig();

				// adapter.targetFamily(05);
				ControllerBusinessUnit.getInstance().getLogger().log(Level.INFO, "Adapter: " + adapter.getAdapterName() + " Port: " + adapter.getPortName());
				// adapter.
				for (@SuppressWarnings("unchecked")
				Enumeration<Object> devices = adapter.getAllDeviceContainers(); devices.hasMoreElements();) {
					Object curObj = devices.nextElement();

					// Switch
					if (isSwitch(curObj)) {
						SwitchContainer switchCon = (SwitchContainer) curObj;
						byte[] data = switchCon.readDevice();
						if (switchCon.hasLevelSensing()) {
							for (int x = 0; x < switchCon.getNumberChannels(data); x++) {
								boolean found = false;
								Iterator<BcDeviceConfigDTO> iter = curDevices.iterator();
								while (iter.hasNext()) {
									BcDeviceConfigDTO cur = (BcDeviceConfigDTO) iter.next();
									if (cur.getId().equals(getAdress(switchCon) + "#" + x)) {
										found = true;
										// iter.remove();
									}
								}
								if (!found) {
									BcDeviceConfigDTO curConfig = new BcDeviceConfigDTO();
									curConfig.setId(getAdress(switchCon) + "#" + x);
									curConfig.setActive(false);
									curConfig.setType(DeviceTyp.RELAIS);
									// curConfig.setActivitySensing(switchCon.hasActivitySensing());
									curConfig.setConnected(true);
									configContainer.addUpdateDeviceConfig(curConfig);

								}
							}
						}
					} else if (isSensor(curObj)) {
						boolean found = false;
						TemperatureContainer temp = (TemperatureContainer) curObj;
						byte[] data = temp.readDevice();
						if(temp.hasSelectableTemperatureResolution())
						{
							temp.setTemperatureResolution(0.5, data);
						}
						temp.writeDevice(data);

						Iterator<BcDeviceConfigDTO> iter = curDevices.iterator();
						while (iter.hasNext()) {
							BcDeviceConfigDTO cur = (BcDeviceConfigDTO) iter.next();
							if (cur.getId().equals(getAdress(temp))) {
								found = true;
								// iter.remove();
							}
						}
						if (!found) {
							BcDeviceConfigDTO curConfig = new BcDeviceConfigDTO();
							curConfig.setId(getAdress(temp));
							curConfig.setActive(true);
							curConfig.setType(DeviceTyp.SENSOR);
							curConfig.setConnected(true);
							configContainer.addUpdateDeviceConfig(curConfig);
							
						}
					}
					

				}

			} catch (OneWireIOException e) {
				e.printStackTrace();
			} catch (OneWireException e) {
				e.printStackTrace();
			}
		}

	}

	public void initDevices() {

		Iterator<BcDeviceConfigDTO> iter = configContainer.getAllDevicesConfig().iterator();
		while (iter.hasNext()) {
			BcDeviceConfigDTO cur = (BcDeviceConfigDTO) iter.next();
			initDevice(cur);
		}

	}

	public void initDevice(BcDeviceConfigDTO device) {
		if (device.getType() == DeviceTyp.SWITCH) {
			BcDeviceValueDTO value = new BcDeviceValueDTO();
			value.setId(device.getId());
			value.setValue("OFF");
			try {
				setValue(value);
			} catch (Exception e) {
				// TODO Logs
			}
		}
	}

}
