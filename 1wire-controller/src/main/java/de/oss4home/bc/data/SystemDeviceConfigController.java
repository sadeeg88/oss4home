//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.bc.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.xml.bind.annotation.*;

import de.oss4home.bc.Interfaces.IDeviceConfig;
import de.oss4home.bc.dto.BcDeviceConfigDTO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SystemDeviceConfigController implements IDeviceConfig,Serializable {
	
	

	private static final long serialVersionUID = 329892001172824983L;
	
		
	@XmlElement
	private List<BcDeviceConfigDTO> devices = new ArrayList<BcDeviceConfigDTO>();
	
	private SystemDeviceConfigController()
	{
		
	}
	
	
	public static SystemDeviceConfigController loadFromFile()
	{
		SystemDeviceConfigController instance = null;
		if(instance == null)
		{
			synchronized (SystemDeviceConfigController.class) {
				try {
					instance = load();
				} catch (FileNotFoundException e) {
	
					e.printStackTrace();
				} catch (JAXBException e) {

				}
				if(instance == null)
					instance = new SystemDeviceConfigController();
				
			}
		}
		return instance;
	}

	public List<BcDeviceConfigDTO> getDevices() {
		//Damit die Orginal Liste nicht ver�ndert werden kann
		//sondern nur die Werte der Elemente
		return new Vector<BcDeviceConfigDTO>(devices);
	}
	
	
	
	public void save() throws FileNotFoundException, JAXBException
	{
		OutputStream out = new FileOutputStream("SystemDeviceInfoController.xml"); 
		OutputStreamWriter writer = new OutputStreamWriter(out);
		JAXBContext context = JAXBContext.newInstance(SystemDeviceConfigController.class);
		Marshaller m = context.createMarshaller();
		m.marshal(this, writer);
	}
	
	
	private static SystemDeviceConfigController load() throws JAXBException, FileNotFoundException
	{
		InputStream in = new FileInputStream("SystemDeviceInfoController.xml"); 
		InputStreamReader input = new InputStreamReader(in);
		JAXBContext context = JAXBContext.newInstance(SystemDeviceConfigController.class);
		Unmarshaller m = context.createUnmarshaller();
		return (SystemDeviceConfigController)m.unmarshal(input);
		
	}


	public void addUpdateDeviceConfig(BcDeviceConfigDTO config) {
		//Gibt es das Element schon, dann nur Updaten
		BcDeviceConfigDTO deviceThis = getDeviceConfig(config.getId());
		if(deviceThis  != null) 
		{
			//Nur wenn es nicht das gleiche Object ist
			if(deviceThis != config)
			{
				try {
					deviceThis.setActive(config.getActive());
					deviceThis.setType(config.getType());
				} catch (Exception e) {
					// TODO Auto-generated catch block
				}
			}
		}
		else
		{
			devices.add(config);
		}
		try {
			save();
		} catch (FileNotFoundException e) {
			// TODO Logging
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Logging
		}
		
		
	}


	public List<BcDeviceConfigDTO> getAllDevicesConfig() {
		//Damit die Orginal Liste nicht ver�ndert werden kann
		//sondern nur die Werte der Elemente
		return new Vector<BcDeviceConfigDTO>(devices);
	}


	public BcDeviceConfigDTO getDeviceConfig(String Id) {
		Iterator<BcDeviceConfigDTO> iter = devices.iterator();
		while(iter.hasNext())
		{
			BcDeviceConfigDTO cur = (BcDeviceConfigDTO) iter.next();
			 if(cur.getId().equals(Id))
				 return cur;
		}
		return null;
	}

	

}
