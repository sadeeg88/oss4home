import { OSS4HomeAppPage } from './app.po';

describe('oss4-home-app App', () => {
  let page: OSS4HomeAppPage;

  beforeEach(() => {
    page = new OSS4HomeAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
