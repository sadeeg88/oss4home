
	



/**
 * Generelle Anwendungsübergreifende Funtktionen
 */

var cont = angular.module('SMKSmartSystemApp', ['WebserviceCC', 'Route', 'ngRoute', 'BusinessLogic','frapontillo.bootstrap-switch']);


/**
 * Navigation
 */

cont.controller('nav', function ($scope, $http, $location) {
    $scope.action = function (path) {
        $('#bs-example-navbar-collapse-1').collapse("hide");
        $location.path(path);
        
    }
});

/**
Controller für MainSite
*/
cont.controller('Main', function ($scope, $http, $location, SensorenBusiness, RelaisBusiness, GroupBusiness, SwitchBusiness) {
    SensorenBusiness.getValues(function (data, errorMsg) {
        $scope.Sensoren = data.data;
        if (errorMsg != '')
            $scope.SensorenError = errorMsg;
    });

   SwitchBusiness.getValues(function (data, errorMsg) {
        $scope.Switches = data.data;
        if (errorMsg != '')
            $scope.SwitchesError = errorMsg;
    });

    RelaisBusiness.getValues(function (data, errorMsg) {
        $scope.Relais = data.data;
        if (errorMsg != '')
        $scope.RelaisError = errorMsg;
    });

    GroupBusiness.getValues(function (data, errorMsg) {
        $scope.Groups = data.data;
        if (errorMsg != '')
        $scope.GroupsError = errorMsg;
    });
});


/**
Controller für ControllRelais
*/
cont.controller('ControllRelais', function ($scope, $http, $location, RelaisBusiness) {
    $scope.loadData = function () {
        RelaisBusiness.getValues(function (data, errorMsg) {
            $scope.Relais = data.data;
            if (errorMsg != '')
                $scope.RelaisError = errorMsg;
            if (angular.isArray(data)) {
                data.forEach(function (value, i, ar) {

                });
            }



        });
    };

    $scope.loadData();

    $scope.changeSwitch = function (obj) {
        RelaisBusiness.changeValue(obj, function (ok, message) {
            if (!ok) {
                BootstrapDialog.alert({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'ERROR',
                    message: message
                });
            }
       
                $scope.loadData();
        });
    }


    


});


cont.controller('ControllGroups', function ($scope, $http, $location, GroupBusiness) {

    $scope.loadData = function () {
        GroupBusiness.getValues(function (data, errorMsg) {
            $scope.Groups = data.data;
            if (errorMsg == '')
                $scope.GroupsError = errorMsg;
        });
    };

    $scope.loadData();

        $scope.change = function (group) {
            GroupBusiness.changeValue(group, function (ok, message) {
                if (!ok) {
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'ERROR',
                        message: message
                    });
                }
               
               $scope.loadData();
            });

    };








});


/**
cont.controller('Sensoren', function($scope, $http){
    $http.get(webservice+'/Sensors').then(function(sdata) {
        $scope.sensoren = sdata.data;
      });
    });*/

cont.controller('ControllerSettings', function ($scope, $http, $location, ControllerBusiness) {

    $scope.load = function () {
        ControllerBusiness.getControllers(function (sdata, errorMsg) {

            $scope.controllers = sdata.data;
            if (errorMsg != '')
                $scope.controllersError = errorMsg;



        });

    };
    $scope.load();

    $scope.add = function() {
        ControllerBusiness.setCurController(null);
        $location.path('/AddController');
    };
    
    $scope.change = function(id) {
    	for (x in $scope.controllers)
        {
    		var controller =  $scope.controllers[x];
    		if(controller.id ==   id)
    		{
    		    ControllerBusiness.setCurController(controller);
    		}
    	}
    	$location.path('/AddController');
    };
    
    $scope.del = function (id) {

        var handling = function (Success, ErrorMsg) {
            if (Success == false) {
                BootstrapDialog.alert({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'ERROR',
                    message: ErrorMsg
                });
            }
            else {
                $scope.load();
            }
        }

        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_WARNING,
            message: 'Wollen Sie den Controller wirklich löschen?',
            buttons: [{
                icon: 'glyphicon glyphicon-remove',
                label: 'Delete',
                cssClass: 'btn-danger',
                action: function (dialogItself) {
                    ControllerBusiness.delete(id, handling);
                    dialogItself.close();
                }},
            {
                label: 'Cancel',
                cssClass: 'btn-primary',
                action: function (dialogItself) {
                    dialogItself.close();
                }
            }]
        });
    	    	

    	

    	
    	
    };
    
    });


cont.controller('AddChangeController', function ($scope, $http, $location, ControllerBusiness) {
    var data = ControllerBusiness.getCurController();
	
	// Wenn es sich um einen Änderungsdialog handelt
    // Description und funktion überschreiben
    if(data != null )
    {
        $scope.cur_controller = data;
		
        $scope.buttonACConntroller = 'Change'; 
       
			
    }
    else
    {
        $scope.buttonACConntroller = 'Add'; 
		
        $scope.cur_controller = JSON.parse('{"id" : "" , "connectionString" : "", "name" : "" , "active" : false}');

    }

    $scope.AddChangeControllerToWS = function (valid) {
        var data = $scope.cur_controller;
        var handling = function (Success, ErrorMsg) {
            if (Success == false) {
                BootstrapDialog.alert({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'ERROR',
                    message: ErrorMsg
                });
            }
            else {
                $location.path('/SystemSettings');
            }
        }
        if (valid) {
            ControllerBusiness.addupdate(data, handling);
        }
        else {
            var message = "Bitte füllen Sie die Felder mit korrekten Werten!"
            BootstrapDialog.alert({
                type: BootstrapDialog.TYPE_DANGER,
                title: 'ERROR',
                message: message
            })
        }
		
    };
	
    
    $scope.Cancel =  function() {
        $location.path('/SystemSettings');
    };
    
    
    
});

cont.controller('DevicesConfig', function ($scope, $http, $location, DevicesBusiness) {
    //Webservice Laden

    DevicesBusiness.getConfig(function (sdata, errorMsg) {
        $scope.DevicesConfig = sdata.data;
        if (errorMsg != '')
            $scope.DeviceConfigError = errorMsg;
    });

    $scope.update = function (obj) {
     //   alert('sada');
        DevicesBusiness.setCurDevice(obj);
        $location.path('/UpdateDevicesConfig');
    };

});

cont.controller('UpdateDevicesConfig', function ($scope,$route, $http, $location, DevicesBusiness) {
    //Webservice Laden

    $scope.cur_device = DevicesBusiness.getCurDevice();

    if ($scope.cur_device.type == 'SENSOR')
    {
        $scope.options = JSON.parse('["SENSOR"]');
    }
    else
    {
        $scope.options = JSON.parse('["SWITCH","RELAIS"]');
        $scope.cur_device.calculationFormula = 'x'
        $scope.cur_device.unit = 'none'
    }

    $scope.update = function (valid) {
        if (valid) {
            var data = $scope.cur_device;
            var handling = function (Success, ErrorMsg) {
                if (Success == false) {
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'ERROR',
                        message: ErrorMsg
                    });
                }
                else {
                    $route.reload();
                    $location.path('/DevicesConfig');
                }
            }
            DevicesBusiness.setConfig(data, handling);
        }
        else {
            var message = "Bitte füllen Sie die Felder mit korrekten Werten!"
            BootstrapDialog.alert({
                type: BootstrapDialog.TYPE_DANGER,
                title: 'ERROR',
                message: message
            })
        }
        
    };

    $scope.cancel = function (obj) {
        $location.path('/DevicesConfig');
    };

});

cont.controller('Settings', function ($scope, $http, $location, SystemBusiness) {

    SystemBusiness.getSettings(function (sdata, errorMsg) {
        $scope.settings = sdata.data;
    });

    $scope.saveChanges = function (valid) {
        if (valid) {
            SystemBusiness.saveChanges($scope.settings, function (ok, message) {
                if (!ok) {
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'ERROR',
                        message: message
                    })
                }
                else {
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_SUCCESS,
                        title: 'SUCCESS',
                        message: 'Daten erfolgreich gespeichert!'
                    })
                }
            });
        }
        else {
            var message = "Bitte füllen Sie die Felder mit korrekten Werten!"
            BootstrapDialog.alert({
                type: BootstrapDialog.TYPE_DANGER,
                title: 'ERROR',
                message: message
            })
        }

    };


});

cont.controller('AutoView', function ($scope, $http, $location, AutoBusiness) {
    $scope.load = function () {
        AutoBusiness.get(function (sdata, errorMsg) {
            $scope.autos = sdata.data;
        });
    }

    $scope.load();

    $scope.change = function (obj) {
        BootstrapDialog.alert({
            type: BootstrapDialog.TYPE_DANGER,
            title: 'ERROR',
            message: obj
        });
    };

    $scope.add = function ()
    {
        var emptyRule = JSON.parse('{"id" : "" , "name" : "" , "active" : true , "trigger" : [], "actions" : []}');
        AutoBusiness.setCurAutoRule(emptyRule);
        $location.path('/AddChangeAuto');
    }

    $scope.change = function (obj) {
        AutoBusiness.setCurAutoRule(obj);
        $location.path('/AddChangeAuto');
    }

    $scope.changeValue = function (obj) {
        AutoBusiness.saveChanges(obj, function (ok, message) {
            if (!ok) {
                BootstrapDialog.alert({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'ERROR',
                    message: message
                })
            }
        });
     
    }


    $scope.del = function (id) {

                BootstrapDialog.show({
            type: BootstrapDialog.TYPE_WARNING,
            message: 'Wollen Sie den Regel wirklich löschen?',
            buttons: [{
                icon: 'glyphicon glyphicon-remove',
                label: 'Delete',
                cssClass: 'btn-danger',
                action: function (dialogItself) {
                    AutoBusiness.del(id, function (ok, message) {
                        if (!ok) {
                            BootstrapDialog.alert({
                                type: BootstrapDialog.TYPE_DANGER,
                                title: 'ERROR',
                                message: message
                            })
                        }
                        else {
                            $scope.load();
                        }
                    });
                    dialogItself.close();
                }},
            {
                label: 'Cancel',
                cssClass: 'btn-primary',
                action: function (dialogItself) {
                    dialogItself.close();
                }
            }]
        });

   
    };


});



cont.controller('AddChangeAuto', function ($scope, $http, $location, AutoBusiness) {
    $scope.newGroupActions = new Array(0);
    $scope.newDeviceActions = new Array(0);


    $scope.auto = AutoBusiness.getCurAutoRule();
    AutoBusiness.getTrigger(function (sdata, errorMsg) {
        $scope.newTrigger = sdata;
    });
    
    AutoBusiness.getMailTemplates(function (sdata, errorMsg) {
        $scope.mailTemplates = sdata.data;
    });

    $scope.fusion = function()
    {
        $scope.newActions = $scope.newDeviceActions.concat($scope.newGroupActions);
    }

    AutoBusiness.getActions(function (sdata, errorMsg) {
        $scope.newDeviceActions = sdata;
        $scope.fusion();
    });

    AutoBusiness.getActionsGroups(function (sdata, errorMsg) {
        $scope.newGroupActions = sdata;
        $scope.fusion();
    });

    $scope.sensorOperator = JSON.parse('["<>","<",">","="]');
    $scope.switchRelaiValue = JSON.parse('["ON","OFF","TOGGLE"]');

    if ($scope.auto.id == "")
        $scope.u1 = "Regel erstellen";
    else
        $scope.u1 = "Regel ändern";

    $scope.addTrigger = function ()
    {
        if (typeof $scope.selectedTrigger != 'undefined') {
            // $scope.auto.trigger.length = auto.trigger.length + 1;
            $scope.auto.trigger.push(jQuery.extend(true,{}, $scope.selectedTrigger));
       
        }

    }
    $scope.delTrigger= function (i)
    {
        $scope.auto.trigger.splice(i, 1);
    }

    $scope.addAction = function () {
        if (typeof $scope.selectedAction != 'undefined') {
            // $scope.auto.trigger.length = auto.trigger.length + 1;
            $scope.auto.actions.push(jQuery.extend(true, {}, $scope.selectedAction));

        }

    }
    $scope.delAction= function (i) {
        $scope.auto.actions.splice(i, 1);
    }

    $scope.save = function (valid) {
        var message = "Bitte füllen Sie die Felder mit korrekten Werten!"
        if($scope.auto.actions.length == 0
            || $scope.auto.trigger.length == 0)
        {
            valid = false;
            message = "Es muss mindestens ein Trigger und eine Action angelegt sein!\r\n" + message;
        }
        
        if (valid) {
            AutoBusiness.saveChanges($scope.auto, function (ok, message) {
                if (!ok) {
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'ERROR',
                        message: message
                    });
                }
                else
                    $location.path('/AutoView');
            });
        }
        else
        {
            BootstrapDialog.alert({
                type: BootstrapDialog.TYPE_DANGER,
                title: 'ERROR',
                message: message
            })
        }

    }

    $scope.cancel = function () {
        $location.path('/AutoView');
    }


});



cont.controller('GroupsSetting', function ($scope, $http, $location, GroupBusiness) {
    
    $scope.load = function () {
        GroupBusiness.getConfig(function (sdata, errorMsg) {
            $scope.groups = sdata.data;
        });
    }
    $scope.load();

    $scope.add = function () {
        var emptyRule = JSON.parse('{"id" : "" , "name" : "" , "devices" : []}');
        GroupBusiness.setCurGroup(emptyRule);
        $location.path('/AddChangeGroup');
    }

    $scope.change = function (obj) {
        GroupBusiness.setCurGroup(obj);
        $location.path('/AddChangeGroup');
    }

    $scope.del = function (id) {

        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_WARNING,
            message: 'Wollen Sie den Regel wirklich löschen?',
            buttons: [{
                icon: 'glyphicon glyphicon-remove',
                label: 'Delete',
                cssClass: 'btn-danger',
                action: function (dialogItself) {
                    GroupBusiness.del(id, function (ok, message) {
                        if (!ok) {
                            BootstrapDialog.alert({
                                type: BootstrapDialog.TYPE_DANGER,
                                title: 'ERROR',
                                message: message
                            })
                        }
                        else {
                            $scope.load();
                        }
                    });
                    dialogItself.close();
                }
            },
            {
                label: 'Cancel',
                cssClass: 'btn-primary',
                action: function (dialogItself) {
                    dialogItself.close();
                }
            }]
        });


    };

   

    


}); 

cont.controller('EventLog', function ($scope, $http, $location, AutoBusiness) {

    AutoBusiness.getEventlog(function (sdata, errorMsg) {
        $scope.data = sdata.data;
    });

});



cont.controller('AddChangeGroup', function ($scope, $http, $location, GroupBusiness) {

    $scope.group = GroupBusiness.getCurGroup();
    GroupBusiness.getAvabileDevices(function (sdata, errorMsg) {
        $scope.newDevices = sdata;
    },$scope.group.devices);


    if ($scope.group.id == "")
        $scope.u1 = "Gruppe erstellen";
    else
        $scope.u1 = "Gruppe ändern";

    $scope.addDevice = function () {
        if (typeof $scope.selectedDevice != 'undefined') {
            // $scope.auto.trigger.length = auto.trigger.length + 1;
            $scope.group.devices.push(jQuery.extend(true, {}, $scope.selectedDevice));
            $scope.newDevices.forEach(function 
                    (element, index, array) {
                if ($scope.selectedDevice == element)
                {
                    $scope.newDevices.splice(element, 1);
                }
            });

        }

    }
    $scope.delDevice = function (i) {
        $scope.newDevices.push(jQuery.extend(true, {}, $scope.group.devices[i]))
        $scope.group.devices.splice(i, 1);
    }

    

    $scope.save = function (valid) {
        var message = "Bitte füllen Sie die Felder mit korrekten Werten!"
        if (valid) {
            GroupBusiness.setConfig($scope.group, function (ok, message) {
                if (!ok) {
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'ERROR',
                        message: message
                    })
                }
                else {
                    $location.path('/GroupsSetting');
                }

            });
        }
        else {
            var message = "Bitte füllen Sie die Felder mit korrekten Werten!"
            BootstrapDialog.alert({
                type: BootstrapDialog.TYPE_DANGER,
                title: 'ERROR',
                message: message
            })
        }

    }

    $scope.cancel = function () {
        $location.path('/GroupsSetting');
    }


});

cont.controller('MailTemplate', function ($scope, $http, $location, AutoBusiness) {
    $scope.load = function ()
    {
        AutoBusiness.getMailTemplates(function (sdata, errorMsg) {
            $scope.mailTmp = sdata.data;
        });
    };
    $scope.load();

    $scope.add = function () {
        var emptyRule = JSON.parse('{"id" : "" , "name" : "" , "subject" : "" , "body" : "" , "to" :""}');
        AutoBusiness.setCurMailTmp(emptyRule);
        $location.path('/AddChangeMailTmp');
    }

    $scope.change = function (obj) {
        AutoBusiness.setCurMailTmp(obj);
        $location.path('/AddChangeMailTmp');
    }

    $scope.del = function (id) {
        AutoBusiness.delMailTemplates(id, function (ok, message) {
            if (!ok) {
                BootstrapDialog.alert({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'ERROR',
                    message: message
                })
            }
            else {
                $scope.load();
            }
        })
        };






});

cont.controller('AddChangeMailTmp', function ($scope, $http, $location, AutoBusiness) {

    $scope.mail = AutoBusiness.getCurMailTmp();

    if ($scope.mail.id == "")
        $scope.u1 = "MailTemplate erstellen";
    else
        $scope.u1 = "MailTemplate ändern";



    $scope.save = function (valid) {
        if (valid) {
            AutoBusiness.addupdateMailTemplates($scope.mail, function (ok, message) {
                if (!ok) {
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'ERROR',
                        message: message
                    })
                }
                else {
                    $location.path('/MailTemplate');
                }

            });
        }

    }

    $scope.cancel = function () {
        $location.path('/MailTemplate');
    }





});