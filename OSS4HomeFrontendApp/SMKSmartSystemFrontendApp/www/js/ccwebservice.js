/**
 *  Zugriffe auf das Backend
 */


var webservicecc =angular.module('WebserviceCC', ['Route','ngRoute']);

webservicecc.service('CCWS', function($http) {
	  this.post = function(data, handling , path) {
		 	$http.post(webservice+'/'+path, data).success(function(data, status, headers, config) {
		 		handling(true,'')
	        }).error(function(data, status, headers, config) {
	            
	        }).catch(function(error){
	        	handling(false,JSON.stringify(error));
	        }); 
	  };
	  
	  this.put = function(data, handling , path) {
		 	$http.put(webservice+'/'+path, data).success(function(data, status, headers, config) {
		 		handling(true,'')
	        }).error(function(data, status, headers, config) {
	            
	        }).catch(function(error){
	        	handling(false,JSON.stringify(error));
	        }); 
	  };

	  this.get = function(obj, path){
		  $http.get(webservice+'/'+path).then(obj);
	  };
	  
	  this.delete = function(data, handling , path){
		  $http.delete(webservice+'/'+path+'/'+data).success(function(data, status, headers, config){
			  handling(true,'')
	        }).error(function(data, status, headers, config) {
	        	 
	        }).catch(function(error){
	        	handling(false,JSON.stringify(error));
	        });
		  
	  };
});


