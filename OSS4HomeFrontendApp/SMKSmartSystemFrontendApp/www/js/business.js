﻿var business = angular.module('BusinessLogic', ['WebserviceCC']);

business.service('SensorenBusiness', function ($http, CCWS) {
    
    this.getValues = function (modelsetter) {
        
        
        CCWS.get(modelsetter, 'devices/value/SENSOR');


    };

 

   
});


business.service('SwitchBusiness', function ($http, CCWS) {

    this.getValues = function (modelsetter) {


        CCWS.get(modelsetter, 'devices/value/SWITCH');


    };




});


business.service('RelaisBusiness', function ($http, CCWS) {

    this.getValues = function (modelsetter){


        CCWS.get(modelsetter, 'devices/value/RELAIS');

    };

    this.changeValue = function(data, errorHandler)
    {
        CCWS.put(data, errorHandler, 'devices/value');

    }




});




business.service('SystemBusiness', function ($http, CCWS) {

    this.getSettings = function (modelsetter) {
        CCWS.get(modelsetter, 'settings');


    };

    this.saveChanges = function (value, errorHandler) {
        
        CCWS.put(value, errorHandler, 'settings');
    }




});


business.service('GroupBusiness', function ($http, CCWS, RelaisBusiness) {

    var curGroup = null;

    this.setCurGroup = function (newObj) {
        curGroup = newObj;
    };

    this.getCurGroup = function () {
        return curGroup;
    };

    this.getValues = function (modelsetter) {

        CCWS.get(modelsetter, 'groups/value');


    };

    this.getConfig = function (modelsetter) {
        CCWS.get(modelsetter, 'groups/config');
    };

    this.setConfig = function ( data ,handling) {
        CCWS.put(data, handling, 'groups/config');
    };

    this.changeValue = function (value, errorHandler) {
        CCWS.put(value, errorHandler, 'groups/value');
    }

    this.getAvabileDevices = function (modelsetter, filter)
    {
        var man = function (data, error) {
            if (!error) {
                var dataarray = new Array(0);

                var start = 0
                data.data.forEach(function
                    (element, index, array) {
                    var found = false;
                    filter.forEach(function
                    (elementfilter, indexfilter, arrayfilter) {
                        if (elementfilter.deviceId == element.id &&
                            elementfilter.controllerId == element.controllerId) {
                            found = true;
                        }
                    });

                    if (!found) {
                        var x = JSON.parse('{ "id" : "","deviceId" : "" , "controllerId" : "" , "name" : ""}');
                        x.deviceId = element.id;
                        x.controllerId = element.controllerId;
                        x.name = element.name;
                        dataarray.push(x);
                    }
                });
                modelsetter(dataarray, '')

            }
            else {
                modelsetter(data, error)
            }
        };
        RelaisBusiness.getValues(man);

    }

    this.del = function (data, handling) {
        CCWS.delete(data, handling, 'groups/config');
    }


});


business.service('DevicesBusiness', function ($http, CCWS) {

    var curDevice = null;

    this.setCurDevice = function (newObj) {
        curDevice = newObj;
    };

    this.getCurDevice = function () {
        return curDevice;
    };

    this.getConfig = function (modelsetter) {

        CCWS.get(modelsetter, 'devices/config');

    };

    this.setConfig = function (data, handling) {

        CCWS.put(data, handling, 'devices/config');

    };






});

business.service('ControllerBusiness', function ($http, CCWS) {
    var productList = null;

    this.setCurController = function (newObj) {
        productList = newObj;
    };

    this.getCurController = function () {
        return productList;
    };

    this.addupdate = function (data, handling) {

        CCWS.put(data, handling, 'controllers');

    };

    this.getControllers = function (modelsetter) {

        CCWS.get(modelsetter, 'controllers');

    };


    this.delete = function (id, handling) {

        CCWS.delete(id, handling, 'controllers');

    };


});



business.service('AutoBusiness', function ($http, DevicesBusiness, GroupBusiness, CCWS) {
    var autoRule = null;
    var mailtmp = null;

    this.setCurAutoRule = function (newObj) {
        autoRule = newObj;
    };

    this.getCurAutoRule = function () {
        return autoRule;
    };

    this.setCurMailTmp = function (newObj) {
        mailtmp = newObj;
    };

    this.getCurMailTmp = function () {
        return mailtmp;
    };

    this.getTrigger = function(modelsetter)
    {
        
        var man = function (data, error)
        {
            if (!error)
            {
                var triggerarray = new Array(1);
                triggerarray[0] = JSON.parse('{"id" : "", "controllerId" : "" , "deviceId" :"" , "type" : "TIME" , "relationalOperator" : "", "value" : "* * * * *", "name" : "Zeitsteuerung"}');
                var start = 1
                data.data.forEach(function
                    (element, index, array) {
                    if (element.active) {
                        triggerarray.length = triggerarray.length + 1;
                        triggerarray[start] = JSON.parse('{"id" : "", "controllerId" : "" , "deviceId" :"" , "type" : "" , "relationalOperator" : "", "value" : "", "name" : ""}');
                        triggerarray[start].controllerId = element.controllerId;
                        triggerarray[start].deviceId = element.id;
                        triggerarray[start].name = element.name;
                        if (element.type == "SENSOR") {
                            triggerarray[start].type = "SENSOR"
                        }
                        else {

                            triggerarray[start].type = "SWITCHRELAIS"

                        }
                        start = start + 1;
                    }


                }

                );


                modelsetter(triggerarray, '');
            }
            else
            {
                modelsetter('', error);
            }

            

        }

        DevicesBusiness.getConfig(man);


    }

    this.getActionsGroups = function (modelsetter) {

        var man = function (data, error) {
            if (!error) {
                var triggerarray = new Array(0);
               
                var start = 0
                data.data.forEach(function 
                    (element, index, array) {
                   
                        triggerarray.length = triggerarray.length + 1;
                        triggerarray[start] = JSON.parse('{"id" : "", "controllerId" : "" , "deviceId" :"" , "type" : "" , "value" : "", "name" : ""}');
                        triggerarray[start].groupId = element.id;
                        triggerarray[start].name = 'GROUP: '+element.name;
                        triggerarray[start].type = "GROUP"
                        start = start + 1;
                    


                }

                );


                modelsetter(triggerarray, '');
            }
            else {
                modelsetter('', error);
            }
        }

        GroupBusiness.getConfig(man);


    }


    this.getActions = function (modelsetter) {

        var man = function (data, error) {
            if (!error) {
                var triggerarray = new Array(1);
                triggerarray[0] = JSON.parse('{"id" : "", "controllerId" : "" , "deviceId" :"" , "type" : "MAIL" , "groupId" : "", "value" : "", "name" : "MAIL"}');
                var start = 1
                data.data.forEach(function 
                    (element, index, array) {
                    if (element.active && element.type == "RELAIS") {
                        triggerarray.length = triggerarray.length + 1;
                        triggerarray[start] = JSON.parse('{"id" : "", "controllerId" : "" , "deviceId" :"" , "type" : "MAIL" , "groupId" : "", "value" : "ON", "name" : ""}');
                        triggerarray[start].controllerId = element.controllerId;
                        triggerarray[start].deviceId = element.id;
                        triggerarray[start].name = 'DEVICE: '+element.name;
                        triggerarray[start].type = "SWITCHRELAIS"
                        start = start + 1;
                    }


                }

                );


                modelsetter(triggerarray, '');
            }
            else {
                modelsetter('', error);
            }



        }

        DevicesBusiness.getConfig(man);


    }



    this.get = function (modelsetter) {

        CCWS.get(modelsetter, 'automation');


    };

    this.del = function (data, handling) {
        CCWS.delete(data, handling, 'automation');
    }

    this.saveChanges = function (value, errorHandler) {
        CCWS.put(value, errorHandler, 'automation');
    }

    this.getMailTemplates = function (modelsetter)
    {
        CCWS.get(modelsetter, 'mailtemplate');
    }


    this.addupdateMailTemplates = function (data, handling) {
        CCWS.put(data, handling, 'mailtemplate');
    }

    this.delMailTemplates = function (data, handling) {
        CCWS.delete(data, handling, 'mailtemplate');
    }

    this.getEventlog = function (modelsetter)
    {
        CCWS.get(modelsetter, 'automation/eventlog');
    }




});
