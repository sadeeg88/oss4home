/**
 * 
 */



var cont =angular.module('Route', ['ngRoute']);


cont.config(function ($routeProvider) {
	  $routeProvider.when("/", {
		    templateUrl: "partsites/p_home.html",
		  }).when("/devicesConfig", {
		    templateUrl: "partsites/p_devicesConfig.html",
		  }).when("/SystemSettings", {
			    templateUrl: "partsites/p_SystemSettings.html",
		  }).when("/AddController", {
		      templateUrl: "partsites/p_AddController.html",
		  }).when("/ControllRelais", {
		      templateUrl: "partsites/p_ControllRelais.html",
		  }).when("/ControllGroups", {
		      templateUrl: "partsites/p_ControllGroups.html",
		  }).when("/DevicesConfig", {
		      templateUrl: "partsites/p_DevicesConfig.html"
		  }).when("/UpdateDevicesConfig", {
		      templateUrl: "partsites/p_UpdateDevicesConfig.html"
		  }).when("/AddChangeAuto", {
		      templateUrl: "partsites/p_AddChangeAuto.html"
		  }).when("/GroupsSetting", {
		      templateUrl: "partsites/p_GroupsSetting.html"
		  }).when("/AddChangeGroup", {
		      templateUrl: "partsites/p_AddChangeGroup.html"
		  }).when("/AutoView", {
		      templateUrl: "partsites/p_AutoView.html"
		  }).when("/AddChangeMailTmp", {
		      templateUrl: "partsites/p_AddChangeMailTmp.html"
		  }).when("/MailTemplate", {
		      templateUrl: "partsites/p_MailTemplate.html"
		  }).when("/EventLog", {
		      templateUrl: "partsites/p_EventLog.html"
		  }).otherwise({
		    redirectTo: "/"
		  });
		});

        




