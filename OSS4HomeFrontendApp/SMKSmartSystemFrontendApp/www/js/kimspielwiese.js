/**
 * Test zu Lernen für KIm
 */
var kim =angular.module('Kim', ['WebserviceCC']);

/*
 * Webservice Modul sollte eigentlich in der ccwebservice angelgt werden
 * aber da nur Test, hier definiert hier nichts zu editieren!
 * 
 * Der Webservice liefert und akzeptiert folgende Strucktur bei get und add
 * .id
 * .name
 * .value
 * 
 * beim delete nur ID
 */

kim.service('KimWS', function(CCWS) {

	  this.add = function(data, handling) {
		  CCWS.add(data,handling,'Kim');
	  };

	  this.get = function(obj){
		  CCWS.get(obj,'Kim')
	  };
	  
	  this.delete = function(data, handling){
		  CCWS.delete(data,handling,'Kim');  
	  };


	});






kim.controller('kimlist', function($scope, $http, $location , KimWS)
		{
			$scope.desc = 'Hallo Kim'
			
			//Implementiere den Webservice aufruf über KimWS
			//Vergesse nicht die lambda Funktion mit dem Scope!
			var data = function(sdata)
			{
				$scope.kimdv = sdata.data;
			};
			
			KimWS.get(data);
				
			
			//Rufe Add Form - muss nicht ergänzt werden	
			$scope.add = function ()
			{
				$location.path('/kimadd');
			};
			
			
			//Delete Funktion
			$scope.delete = function (id)
			{
				//Implementiere den Webserviceaufruf
				//Vergesse nicht die lambda Funktion für Fehlerhandling!
				//siehe mainapp.js
			};
			
			
		});

kim.controller('kimadd', function($scope, $http, $location , KimWS)
		{
			$scope.desc = 'Add Button wurde gedrückt'
			
					
			//Funktion um das Formular an den WS zu senden	
			$scope.add = function ()
			{
				    	var data = $scope.cur_controller;
				       	var handling = function(Success , ErrorMsg)
				    	{
				        	if(Success == false)
				        	{
				        		alert(ErrorMsg);
				        	}
				        	else
				        	{
				        		 $location.path('/kim');
				        	}
				    	}
				       	KimWS.add(data,handling); 
			
			};
			
			
			
			
			
			
		});