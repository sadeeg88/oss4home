/**
 * Module zur Konfiguration der Devices
 */

var devicesconfig =angular.module('DeviceConfig', ['WebserviceCC']);


devicesconfig.controller('DeviceConfigList', function($scope, $http, $location , DeviceConfigWS)
{
	//Webservice Laden
	
	//Lambda Funktion für get Response
	var data = function(sdata)
	{
		$scope.devices = sdata.data;
	}
	DeviceConfigWS.get(data);
	
	
});
