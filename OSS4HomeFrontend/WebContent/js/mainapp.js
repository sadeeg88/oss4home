/**
 * Konfiguration 
 */
var webservice = 'http://localhost:5555';
	



/**
 * Generelle Anwendungsübergreifende Funtktionen
 */

var cont =angular.module('OSS4HomeSmartSystemApp', ['Kim', 'DeviceConfig','WebserviceCC','Route','ngRoute']);


/**
 * ControllerService
 */
cont.service('ControllerData', function() {
	  var productList = null;

	  this.setController = function(newObj) {
	      productList = newObj;
	  };

	  this.getController = function(){
	      return productList;
	  };


	});

/**
cont.controller('Sensoren', function($scope, $http){
    $http.get(webservice+'/Sensors').then(function(sdata) {
        $scope.sensoren = sdata.data;
      });
    });*/

cont.controller('ControllerSettings', function($scope, $http, $location , ControllerData, ControllerWS){
	ControllerWS.get(function(sdata) {
        $scope.controllers = sdata.data;
      });
    
    $scope.add = function() {
    	ControllerData.setController(null);
    	$location.path('/AddController');
    };
    
    $scope.change = function(id) {
    	for (x in $scope.controllers)
        {
    		var controller =  $scope.controllers[x];
    		if(controller.id ==   id)
    		{
    		    ControllerData.setController(controller);
    		}
    	}
    	$location.path('/AddController');
    };
    
    $scope.del = function(id) {
    	    	
    	var handling = function(Success , ErrorMsg)
    	{
        	if(Success == false)
        	{
        		alert(ErrorMsg);
        	}
        	else
        	{
        		 $location.path('/SystemSettings');
                 window.location.reload();
        	}
    	}
    	ControllerWS.delete(id,handling);

    	
    	
    };
    
    });


cont.controller('AddChangeController', function($scope, $http, $location , ControllerData, ControllerWS){
	var data = ControllerData.getController();
	
	// Wenn es sich um einen Änderungsdialog handelt
    // Description und funktion überschreiben
	if(data != null )
    {
		$scope.cur_controller = data;
		
			  $scope.buttonACConntroller = 'Change'; 
			    $scope.AddChangeControllerToWS = function() {
			    	var data = $scope.cur_controller;
			       	var handling = function(Success , ErrorMsg)
			    	{
			        	if(Success == false)
			        	{
			        		alert(ErrorMsg);
			        	}
			        	else
			        	{
			        		 $location.path('/SystemSettings');

			        	}
			    	}
			    	ControllerWS.update(data,handling); 
			    };
			
    }
	else
	{
		$scope.buttonACConntroller = 'Add'; 
		
	    $scope.AddChangeControllerToWS = function() {
	    	var data = $scope.cur_controller;
	       	var handling = function(Success , ErrorMsg)
	    	{
	        	if(Success == false)
	        	{
	        		alert(ErrorMsg);
	        	}
	        	else
	        	{
	        		 $location.path('/SystemSettings');
	        	}
	    	}
	    	ControllerWS.add(data,handling); 
		
	}

    };	
    
    $scope.Cancel =  function() {
    	$location.path('/SystemSettings');
    };
    
    
    
    });

