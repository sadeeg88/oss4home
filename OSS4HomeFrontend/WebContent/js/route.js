/**
 * 
 */



var cont =angular.module('Route', ['ngRoute']);


cont.config(function ($routeProvider) {
	  $routeProvider.when("/", {
		    templateUrl: "partsites/p_home.html",
		  }).when("/devicesConfig", {
		    templateUrl: "partsites/p_devicesConfig.html",
		  }).when("/SystemSettings", {
			    templateUrl: "partsites/p_SystemSettings.html",
		  }).when("/AddController", {
			    templateUrl: "partsites/p_AddController.html",
			    
		  }).when("/kim", {
			    templateUrl: "kimsp/p_kim.html",
		  }).when("/kimadd", {
			    templateUrl: "kimsp/p_kimadd.html",
			    
		  }).otherwise({
		    redirectTo: "/"
		  });
		});






