/**
 *  Zugriffe auf das Backend
 */
var webservice = 'http://localhost:5555';

var webservicecc =angular.module('WebserviceCC', ['Route','ngRoute']);

webservicecc.service('CCWS', function($http) {
	  this.add = function(data, handling , path) {
		 	$http.put(webservice+'/'+path, data).success(function(data, status, headers, config) {
		 		handling(true,'')
	        }).error(function(data, status, headers, config) {
	            
	        }).catch(function(error){
	        	handling(false,JSON.stringify(error));
	        }); 
	  };
	  
	  this.update = function(data, handling , path) {
		 	$http.post(webservice+'/'+path, data).success(function(data, status, headers, config) {
		 		handling(true,'')
	        }).error(function(data, status, headers, config) {
	            
	        }).catch(function(error){
	        	handling(false,JSON.stringify(error));
	        }); 
	  };

	  this.get = function(obj, path){
		  $http.get(webservice+'/'+path).then(obj);
	  };
	  
	  this.delete = function(data, handling , path){
		  $http.delete(webservice+'/'+path+'/'+data).success(function(data, status, headers, config){
			  handling(true,'')
	        }).error(function(data, status, headers, config) {
	        	 
	        }).catch(function(error){
	        	handling(false,JSON.stringify(error));
	        });
		  
	  };
});


webservicecc.service('ControllerWS', function(CCWS) {

	  this.add = function(data, handling) {
		  CCWS.add(data,handling,'Controllers');
	  };
	  
	  this.update = function(data, handling) {
		  CCWS.update(data,handling,'Controllers');
	  };

	  this.get = function(obj){
		  CCWS.get(obj,'Controllers')
	  };
	  
	  this.delete = function(data, handling){
		  CCWS.delete(data,handling,'Controllers');  
	  };


	});

webservicecc.service('DeviceConfigWS', function(CCWS) {

	  this.add = function(data, handling) {
		  CCWS.add(data,handling,'Sensors');
	  };
	  
	  this.update = function(data, handling) {
		  CCWS.update(data,handling,'Sensors');
	  };

	  this.get = function(obj){
		  CCWS.get(obj,'Sensors')
	  };
	  
	  this.delete = function(data, handling){
		  CCWS.delete(data,handling,'Sensors');  
	  };


	});