package de.oss4home.zwave;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
public class zwaveapiTestCase1 {
	
	@Autowired(required=true)
	Zwaveapi impZWaveApi;

	@Test
	public void test() throws ZwaveException {
		
		impZWaveApi.setConnectionString("http://192.168.22.35:8083/ZAutomation/api/v1/", new Authenticator("admin", "bmw2x#88"));	
		Zwavedevices dev = impZWaveApi.getDevices();
		assertEquals(false, dev.getData().getDevices().isEmpty());
		
		//impZWaveApi.setDevice("ZWayVDev_zway_2-0-37", "off");
		//impZWaveApi.setDevice("ZWayVDev_zway_2-0-37", "on");
		
	}

}
