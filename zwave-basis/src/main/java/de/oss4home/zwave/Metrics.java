
package de.oss4home.zwave;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "title",
    "text",
    "icon",
    "probeTitle",
    "scaleTitle",
    "level"
})
public class Metrics {

    @JsonProperty("title")
    private String title;
    @JsonProperty("text")
    private String text;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("probeTitle")
    private String probeTitle;
    @JsonProperty("scaleTitle")
    private String scaleTitle;
    @JsonProperty("level")
    private String level;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Metrics() {
    }

    /**
     * 
     * @param icon
     * @param text
     * @param title
     * @param level
     * @param probeTitle
     * @param scaleTitle
     */
    public Metrics(String title, String text, String icon, String probeTitle, String scaleTitle, String level) {
        super();
        this.title = title;
        this.text = text;
        this.icon = icon;
        this.probeTitle = probeTitle;
        this.scaleTitle = scaleTitle;
        this.level = level;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    @JsonProperty("probeTitle")
    public String getProbeTitle() {
        return probeTitle;
    }

    @JsonProperty("probeTitle")
    public void setProbeTitle(String probeTitle) {
        this.probeTitle = probeTitle;
    }

    @JsonProperty("scaleTitle")
    public String getScaleTitle() {
        return scaleTitle;
    }

    @JsonProperty("scaleTitle")
    public void setScaleTitle(String scaleTitle) {
        this.scaleTitle = scaleTitle;
    }

    @JsonProperty("level")
    public String getLevel() {
        return level;
    }

    @JsonProperty("level")
    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(title).append(text).append(icon).append(probeTitle).append(scaleTitle).append(level).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Metrics) == false) {
            return false;
        }
        Metrics rhs = ((Metrics) other);
        return new EqualsBuilder().append(title, rhs.title).append(text, rhs.text).append(icon, rhs.icon).append(probeTitle, rhs.probeTitle).append(scaleTitle, rhs.scaleTitle).append(level, rhs.level).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
