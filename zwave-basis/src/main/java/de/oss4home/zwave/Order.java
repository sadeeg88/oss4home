
package de.oss4home.zwave;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "rooms",
    "elements",
    "dashboard",
    "room"
})
public class Order {

    @JsonProperty("rooms")
    private Integer rooms;
    @JsonProperty("elements")
    private Integer elements;
    @JsonProperty("dashboard")
    private Integer dashboard;
    @JsonProperty("room")
    private Integer room;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Order() {
    }

    /**
     * 
     * @param dashboard
     * @param elements
     * @param room
     * @param rooms
     */
    public Order(Integer rooms, Integer elements, Integer dashboard, Integer room) {
        super();
        this.rooms = rooms;
        this.elements = elements;
        this.dashboard = dashboard;
        this.room = room;
    }

    @JsonProperty("rooms")
    public Integer getRooms() {
        return rooms;
    }

    @JsonProperty("rooms")
    public void setRooms(Integer rooms) {
        this.rooms = rooms;
    }

    @JsonProperty("elements")
    public Integer getElements() {
        return elements;
    }

    @JsonProperty("elements")
    public void setElements(Integer elements) {
        this.elements = elements;
    }

    @JsonProperty("dashboard")
    public Integer getDashboard() {
        return dashboard;
    }

    @JsonProperty("dashboard")
    public void setDashboard(Integer dashboard) {
        this.dashboard = dashboard;
    }

    @JsonProperty("room")
    public Integer getRoom() {
        return room;
    }

    @JsonProperty("room")
    public void setRoom(Integer room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(rooms).append(elements).append(dashboard).append(room).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Order) == false) {
            return false;
        }
        Order rhs = ((Order) other);
        return new EqualsBuilder().append(rooms, rhs.rooms).append(elements, rhs.elements).append(dashboard, rhs.dashboard).append(room, rhs.room).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
