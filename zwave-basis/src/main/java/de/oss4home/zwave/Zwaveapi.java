package de.oss4home.zwave;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.springframework.stereotype.Service;



@Service
public class Zwaveapi {
	
		private String connectionString;
		private ClientConfig cc = null;
		private Client client = null;
		private WebTarget maintarget = null;
		private Cookie cookie = null;
		private Authenticator auth;

		
		
	


		public void setConnectionString(String connectionString,Authenticator auth) throws ZwaveException {
			this.connectionString = connectionString;
			cc = new ClientConfig().register(new JacksonFeature());
			client = JerseyClientBuilder.newClient(cc);
			maintarget = client.target(connectionString);
			this.auth = auth;
			login();
		}
		
		private void login() throws ZwaveException
		{
			String logindata = "{\"form\":true,\"login\":\""+auth.getUser()+"\",\"password\":\""+auth.getPassword()+"\",\"keepme\":false,\"default_ui\":1}";
			try {
				WebTarget target = maintarget.path("login");
				Response resp = target.request(MediaType.APPLICATION_JSON).cookie(cookie)
						.post(Entity.json(logindata), Response.class);
				cookie = resp.getCookies().get("ZWAYSession");
				resp.getDate();
			} catch (Exception ex) {
				String message = "Fehler beim Abruf der Devices vom Controller ," + "ConnectionString: " + connectionString;
				throw new ZwaveException(message, ex);
			}
		}
		
		

		public Zwavedevices getDevices() throws ZwaveException  {
			try {
				WebTarget target = maintarget.path("devices");
				Zwavedevices ret = target.request(MediaType.APPLICATION_JSON).cookie(cookie)
						.get(new GenericType<Zwavedevices>() {
						});
				return ret;
			} catch (Exception ex) {
				String message = "Fehler beim Abruf der Devices vom Controller ," + "ConnectionString: " + connectionString;
				throw new ZwaveException(message, ex);
			}
		}
		
		public Response setDevice(String device, String command) throws ZwaveException
		{
			try {
				WebTarget target = maintarget.path("devices").path(device).path("command").path(command);
				Response ret = target.request(MediaType.APPLICATION_JSON).cookie(cookie)
						.get(Response.class);
				return ret;
			} catch (Exception ex) {
				String message = "Fehler beim Abruf der Devices vom Controller ," + "ConnectionString: " + connectionString;
				throw new ZwaveException(message, ex);
			}
			
		}

}
