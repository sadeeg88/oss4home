
package de.oss4home.zwave;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "creationTime",
    "creatorId",
    "customIcons",
    "deviceType",
    "h",
    "hasHistory",
    "id",
    "location",
    "metrics",
    "order",
    "permanently_hidden",
    "probeType",
    "tags",
    "visibility",
    "updateTime"
})
public class Device {

    @JsonProperty("creationTime")
    private Integer creationTime;
    @JsonProperty("creatorId")
    private Integer creatorId;
    @JsonProperty("customIcons")
    private CustomIcons customIcons;
    @JsonProperty("deviceType")
    private String deviceType;
    @JsonProperty("h")
    private Integer h;
    @JsonProperty("hasHistory")
    private Boolean hasHistory;
    @JsonProperty("id")
    private String id;
    @JsonProperty("location")
    private Integer location;
    @JsonProperty("metrics")
    private Metrics metrics;
    @JsonProperty("order")
    private Order order;
    @JsonProperty("permanently_hidden")
    private Boolean permanentlyHidden;
    @JsonProperty("probeType")
    private String probeType;
    @JsonProperty("tags")
    private List<Object> tags = null;
    @JsonProperty("visibility")
    private Boolean visibility;
    @JsonProperty("updateTime")
    private Integer updateTime;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Device() {
    }

    /**
     * 
     * @param tags
     * @param updateTime
     * @param visibility
     * @param location
     * @param permanentlyHidden
     * @param creatorId
     * @param customIcons
     * @param h
     * @param id
     * @param order
     * @param metrics
     * @param deviceType
     * @param hasHistory
     * @param creationTime
     * @param probeType
     */
    public Device(Integer creationTime, Integer creatorId, CustomIcons customIcons, String deviceType, Integer h, Boolean hasHistory, String id, Integer location, Metrics metrics, Order order, Boolean permanentlyHidden, String probeType, List<Object> tags, Boolean visibility, Integer updateTime) {
        super();
        this.creationTime = creationTime;
        this.creatorId = creatorId;
        this.customIcons = customIcons;
        this.deviceType = deviceType;
        this.h = h;
        this.hasHistory = hasHistory;
        this.id = id;
        this.location = location;
        this.metrics = metrics;
        this.order = order;
        this.permanentlyHidden = permanentlyHidden;
        this.probeType = probeType;
        this.tags = tags;
        this.visibility = visibility;
        this.updateTime = updateTime;
    }

    @JsonProperty("creationTime")
    public Integer getCreationTime() {
        return creationTime;
    }

    @JsonProperty("creationTime")
    public void setCreationTime(Integer creationTime) {
        this.creationTime = creationTime;
    }

    @JsonProperty("creatorId")
    public Integer getCreatorId() {
        return creatorId;
    }

    @JsonProperty("creatorId")
    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    @JsonProperty("customIcons")
    public CustomIcons getCustomIcons() {
        return customIcons;
    }

    @JsonProperty("customIcons")
    public void setCustomIcons(CustomIcons customIcons) {
        this.customIcons = customIcons;
    }

    @JsonProperty("deviceType")
    public String getDeviceType() {
        return deviceType;
    }

    @JsonProperty("deviceType")
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    @JsonProperty("h")
    public Integer getH() {
        return h;
    }

    @JsonProperty("h")
    public void setH(Integer h) {
        this.h = h;
    }

    @JsonProperty("hasHistory")
    public Boolean getHasHistory() {
        return hasHistory;
    }

    @JsonProperty("hasHistory")
    public void setHasHistory(Boolean hasHistory) {
        this.hasHistory = hasHistory;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("location")
    public Integer getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(Integer location) {
        this.location = location;
    }

    @JsonProperty("metrics")
    public Metrics getMetrics() {
        return metrics;
    }

    @JsonProperty("metrics")
    public void setMetrics(Metrics metrics) {
        this.metrics = metrics;
    }

    @JsonProperty("order")
    public Order getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Order order) {
        this.order = order;
    }

    @JsonProperty("permanently_hidden")
    public Boolean getPermanentlyHidden() {
        return permanentlyHidden;
    }

    @JsonProperty("permanently_hidden")
    public void setPermanentlyHidden(Boolean permanentlyHidden) {
        this.permanentlyHidden = permanentlyHidden;
    }

    @JsonProperty("probeType")
    public String getProbeType() {
        return probeType;
    }

    @JsonProperty("probeType")
    public void setProbeType(String probeType) {
        this.probeType = probeType;
    }

    @JsonProperty("tags")
    public List<Object> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    @JsonProperty("visibility")
    public Boolean getVisibility() {
        return visibility;
    }

    @JsonProperty("visibility")
    public void setVisibility(Boolean visibility) {
        this.visibility = visibility;
    }

    @JsonProperty("updateTime")
    public Integer getUpdateTime() {
        return updateTime;
    }

    @JsonProperty("updateTime")
    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(creationTime).append(creatorId).append(customIcons).append(deviceType).append(h).append(hasHistory).append(id).append(location).append(metrics).append(order).append(permanentlyHidden).append(probeType).append(tags).append(visibility).append(updateTime).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Device) == false) {
            return false;
        }
        Device rhs = ((Device) other);
        return new EqualsBuilder().append(creationTime, rhs.creationTime).append(creatorId, rhs.creatorId).append(customIcons, rhs.customIcons).append(deviceType, rhs.deviceType).append(h, rhs.h).append(hasHistory, rhs.hasHistory).append(id, rhs.id).append(location, rhs.location).append(metrics, rhs.metrics).append(order, rhs.order).append(permanentlyHidden, rhs.permanentlyHidden).append(probeType, rhs.probeType).append(tags, rhs.tags).append(visibility, rhs.visibility).append(updateTime, rhs.updateTime).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
