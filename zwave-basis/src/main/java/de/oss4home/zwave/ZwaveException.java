package de.oss4home.zwave;

public class ZwaveException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8684211279615842405L;
		
		public ZwaveException()
		{
		}

		public ZwaveException(String message)
		{
			super(message);
		}

		public ZwaveException(Throwable cause)
		{
			super(cause);
		}

		public ZwaveException(String message, Throwable cause)
		{
			super(message, cause);
		}

		public ZwaveException(String message, Throwable cause,boolean enableSuppression, boolean writableStackTrace)
		{
			super(message, cause, enableSuppression, writableStackTrace);
		}

	

}
