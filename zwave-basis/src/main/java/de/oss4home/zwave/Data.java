
package de.oss4home.zwave;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "structureChanged",
    "updateTime",
    "devices"
})
public class Data {

    @JsonProperty("structureChanged")
    private Boolean structureChanged;
    @JsonProperty("updateTime")
    private Integer updateTime;
    @JsonProperty("devices")
    private List<Device> devices = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param structureChanged
     * @param updateTime
     * @param devices
     */
    public Data(Boolean structureChanged, Integer updateTime, List<Device> devices) {
        super();
        this.structureChanged = structureChanged;
        this.updateTime = updateTime;
        this.devices = devices;
    }

    @JsonProperty("structureChanged")
    public Boolean getStructureChanged() {
        return structureChanged;
    }

    @JsonProperty("structureChanged")
    public void setStructureChanged(Boolean structureChanged) {
        this.structureChanged = structureChanged;
    }

    @JsonProperty("updateTime")
    public Integer getUpdateTime() {
        return updateTime;
    }

    @JsonProperty("updateTime")
    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }

    @JsonProperty("devices")
    public List<Device> getDevices() {
        return devices;
    }

    @JsonProperty("devices")
    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(structureChanged).append(updateTime).append(devices).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Data) == false) {
            return false;
        }
        Data rhs = ((Data) other);
        return new EqualsBuilder().append(structureChanged, rhs.structureChanged).append(updateTime, rhs.updateTime).append(devices, rhs.devices).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
