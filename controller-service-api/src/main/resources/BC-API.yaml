swagger: '2.0'
info:
  version: '1.0.0'
  title: 'Backend API'
  description: 'API für die HarwareAnbindung'
basePath: /api
schemes:
- http
paths:
 /config:
   get:
      summary: Configuration
      description: ""
      operationId: getConfig
      consumes:
      - application/json
      produces:
      - application/json
      tags:
      - Configuration    
      responses:
        200:
          description: Config
          schema:
                type: string 
        400:
          description: unexpected error
          schema:
                $ref: "#/definitions/error"
   put:
     summary: Configuration
     operationId: setConfig
     tags:
      - Configuration
     consumes:
      - application/json
     produces:
      - application/json
     parameters:
     - name: Configuration
       in: body
       description: Configuration to Save
       required: true
       schema:
         type: string 
     responses:
        204:
          description: OK
        400:
          description: unexpected error
          schema:
                $ref: "#/definitions/error"     
 
 /devices:
    get:
      summary: List of all avabile Devices
      description: ""
      operationId: getDevices
      consumes:
      - application/json
      produces:
      - application/json
      tags:
      - Devices    
      parameters:
       - in: "query"
         name: "devicetype"
         description: "Filter of devicetypes"
         required: false
         type: string
       - in: "query"
         name: "sytemtype"
         description: Filter of Systemtypes
         required: false
         type: string 
      responses:
        200:
          description: List of Device
          schema:
                $ref: "#/definitions/devices" 
        400:
          description: unexpected error
          schema:
                $ref: "#/definitions/error" 
 /devices/{deviceId}:
    get:
      summary: Get the Setting of One Device
      operationId: getDevice
      tags:
      - Devices
      consumes:
      - application/json
      produces:
      - application/json      
      parameters:
       - name: deviceId
         in: path
         required: true
         description: deviceId
         type: string  
      responses:
        200:
          description: a Setting of one Device
          schema:
                $ref: "#/definitions/device" 
        400:
          description: unexpected error
          schema:
                $ref: "#/definitions/error" 
    put:
     summary: Change the Setting of One Device
     operationId: changeDeviceSettings
     tags:
      - Devices
     consumes:
      - application/json
     produces:
      - application/json
     parameters:
     - name: deviceId
       in: path
       required: true
       description: deviceId
       type: string
     - name: setting
       in: body
       description: Setting to Save
       required: true
       schema:
        $ref: "#/definitions/update_device"  
     responses:
        204:
          description: OK
        400:
          description: unexpected error
          schema:
                $ref: "#/definitions/error"            
 /devices/{deviceId}/values:
    get:
      summary: Get the Values of One Device
      operationId: getDeviceValues
      tags:
      - Devices
      consumes:
      - application/json
      produces:
      - application/json    
      parameters:
       - name: deviceId
         in: path
         required: true
         description: deviceId 
         type: string 
      responses:
        200:
          description: Values of one Device
          schema:
                $ref: "#/definitions/values" 
        400:
          description: unexpected error
          schema:
                type: array
                items: 
                   $ref: "#/definitions/error"    
 /devices/{deviceId}/value/{valueId}:
    get:
      summary: Get one Value of One Device
      operationId: getDeviceValue
      tags:
      - Devices
      consumes:
      - application/json
      produces:
      - application/json    
      parameters:
       - name: deviceId
         in: path
         required: true
         description: deviceId
         type: string
       - name: valueId
         in: path
         required: true
         description: valueId
         type: string    
      responses:
        200:
          description: one Value
          schema:
                $ref: "#/definitions/value"  
        400:
          description: unexpected error
          schema:
                $ref: "#/definitions/error" 
    put:
     summary: Change Value of One Device
     operationId: changeDeviceValue
     tags:
      - Devices
     consumes:
      - application/json
     produces:
      - application/json
     parameters:
     - name: deviceId
       in: path
       required: true
       description: deviceId
       type: string
     - name: valueId
       in: path
       required: true
       description: valueId
       type: string
     - name: value   
       in: body
       description: Setting to Save
       required: true
       schema:
        $ref: "#/definitions/value"  
     responses:
        204:
          description: OK
        400:
          description: unexpected error
          schema:
                $ref: "#/definitions/error"
 /version:
     get:
      summary: Get BC Controller Version
      operationId: getVersion
      tags:
      - Devices
      consumes:
      - application/json
      produces:
      - application/json    
      responses:
        200:
          description: Version des Controller apiversion Muss der aktuellen API entsprechen also aktuell version 2
          schema:
                $ref: "#/definitions/version"  
        400:
          description: unexpected error
          schema:
                $ref: "#/definitions/error" 
 /register:
     put:
      summary: registerPush
      operationId: registerPush
      tags:
      - Devices
      consumes:
       - application/json
      produces:
       - application/json
      parameters:
      - name: value   
        in: body
        description: Setting fpr Register
        required: true
        schema:
         $ref: "#/definitions/registerType"  
      responses:
        200:
          description: Controller regist
        400:
          description: unexpected error
        
definitions:
  version:
    type: object
    properties:
      version:
        type: integer
        format: int32
      name:
        type: string
      desciption:
        type: string
      apiversion:
        type: integer
        format: int32
      push_compatible:
        type: boolean
  devices:
    type: array
    items: 
      $ref: "#/definitions/device"
  device:
    type: object
    required:
      - id
    properties:
      id:
        type: string
      devicetype:
           $ref: "#/definitions/devicetype"
      possibledevicetypes:
        type: array
        items:
           $ref: "#/definitions/devicetype"
      values:
        type: array
        items: 
          $ref: "#/definitions/value"
  update_device:
    type: object
    required:
      - id
    properties:
      id:
        type: string
      devicetype:
           $ref: "#/definitions/devicetype"
      possibledevicetypes:
        type: array
        items:
           $ref: "#/definitions/devicetype"
  values:
    type: array
    items:
       $ref: "#/definitions/value"
  value:
     type: object      
     properties: 
      id:
       type: integer
       format: int32
      readonly:
        type: boolean
      valuetype:
        type: string
        enum: 
         - booleanValue
         - intValue
         - stringValue
      value:
        type: string
      maxValue:
        type: integer
        format: int32
      minValue:
        type: integer
        format: int32
      valueUnit:
        type: string
  error:
    type: object
    required:
      - code
      - message
    properties:
      code:
        type: integer
        format: int32
      message:
        type: string
  registerType:
    type: object
    properties: 
     url:
      type: string     
  devicetype:
    type: object
    properties:
      value:
        type: string
        enum:
              - batterie
              - doorlock
              - thermostat
              - switchBinary
              - switchMultilevel
              - sensorBinary
              - sensorMultilevel
              - toggelButton
              - switchControl
              - switchRGB