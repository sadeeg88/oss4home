package de.oss4home.bc.converter;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import de.oss4home.zwave.Data;
import de.oss4home.zwave.Metrics;
import io.swagger.model.DeviceType;
import io.swagger.model.DevicesSetting;
import io.swagger.model.DevicesValues;

public class ZWaveOss4HomeTranslaterTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void zWaveToOss4Hometest() {
		//Arrange
		de.oss4home.zwave.Data data = new de.oss4home.zwave.Data();
		data.setDevices(new ArrayList<de.oss4home.zwave.Device>());
		de.oss4home.zwave.Device devices = new de.oss4home.zwave.Device();
		devices.setDeviceType("switchBinary");
		devices.setId("1");
		devices.setMetrics(new Metrics("","","","on","",""));
		
		io.swagger.model.Device excepted = new io.swagger.model.Device();
		DevicesSetting deviceSetting = new DevicesSetting();
		deviceSetting.setDevicetype(DeviceType.NUMBER_switchBinary);
		deviceSetting.addPossibledevicetypesItem(DeviceType.NUMBER_switchBinary);
		//excepted.id("1").setting(deviceSetting).addValuesItem(new DevicesValues().id(1).readonly(false).valuetype(valuetype))
		
		//Act
		
		//Assert
	}

}
