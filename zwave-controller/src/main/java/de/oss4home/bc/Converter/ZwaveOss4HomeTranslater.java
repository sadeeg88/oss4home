package de.oss4home.bc.Converter;

import org.springframework.stereotype.Service;
import de.oss4home.zwave.ZWaveCommand;
import de.oss4home.swagger.generate.model.Device;
import de.oss4home.swagger.generate.model.Devicetype;
import de.oss4home.swagger.generate.model.DeviceSetting;;

@Service
public class ZwaveOss4HomeTranslater {
	
	public ZWaveCommand generateZWaveCommand(de.oss4home.swagger.generate.model.Device device)
	{
		ZWaveCommand command = new ZWaveCommand();
		
		if(device.getSetting().getDevicetype().getValue() == Devicetype.ValueEnum.switchBinary )
		{
			command.setId(device.getId());
			
			if(device.getValues().size() == 1)
			{
				if(device.getValues().get(0).getValue().equals(ConverterConstants.ON)
						||device.getValues().get(0).getValue().equals(ConverterConstants.OFF))
				command.setCommand(device.getValues().get(0).getValue());
			}
		}
		
		
		return command;		
	}
	
	public Device zWaveToOss4Home(String id, de.oss4home.zwave.Data data)
	{
		Device device = null;
		for (de.oss4home.zwave.Device zWaveDevice : data.getDevices()) {
			if(zWaveDevice.getId().equalsIgnoreCase(id))
			{
				device = new Device();
				device.setId(id);
				device.setSetting(new DeviceSetting());
				//device.getSetting().setDevicetype(DeviceType.fromValue(zWaveDevice.getDeviceType()));
				//device.getSetting().addPossibledevicetypesItem(DeviceType.fromValue(zWaveDevice.getDeviceType()));
				
				
			}
		}
		
		return device;
	}
	

}
