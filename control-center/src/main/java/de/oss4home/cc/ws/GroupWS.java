//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.Level;

import de.oss4home.cc.business.OSS4HomeManager;
import de.oss4home.cc.dto.GroupConfigDTO;
import de.oss4home.cc.dto.GroupValueDTO;
import de.oss4home.cc.ws.validation.Validation;
import de.oss4home.exceptions.OSS4HomeException;

@Path("groups")
public class GroupWS {

	@GET
	@Path("config")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<GroupConfigDTO> getGroupConfigs(){
		
	
			return OSS4HomeManager.getInstance().getGroupConfigs();

	}
	
	@PUT
	@Path("config")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response addUpdateGroupConfig(GroupConfigDTO groupDTO){
		// TODO: Validation
		Validation validation = new Validation();
		try{
		boolean add = true;
		
		
		if (groupDTO.getId() != null && (!groupDTO.getId().isEmpty())) {
			add = false;
		}
		
		
		if (add) {
			
			if(validation.isAddGroupValid(groupDTO)){
				OSS4HomeManager.getInstance().addGroup(groupDTO);
				return Response.ok().build();
			}
			else {
				return Response.notAcceptable(null).build();
			}
		}
		else {
			if (validation.isUpdateGroupValid(groupDTO)) {
				OSS4HomeManager.getInstance().updateGroupConfig(groupDTO);
				return Response.ok().build();
			}
			else {
				return Response.notAcceptable(null).build();
			}
		}
		}catch(Exception e)
		{
			OSS4HomeManager.getInstance().getLogger().log(Level.ERROR, e);
			return Response.serverError().build();
		}
	}
	
	@DELETE
	@Path("config/{id}")
	public Response deleteGroup(@PathParam("id") int id){
		Validation validation = new Validation();
		try
		{
			if (validation.isDeleteGroupValid(id)) {
				OSS4HomeManager.getInstance().deleteGroup(id);
				return Response.ok().build();
			}
			else {
			return Response.notAcceptable(null).build();	
			}
		}catch(Exception e)
		{
			OSS4HomeManager.getInstance().getLogger().log(Level.ERROR, e);
			return Response.serverError().build();
		}

	}
	
	@GET
	@Path("value")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<GroupValueDTO> getGroupValues(){
		
		
		
			return OSS4HomeManager.getInstance().getGroupValues();
		
	}
	
	@PUT
	@Path("value")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response setGroupValue(GroupValueDTO groupValueDTO){
		Validation validation = new Validation();
		
		if (validation.isSetGroupValueValid(groupValueDTO)) {
			try {
				OSS4HomeManager.getInstance().setGroupValue(groupValueDTO);
				return Response.ok().build();
			} catch (OSS4HomeException e) {
				return Response.serverError().build();
			}
		}
		else {
			return Response.notAcceptable(null).build();
		}
	}
	
	
}
