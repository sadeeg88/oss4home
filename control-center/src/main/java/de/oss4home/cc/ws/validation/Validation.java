//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.ws.validation;

import de.oss4home.cc.business.AutoTrigger;
import de.oss4home.cc.business.Controller;
import de.oss4home.cc.business.Device;
import de.oss4home.cc.business.Group;
import de.oss4home.cc.business.OSS4HomeManager;
import de.oss4home.cc.dto.AutoActionDTO;
import de.oss4home.cc.dto.AutoTriggerDTO;
import de.oss4home.cc.dto.AutomationRuleDTO;
import de.oss4home.cc.dto.AutomationTyp;
import de.oss4home.cc.dto.ControllerDTO;
import de.oss4home.cc.dto.DeviceConfigDTO;
import de.oss4home.cc.dto.DeviceInfoDTO;
import de.oss4home.cc.dto.DeviceTyp;
import de.oss4home.cc.dto.DeviceValueDTO;
import de.oss4home.cc.dto.GroupConfigDTO;
import de.oss4home.cc.dto.GroupValueDTO;
import de.oss4home.cc.dto.MailTemplateDTO;
import de.oss4home.cc.dto.SystemSettingDTO;
import de.oss4home.cc.dto.GroupValueDTO.GroupValue;
import de.oss4home.cc.wshandler.ControllerWsHandler;
import de.oss4home.exceptions.OSS4HomeException;

public class Validation {
	
	public boolean isUpdateSystemSettingValid(SystemSettingDTO dto)
	{
		boolean ok = true;
		
		if(dto.getSmtpHost().length()> 255
				||dto.getPort().length()> 255
				||dto.getUserName().length()> 255
				||dto.getPassword().length()> 255)
		{
			ok = false;
		}
		return ok;
	}

	/**
	 * Gibt false zur�ck, falls die DTO fehlende Werte enth�lt.
	 * 
	 * @param dto
	 * @return
	 */
	public boolean isUpdateDeviceValueValid(DeviceValueDTO dto) {
		try {
			Device device = OSS4HomeManager.getInstance().getDevice(dto.getId(), dto.getControllerId());

			if (device == null) {
				return false;
			}
			if (device.getTyp() != DeviceTyp.RELAIS) {
				return false;
			}
			if (device.isActive() == false) {
				return false;
			}
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Gibt false zur�ck, falls die DTO fehlende Werte enth�lt.
	 * 
	 * @param dto
	 * @return
	 */
	public boolean isUpdateDeviceConfigValid(DeviceConfigDTO dto) {
		try {
			// Might throw a Exception if Id or ControllerId are invalid
			Device device = OSS4HomeManager.getInstance().getDevice(dto.getId(), dto.getControllerId());
			if (dto.getType() == null) {
				return false;
			}
			if (device.getTyp() == DeviceTyp.SENSOR) {
//				if (dto.getCalculationFormula() == null || dto.getCalculationFormula().isEmpty()) {
//					return false;
//				}
//				if (dto.getUnit() == null || dto.getUnit().isEmpty()) {
//					return false;
//				} 
			}

			// If Device is a Sensor it can't be changed to something other than
			// Sensor
			// Also possible NullpointerException if getDevice return Null
			if (device.getTyp() == DeviceTyp.SENSOR && dto.getType() != DeviceTyp.SENSOR) {
				return false;
			}
			if (dto.getName() == null || dto.equals("")) {
				return false;
			}

		} catch (Exception e) {
			return false;
		}

		// If nothing went wrong return true
		return true;
	}
	
	
	public boolean isAutoAddUpdateValid(AutomationRuleDTO auto)
	{
		if(auto.getName() == null 
			|| auto.getName().isEmpty()
			|| auto.getName().length() > 255
				)
		{
			return false;
		}
		if(auto.getTrigger() == null
				|| auto.getTrigger().isEmpty())
		{
			return false;
		}
		if(auto.getActions() == null
				|| auto.getActions().isEmpty())
		{
			return false;
		}
		for (AutoTriggerDTO trigger : auto.getTrigger() ) {
			if(!isAutoTriggerValid(trigger))
				return false;
		}
		
		for (AutoActionDTO action : auto.getActions() ) {
			if(!isAutoActionValid(action))
				return false;
		}
		
		
		return true;
	}
	
	private boolean isAutoTriggerValid(AutoTriggerDTO auto)
	{
		if(auto.getType() != AutomationTyp.SENSOR
				&& auto.getType() != AutomationTyp.SWITCHRELAIS
				&& auto.getType() != AutomationTyp.TIME)
		{
			return false;
		}
		
		if(auto.getType() == AutomationTyp.SENSOR ||
				auto.getType() == AutomationTyp.SWITCHRELAIS)
		{
			
				if(null == OSS4HomeManager.getInstance().getDevice(auto.getDeviceId(), auto.getContollerId()))
				{
					return false;
				}
	
		}
		
		
		if(auto.getType() == AutomationTyp.SENSOR)
		{
			if(!(auto.getRelationalOperator().equals("=")
			||auto.getRelationalOperator().equals("<>") 
			||auto.getRelationalOperator().equals("<") 
			||auto.getRelationalOperator().equals(">")) )
			{
				return false;
			}
			try
			{
				Double.parseDouble(auto.getValue());
			}catch(Exception ex)
			{
				return false;
			}
			
		}
		else if(auto.getType() == AutomationTyp.SWITCHRELAIS)
		{
			if(!(auto.getValue().equals("ON")
					||auto.getValue().equals("OFF") 
					||auto.getValue().equals("TOGGLE") ) )
					{
					return false;
					}
		}
		
		return true;
	}
	
	private boolean isAutoActionValid(AutoActionDTO auto)
	{
		if(auto.getType() != AutomationTyp.MAIL
				&& auto.getType() != AutomationTyp.SWITCHRELAIS
				&& auto.getType() != AutomationTyp.GROUP)
		{
			return false;
		}
		
		if(auto.getType() == AutomationTyp.SWITCHRELAIS)
		{
			if(!(auto.getValue().equals("ON")
					||auto.getValue().equals("OFF") 
					||auto.getValue().equals("TOGGLE") ) )
					{
					return false;
					}
		
				if(null == OSS4HomeManager.getInstance().getDevice(auto.getDeviceId(), auto.getControllerId()))
				{
					return false;
				}
		
		}
		else if(auto.getType() == AutomationTyp.GROUP)
		{
			if(!(auto.getValue().equals("ON")
					||auto.getValue().equals("OFF") 
					||auto.getValue().equals("TOGGLE") ) )
					{
					return false;
					}
			
			try {
				if(null == OSS4HomeManager.getInstance().getGroup(Integer.parseInt(auto.getGroupId())))
				{
					return false;
				}
			} catch (Exception e)
			{
				return false;
			}
		}
		else if(auto.getType() == AutomationTyp.MAIL)
		{
			try {
				if(null == OSS4HomeManager.getInstance().getMailTemplate(Integer.parseInt(auto.getValue())))
				{
					return false;
				}
			} catch (Exception e)
			{
				return false;
			}
		}
		

		
		return true;
	}

	/**
	 * Gibt false zur�ck, falls der Connection String bereits verwendet wird
	 * oder Name leer ist.
	 * 
	 * @param dto
	 * @return
	 * @throws OSS4HomeException 
	 * @throws Exception
	 */
	public boolean isAddControllerValid(ControllerDTO dto) throws OSS4HomeException {
		// Wenn der ConnectionString schon verwendet wird, geben wir False
		// zur�ck.
		for (Controller controller : OSS4HomeManager.getInstance().getControllerManager().getAllController()) {
			if (controller.getConnectionString().equals(dto.getConnectionString())) {
				return false;
			}
		}
		if (dto.getName() == null || dto.getName().equals("")) {
			return false;
		}

		ControllerWsHandler wshandler = new ControllerWsHandler(dto.getConnectionString());
		try {
			wshandler.getVersionInfos();
		} catch (Exception ex) {
			return false;
		}

		return true;
	}

	/**
	 * Gibt false zur�ck, falls der Controller nicht gefunden wird oder das DTO
	 * fehlende Werte enth�lt.
	 * 
	 * @param dto
	 * @return
	 */
	public boolean isUpdateControllerValid(ControllerDTO dto) {
		try {
			// Return false if the controller doesn't exists or the Id could not
			// be converted to int
			if (OSS4HomeManager.getInstance().getControllerManager().getController(Integer.parseInt(dto.getId())) == null) {
				return false;
			}
			if (dto.getConnectionString() == null || dto.getConnectionString().isEmpty()) {
				return false;
			}
			if (dto.getName() == null || dto.getName().isEmpty()) {
				return false;
			}

			ControllerWsHandler wshandler = new ControllerWsHandler(dto.getConnectionString());
			wshandler.getVersionInfos();

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Gibt false zur�ck, falls der zu l�schende Controller nicht gefunden wird.
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public boolean isDeleteControllerValid(int id) throws Exception {
		if (OSS4HomeManager.getInstance().getControllerManager().getController(id) == null) {
			return false;
		}
		return true;
	}

	/**
	 * Gibt false zur�ck, falls die Gruppe keinen Namen hat und die
	 * DeviceInfoDTOs keine Controller/Device Id besitzten.
	 * 
	 * @param dto
	 * @return
	 */
	public boolean isAddGroupValid(GroupConfigDTO dto) {
		if (dto.getName() == null || dto.getName().isEmpty()) {
			return false;
		}
		for (DeviceInfoDTO deviceInfoDTO : dto.getDevices()) {
			if (deviceInfoDTO.getControllerId() == null || deviceInfoDTO.getControllerId().isEmpty()) {
				return false;
			}
			if (deviceInfoDTO.getDeviceId() == null || deviceInfoDTO.getDeviceId().isEmpty()) {
				return false;
			}
		}

		return true;
	}

	public boolean isUpdateGroupValid(GroupConfigDTO dto) {
		if (dto.getId() == null || dto.getId().isEmpty()) {
			return false;
		}
		if (dto.getName() == null || dto.getName().isEmpty()) {
			return false;
		}

		for (DeviceInfoDTO deviceInfoDTO : dto.getDevices()) {
			if (deviceInfoDTO.getControllerId() == null || deviceInfoDTO.getControllerId().isEmpty()) {
				return false;
			}
			if (deviceInfoDTO.getDeviceId() == null || deviceInfoDTO.getDeviceId().isEmpty()) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Gibt false zur�ck, falls es die zu L�schende Gruppe nicht gibt.
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public boolean isDeleteGroupValid(int id) throws Exception {
		Group group = OSS4HomeManager.getInstance().getGroup(id);
		if (group == null) {
			return false;
		}
		return true;
	}

	/**
	 * Gibt false zur�ck, falls das DTO keine Id enth�lt oder keinen g�ltigen
	 * GroupValue
	 * 
	 * @param dto
	 * @return
	 */
	public boolean isSetGroupValueValid(GroupValueDTO dto) {
		if (dto.getId() == null || dto.getId().isEmpty()) {
			return false;
		}
		if (dto.getValue() != GroupValue.OFF && dto.getValue() != GroupValue.ON) {
			return false;
		}
		return true;
	}

	/**
	 * Gibt false zur�ck, falls einer der Werte im DTO leer ist.
	 * 
	 * @param dto
	 * @return
	 */
	public boolean isAddMailTemplateValid(MailTemplateDTO dto) {
		if (dto.getName() == null || dto.getName().isEmpty() || dto.getName().length() >255) {
			return false;
		}
		if (dto.getTo() == null || dto.getTo().isEmpty()|| dto.getTo().length() >255) {
			return false;
		}
		if (dto.getSubject() == null || dto.getSubject().isEmpty()|| dto.getSubject().length() >255) {
			return false;
		}
		if (dto.getBody() == null || dto.getBody().isEmpty()) {
			return false;
		}
		return true;
	}

	/**
	 * Gibt false zur�ck, falls es kein MailTemplate mit der Id existiert oder
	 * einer der Werte leer ist.
	 * 
	 * @param dto
	 * @return
	 */
	public boolean isUpdateMailTemplateValid(MailTemplateDTO dto) {
		try {
			if (OSS4HomeManager.getInstance().getMailTemplate(Integer.parseInt(dto.getId())) == null) {
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}

		if (dto.getName() == null || dto.getName().isEmpty()|| dto.getName().length() >255) {
			return false;
		}
		if (dto.getTo() == null || dto.getTo().isEmpty()|| dto.getTo().length() >255) {
			return false;
		}
		if (dto.getSubject() == null || dto.getSubject().isEmpty()|| dto.getSubject().length() >255) {
			return false;
		}
		if (dto.getBody() == null || dto.getBody().isEmpty()) {
			return false;
		}

		return true;
	}

	/**
	 * Gibt false zur�ck, falls kein MailTemplate mit der �bergebenen Id existiert.
	 * @param id
	 * @return
	 */
	public boolean isDeleteMailTemplateValid(int id) {

		try {
			if (OSS4HomeManager.getInstance().getMailTemplate(id) == null) {
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}

		return true;
	}

	public boolean isDeviceTypeValid(String type){
		if (type.equals(DeviceTyp.RELAIS.toString())) {
			return true;
		}else if (type.equals(DeviceTyp.SENSOR.toString())) {
			return true;
		}else if (type.equals(DeviceTyp.SWITCH.toString())) {
			return true;
		}else
			return false;
	}
}
