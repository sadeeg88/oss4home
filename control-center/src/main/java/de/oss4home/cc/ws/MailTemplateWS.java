//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.oss4home.cc.business.OSS4HomeManager;
import de.oss4home.cc.dto.MailTemplateDTO;
import de.oss4home.cc.ws.validation.Validation;
import de.oss4home.exceptions.OSS4HomeException;

@Path("mailtemplate")
public class MailTemplateWS {

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<MailTemplateDTO> getMailTemplates() {
			return OSS4HomeManager.getInstance().getMailTemplates();
	
	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response addUpdateMailTemplate(MailTemplateDTO dto) {
		Validation validation = new Validation();
		boolean add = false;

		if (dto.getId() == null || dto.getId().isEmpty()) {
			add = true;
		}

		if (add) {
			if (validation.isAddMailTemplateValid(dto)) {
				try {
					OSS4HomeManager.getInstance().addMailTemplate(dto);
				} catch (OSS4HomeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return Response.serverError().build();
				}
			} else {
				return Response.notAcceptable(null).build();
			}

		} else {
			if (validation.isAddMailTemplateValid(dto)) {
				try {
					OSS4HomeManager.getInstance().updateMailTemplate(dto);
				} catch (OSS4HomeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return Response.serverError().build();
				}
			} else {
				return Response.notAcceptable(null).build();

			}
		}

		return Response.ok().build();
	}

	@DELETE
	@Path("{id}")
	public Response deleteMailTemplate(@PathParam("id") int id) {
		Validation validation = new Validation();
		if (validation.isDeleteMailTemplateValid(id)) {

			try {
				OSS4HomeManager.getInstance().deleteMailTemplate(id);
			} catch (OSS4HomeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return Response.serverError().build();
			}

		} else {
			return Response.notAcceptable(null).build();

		}

		return Response.ok().build();

	}

}
