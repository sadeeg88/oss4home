//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.Level;

import de.oss4home.cc.business.OSS4HomeManager;
import de.oss4home.cc.dto.ControllerDTO;
import de.oss4home.cc.interfaces.IControllerWS;
import de.oss4home.cc.ws.validation.Validation;
import de.oss4home.cc.wshandler.ControllerWsHandler;
import de.oss4home.exceptions.OSS4HomeException;

@Path("controllers")
public class ControllerWS implements IControllerWS {

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<ControllerDTO> getController() {
		List<ControllerDTO> test = null;
		try {
			test = OSS4HomeManager.getInstance().getControllers();
		} catch (Exception e) {
			OSS4HomeManager.getInstance().getLogger().log(Level.ERROR, e);
		}
		return test;

	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response addUpdateController(ControllerDTO in) {
		boolean ok = true;
		boolean add = false;
		try {
			if (in.getId() == null || in.getId().isEmpty()) {
				add = true;
			}

			Validation validation = new Validation();

			if (add) {
				if (validation.isAddControllerValid(in)) {

					// Hinzuf�gen
					OSS4HomeManager.getInstance().addController(in);
				} else {
					return Response.notAcceptable(null).build();
				}

			} else {
				if (validation.isUpdateControllerValid(in)) {
					// Updaten
					OSS4HomeManager.getInstance().updateController(in);
				} else {
					return Response.notAcceptable(null).build();
				}
			}

			if (ok)
				return Response.ok().build();
			else
				return Response.status(404).build();
		} catch (Exception e) {
			OSS4HomeManager.getInstance().getLogger().log(Level.ERROR, e);
			return Response.serverError().build();
		}

	}

	@DELETE
	@Path("{id}")
	public Response deleteController(@PathParam("id") int id) {
		boolean ok = true;

		// Validation
		// TODO

		// L�schen
		try {
			OSS4HomeManager.getInstance().deleteController(id);
		} catch (OSS4HomeException e) {
			return Response.serverError().build();
		}

		if (ok)
			return Response.ok().build();
		else
			return Response.status(404).build();
	}

	@PUT
	@Path("test")
	public Response checkController(String con) {
		ControllerWsHandler wshandler = new ControllerWsHandler(con);
		try {
			wshandler.getVersionInfos();
		} catch (Exception ex) {
			OSS4HomeManager.getInstance().getLogger().log(Level.ERROR, ex);
			return Response.serverError().build();
		}
		return Response.ok().build();
	}

}
