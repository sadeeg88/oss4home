//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.Level;

import de.oss4home.cc.business.OSS4HomeManager;
import de.oss4home.cc.dto.DeviceConfigDTO;
import de.oss4home.cc.dto.DeviceValueDTO;
import de.oss4home.cc.interfaces.IDeviceWS;
import de.oss4home.cc.ws.validation.Validation;
import de.oss4home.exceptions.OSS4HomeException;

@Path("devices")
public class DevicesWS implements IDeviceWS {
	

	@GET
	@Path("value/{typ}")	    
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public List<DeviceValueDTO> getDeviceValue(@PathParam("typ") String typ) {
		try {
			return OSS4HomeManager.getInstance().getDevicesValues(typ);
		} catch (Exception e) {
			OSS4HomeManager.getInstance().getLogger().log(Level.ERROR, e);
		}
		return null;
	}

	@GET
	@Path("config")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public List<DeviceConfigDTO> getDeviceConfig() {
		try {
			return OSS4HomeManager.getInstance().getDeviceConfigs();
		} catch (Exception e) {
			OSS4HomeManager.getInstance().getLogger().log(Level.ERROR, e);
		}
		return null;
	}

	@PUT
	@Path("config")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response updateDeviceConfig(DeviceConfigDTO dto) {
		// Validation
		Validation validation = new Validation();
		if(!validation.isUpdateDeviceConfigValid(dto))
		{
			return Response.notAcceptable(null).build();
		}
		
		try {
			OSS4HomeManager.getInstance().setDeviceConfig(dto);
		} 
		catch(OSS4HomeException ex)
		{
			return Response.serverError().build();
		}

		return Response.ok().build();
		
	}

	@PUT
	@Path("value")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response setDeviceValue(DeviceValueDTO dto) {
		// Validation
		Validation validation = new Validation();
		if(!validation.isUpdateDeviceValueValid(dto))
		{
			return Response.notAcceptable(null).build();
		}
		
		try {
			OSS4HomeManager.getInstance().setDeviceValue(dto);
		} 
		catch(OSS4HomeException ex)
		{
			ex.printStackTrace();
			return Response.serverError().build();
		}

		return Response.ok().build();
		
	}
	

}
