//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc;

import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.Level;

import de.oss4home.cc.business.OSS4HomeManager;
import de.oss4home.cc.ws.AutomationWS;
import de.oss4home.cc.ws.ControllerWS;
import de.oss4home.cc.ws.DevicesWS;
import de.oss4home.cc.ws.GroupWS;
import de.oss4home.cc.ws.MailTemplateWS;
import de.oss4home.cc.ws.SystemSettingWS;
import de.oss4home.util.OSS4HomeResourceConfig;
import de.oss4home.util.Webserver;

public class ApplicationCC {
	
	/*URL desWebservice*/
	private static final URI BASE_URI = URI.create("http://0.0.0.0:5555/");
    private static boolean stop = false;
	
	public static void main(String[] args) throws Exception {
		

		
		OSS4HomeManager.getInstance();

		
		
		Webserver webserver = new Webserver(BASE_URI, initWebservice());
		try {
			webserver.start();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		while(!stop)
		{
			try {
				Thread.sleep(500);
				OSS4HomeManager.getInstance().sync();
			} catch (Exception e) {
				OSS4HomeManager.getInstance().getLogger().log(Level.ERROR, e);
				e.printStackTrace();
			}
			
	
		}

	}
	
	private static OSS4HomeResourceConfig initWebservice()
	{
		Set<Class<?>> webservices = new HashSet<Class<?>>();
		//webservices.add(SensorsWS.class);
		webservices.add(ControllerWS.class);
		webservices.add(DevicesWS.class);
		webservices.add(GroupWS.class);
		webservices.add(AutomationWS.class);
		webservices.add(SystemSettingWS.class);
		webservices.add(MailTemplateWS.class);
		return OSS4HomeResourceConfig.init(webservices);
	}

}
