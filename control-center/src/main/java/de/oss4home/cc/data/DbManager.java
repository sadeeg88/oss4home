//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import de.oss4home.cc.business.AutoAction;
import de.oss4home.cc.business.AutoTrigger;
import de.oss4home.cc.business.AutomationRule;
import de.oss4home.cc.business.Controller;
import de.oss4home.cc.business.Device;
import de.oss4home.cc.business.DeviceInfo;
import de.oss4home.cc.business.Group;
import de.oss4home.cc.business.MailTemplate;
import de.oss4home.cc.business.Setting;
import de.oss4home.exceptions.OSS4HomeJpaException;

/**
 * Singleton f�r das Speichern, Hinzuf�gen und L�schen der Entit�ten der
 * Datenbank
 * 
 * @author Kim
 *
 */
public class DbManager {

	private static final String PERSISTENCE_UNIT_NAME = "oss4homeData";
	private static EntityManagerFactory factory;

	private static DbManager instance = null;

	private DbManager() {
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	}

	public static DbManager getInstance() {
		if (instance == null) {
			instance = new DbManager();
		}
		return instance;
	}

	/**
	 * Gets and returns all the Controller stored in the DB
	 * 
	 * @return
	 * @throws OSS4HomeJpaException
	 */
	public List<Controller> getController() throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			Query q = em.createQuery("select c from Controller c");
			@SuppressWarnings("unchecked")
			List<Controller> controller = q.getResultList();
			em.close();
			return controller;
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Laden der Controller", e);
		}

	}

	/**
	 * Gets and returns all the Groups stored in the DB
	 * 
	 * @return
	 * @throws OSS4HomeJpaException
	 */
	@SuppressWarnings("unchecked")
	public List<Group> getGroups() throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			Query q = em.createQuery("select c from Group c");
			List<Group> groups = q.getResultList();
			em.close();
			return groups;
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Laden der Gruppen", e);
		}
	}

	/**
	 * This adds a new Device to the DB
	 * 
	 * @param device
	 * @throws OSS4HomeJpaException
	 */
	private  void addDevice(Device device) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			em.persist(device);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Hinzuf�gen eines Devices", e);
		}
	}

	/**
	 * Add the Device to the DB or update it, if it already exists.
	 * 
	 * @param updatedDevice
	 * @throws OSS4HomeJpaException
	 */
	public synchronized void updateDevice(Device updatedDevice) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			Device oldDevice = em.find(updatedDevice.getClass(), updatedDevice.getId());
			if (oldDevice == null) {
				this.addDevice(updatedDevice);
				return;
			}
			em.getTransaction().begin();
			oldDevice.updateDbRelevantData(updatedDevice);
			em.getTransaction().commit();
			em.close();
		} catch (OSS4HomeJpaException e) {
			throw new OSS4HomeJpaException("Fehler beim Updaten eines Devices", e);
		}
	}

	/**
	 * Delete the Device from the DB
	 * 
	 * @param device
	 * @throws OSS4HomeJpaException
	 */
	public void deleteDevice(Device device) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			Device deviceToDelete = em.find(device.getClass(), device.getId());
			em.getTransaction().begin();
			em.remove(deviceToDelete);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim L�schen eines Devices", e);
		}
	}

	/**
	 * Speichert einen Controller in der Datenbank.
	 * 
	 * @param controller
	 * @throws OSS4HomeJpaException
	 */
	public void addController(Controller controller) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			em.persist(controller);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Hinzuf�gen eines Controllers", e);
		}
	}

	/**
	 * Speichert �nderungen an einem Controller oder legt ihn in der Datenbank
	 * an falls er nicht existiert.
	 * 
	 * @param updatedController
	 * @throws OSS4HomeJpaException
	 */
	public void updateController(Controller updatedController) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			Controller oldController = em.find(updatedController.getClass(), updatedController.getId());
			if (oldController == null) {
				this.addController(updatedController);
				return;
			}
			em.getTransaction().begin();
			oldController.updateDbRelevantData(updatedController);
			em.getTransaction().commit();
			em.close();
		} catch (OSS4HomeJpaException e) {
			throw new OSS4HomeJpaException("Fehler beim Updaten eines Controllers", e);
		}
	}

	/**
	 * Delete the Controller from the DB
	 * 
	 * @param controller
	 * @throws OSS4HomeJpaException
	 */
	public synchronized void  deleteController(Controller controller) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			Controller controllerToDelete = em.find(controller.getClass(), controller.getId());
			for (Device dev : controllerToDelete.getDevices()) {
				deleteDevice(dev);
			}
			em.getTransaction().begin();
			em.remove(controllerToDelete);
			em.getTransaction().commit();
			em.close();
		} catch (OSS4HomeJpaException e) {
			throw new OSS4HomeJpaException("Fehler beim L�schen eines Controllers", e);
		}
	}

	/**
	 * Add a Group to the DB
	 * 
	 * @param group
	 * @throws OSS4HomeJpaException
	 */
	public Group addGroup(Group group) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			em.persist(group);
			em.getTransaction().commit();
			em.close();
			return group;
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Hinzuf�gen einer Gruppe", e);
		}
	}

	/**
	 * Add a Group to the DB or update it, if it already exists
	 * 
	 * @param updatedGroup
	 * @throws OSS4HomeJpaException
	 */
	public void updateGroup(Group updatedGroup) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			Group oldGroup = em.find(updatedGroup.getClass(), updatedGroup.getId());
			if (oldGroup == null) {
				return;
			}

			em.getTransaction().begin();
			oldGroup.updateDbRelevantData(updatedGroup);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Updaten einer Gruppe", e);
		}
	}

	/**
	 * Deletes the Group from the DB
	 * 
	 * @param group
	 * @throws OSS4HomeJpaException
	 */
	public void deleteGroup(Group group) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			Group groupToDelete = em.find(group.getClass(), group.getId());
			if (groupToDelete == null) {
				return;
			}
			// L�sche alle DeviceInfos der Gruppe bevor die Gruppe selbst
			// gel�scht
			// wird.
			this.deleteDeviceInfosOfGroup(group);

			em.getTransaction().begin();
			em.remove(groupToDelete);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim L�schen einer Gruppe", e);
		}
	}

	/**
	 * L�sche alle DeviceInfos einer Gruppe. Wird aufgerufen wenn Gruppe
	 * gel�scht wird.
	 * 
	 * @param group
	 * @throws OSS4HomeJpaException
	 */
	private void deleteDeviceInfosOfGroup(Group group) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			for (DeviceInfo deviceInfo : group.getDeviceInfos()) {
				em.remove(em.find(DeviceInfo.class, deviceInfo.getId()));
			}
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim L�schen der DeviceInfos einer Gruppe", e);
		}
	}

	/**
	 * L�scht das �bergebene DeviceInfo aus der Datenbank.
	 * 
	 * @param deviceInfo
	 * @throws OSS4HomeJpaException
	 */
	public void deleteDeviceInfo(DeviceInfo deviceInfo) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			em.remove(em.find(DeviceInfo.class, deviceInfo.getId()));
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim L�schen einer DeviceInfo", e);
		}

	}

	/**
	 * Gibt eine Liste mit allen AutomationRules zur�ck.
	 * 
	 * @return
	 * @throws OSS4HomeJpaException
	 */
	public List<AutomationRule> getAutoRules() throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			Query q = em.createQuery("select a from AutomationRule a");
			List<AutomationRule> ruleList = q.getResultList();
			em.close();
			return ruleList;
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Laden der AutomationRules", e);
		}
	}

	/**
	 * Speichert den �bergebenen AutoTrigger in der Datenbank und gibt diesen
	 * zur�ck.
	 * 
	 * @param autoTrigger
	 * @throws OSS4HomeJpaException
	 */
	public AutoTrigger addAutoTrigger(AutoTrigger autoTrigger) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			em.persist(autoTrigger);
			em.getTransaction().commit();
			em.close();
			return autoTrigger;
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Hinzuf�gen eines AutoTriggers", e);
		}
	}

	/**
	 * Speichert die �nderungen an einem AutoTrigger in der Datebank
	 * 
	 * @param autoTrigger
	 * @throws OSS4HomeJpaException
	 */
	public void updateAutoTrigger(AutoTrigger updatedAutoTrigger) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			AutoTrigger oldAutoTrigger = em.find(updatedAutoTrigger.getClass(), updatedAutoTrigger.getId());
			if (oldAutoTrigger == null) {
				return;
			}
			em.getTransaction().begin();
			oldAutoTrigger.updateDbRelevantData(updatedAutoTrigger);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Updaten eines AutoTriggers", e);
		}
	}

	/**
	 * L�scht den AutoTrigger dessen Id der �bergebenen entspricht aus der
	 * Datenbank.
	 * 
	 * @param id
	 * @throws OSS4HomeJpaException
	 */
	public void deleteAutoTrigger(int id) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			AutoTrigger autoTriggerToDelete = em.find(AutoTrigger.class, id);
			if (autoTriggerToDelete == null) {
				return;
			}
			em.getTransaction().begin();
			em.remove(autoTriggerToDelete);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim L�schen eines AutoTriggers", e);
		}
	}

	/**
	 * Speichert eine AutoAction in der Datenbank
	 * 
	 * @param autoAction
	 * @throws OSS4HomeJpaException
	 */
	public AutoAction addAutoAction(AutoAction autoAction) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			em.persist(autoAction);
			em.getTransaction().commit();
			em.close();
			return autoAction;
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Hinzuf�gen einer AutoAction", e);
		}
	}

	/**
	 * Speichert die �nderung an einer AutoAction in der Datenbank.
	 * 
	 * @param autoAction
	 * @throws OSS4HomeJpaException
	 */
	public void updateAutoAction(AutoAction updatedAutoAction) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			AutoAction oldAutoAction = em.find(updatedAutoAction.getClass(), updatedAutoAction.getId());
			if (oldAutoAction == null) {
				return;
			}
			em.getTransaction().begin();
			oldAutoAction.updateDbRelevantData(updatedAutoAction);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler biem Updaten einer AutoAction", e);
		}
	}

	/**
	 * L�scht eine AutoAction deren Id der �bergebenen Id entspricht.
	 * 
	 * @param id
	 * @throws OSS4HomeJpaException
	 */
	public void deleteAutoAction(int id) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			AutoAction autoActionToDelete = em.find(AutoAction.class, id);
			if (autoActionToDelete == null) {
				return;
			}
			em.getTransaction().begin();
			em.remove(autoActionToDelete);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim L�schen einer AutoAction", e);
		}
	}

	/**
	 * Speichert eine AutomationRule in der Datenbank.
	 * 
	 * @param rule
	 * @throws OSS4HomeJpaException
	 */
	public AutomationRule addAutoRule(AutomationRule rule) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			em.persist(rule);
			em.getTransaction().commit();
			em.close();
			return rule;
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Hinzuf�gen einer AutomationRule", e);
		}
	}

	/**
	 * Speichert �nderungen an einer AutomationRule in der Datenbank
	 * 
	 * @param rule
	 * @throws OSS4HomeJpaException
	 */
	public AutomationRule updateAutoRule(AutomationRule updatedAutoRule) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			AutomationRule oldAutoRule = em.find(updatedAutoRule.getClass(), updatedAutoRule.getId());
			if (oldAutoRule == null) {
				return this.addAutoRule(updatedAutoRule);
			}
			em.getTransaction().begin();
			oldAutoRule.updateDbRelevantData(updatedAutoRule); //in
			em.getTransaction().commit();
			em.close();
			return oldAutoRule;
		} catch (OSS4HomeJpaException e) {
			throw new OSS4HomeJpaException("Fehler beim Updaten einer AutomationRule", e);
		}
	}

	/**
	 * L�scht eine AutomationRule deren Id der �bergebenen entspricht aus
	 * der Datenbank.
	 * 
	 * @param id
	 * @throws OSS4HomeJpaException
	 */
	public void deleteAutoRule(int id) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			AutomationRule autoRuleToDelete = em.find(AutomationRule.class, id);
			if (autoRuleToDelete == null) {
				return;
			}
			// L�sche vorhandene AutoTrigger und AutoActions
			this.deleteAutoActionsOfRule(autoRuleToDelete);
			this.deleteAutoTriggerOfRule(autoRuleToDelete);

			em.getTransaction().begin();
			em.remove(autoRuleToDelete);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim L�schen einer AutomationRule", e);
		}
	}

	/**
	 * F�gt ein MailTemplate zur Datenbank hinzu.
	 * 
	 * @param mailTemplate
	 * @return
	 * @throws OSS4HomeJpaException
	 */
	public MailTemplate addMailTemplate(MailTemplate mailTemplate) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			if (em.find(mailTemplate.getClass(), mailTemplate.getId()) != null) {
				return null;
			}
			em.getTransaction().begin();
			em.persist(mailTemplate);
			em.getTransaction().commit();
			em.close();
			return mailTemplate;
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Hinzuf�gen eines MailTemplates", e);
		}
	}

	/**
	 * Updated ein MailTemplate mit den Werten des �bergebenen MailTemplate.
	 * Wird vom DbManager aufgerufen.
	 * 
	 * @param updatedTemplate
	 * @throws OSS4HomeJpaException
	 */
	public void updateMailTemplate(MailTemplate updatedTemplate) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			MailTemplate oldMailTemplate = em.find(updatedTemplate.getClass(), updatedTemplate.getId());
			if (oldMailTemplate == null) {
				throw new Exception("MailTemplate konnte nicht in der DB gefunden werden");
			}
			em.getTransaction().begin();
			oldMailTemplate.updateDbRelevantData(updatedTemplate);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Updaten eines MailTemplates", e);
		}
	}

	/**
	 * L�scht das MailTemplate deren Id der �bergebenen Id entspricht aus
	 * der Datenbank.
	 * 
	 * @param id
	 * @throws OSS4HomeJpaException
	 */
	public void deleteMailTemplate(int id) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			MailTemplate mailTemplateToDelete = em.find(MailTemplate.class, id);
			if (mailTemplateToDelete == null) {
				return;
			}
			em.getTransaction().begin();
			em.remove(mailTemplateToDelete);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim L�schen eines MailTemplates", e);
		}
	}

	/**
	 * Gibt eine List mit allen in der Datenbank gespeicherten MailTemplates
	 * zur�ck.
	 * 
	 * @return
	 * @throws OSS4HomeJpaException
	 */
	public List<MailTemplate> getMailTemplates() throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			Query q = em.createQuery("select m from MailTemplate m");
			List<MailTemplate> templateList = q.getResultList();
			em.close();
			return templateList;
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Laden der MailTemplates", e);
		}
	}

	/**
	 * L�scht alle AutoTrigger der �bergebenen AutomationRule
	 * 
	 * @param autoRule
	 * @throws OSS4HomeJpaException
	 */
	private void deleteAutoTriggerOfRule(AutomationRule autoRule) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			for (AutoTrigger trigger : autoRule.getTriggers()) {
				em.remove(em.find(AutoTrigger.class, trigger.getId()));
			}
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim L�schen der AutoTrigger einer AutomationRule", e);
		}
	}

	/**
	 * L�scht alle AutoActions der �bergebenen Automation Rule
	 * 
	 * @param autoRule
	 * @throws OSS4HomeJpaException
	 */
	private void deleteAutoActionsOfRule(AutomationRule autoRule) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			for (AutoAction action : autoRule.getActions()) {
				em.remove(em.find(AutoAction.class, action.getId()));
			}
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim L�schen der AutoActions einer AutomationRule", e);
		}
	}

	/**
	 * Gibt das SystemSettings Objekt zur�ck oder Null wenn dieses noch nicht existiert.
	 * @return
	 * @throws OSS4HomeJpaException
	 */
	public List<Setting> getSettings() throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			Query q = em.createQuery("select s from Setting s");
			List<Setting> groups = q.getResultList();
			em.close();
			return groups;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new OSS4HomeJpaException("Fehler beim Laden der SystemSettings", e);
		}
	}

	/**
	 * F�gt eine SystemSetting zur DB hinzu falls es noch keine gibt.
	 * @param systemSettings
	 * @throws OSS4HomeJpaException
	 */
	public void addSetting(Setting setting) throws OSS4HomeJpaException {
		try {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			em.persist(setting);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			throw new OSS4HomeJpaException("Fehler beim Hinzuf�gen der SystemSettings", e);
		}
	}

	
	public void updateSetting(Setting setting) throws OSS4HomeJpaException{
		try {
			EntityManager em = factory.createEntityManager();
			Setting oldSettings = em.find(Setting.class, setting.getKey());
			// Falls es noch keine SystemSettings gibt werden neue angelegt
			if (oldSettings == null) {
				this.addSetting(setting);
				return;
			}
			em.getTransaction().begin();
			oldSettings.setValue(setting.getValue());
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new OSS4HomeJpaException("Fehler beim Updaten der SystemSettings",e);
		}
		
	}
}
