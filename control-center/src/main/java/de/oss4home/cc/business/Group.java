//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;

import de.oss4home.cc.data.DbManager;
import de.oss4home.cc.dto.DeviceInfoDTO;
import de.oss4home.cc.dto.GroupConfigDTO;
import de.oss4home.exceptions.OSS4HomeJpaException;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

@Entity
@Table(name="Gruppe")
public class Group {
	
	@Id
	@TableGenerator(name="groupGen", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator="groupGen")
	private int id;
	
	private String name;
	
	public Group(){
		deviceInfos = new Vector<DeviceInfo>();
	}
	
	@OneToMany(mappedBy="parent",  fetch=FetchType.EAGER, cascade=CascadeType.PERSIST)
	private List<DeviceInfo> deviceInfos;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gibt die DeviceInfo zur�ck deren Id der �bergebenen entspricht oder null, falls es eine solche
	 * DeviceInfo nicht gibt.
	 * @param id
	 * @return
	 */
	private DeviceInfo getDeviceInfo(int id){
		for (DeviceInfo deviceInfo : deviceInfos) {
			if (deviceInfo.getId() == id) {
				return deviceInfo;
			}
		}
		return null;
	}


	/**
	 * Wird vom DbManager aufgerufen um die Gruppe in der Datenbank zu aktualisieren.
	 * @param updatedGroup
	 * @throws OSS4HomeJpaException 
	 */
	public void updateDbRelevantData(Group updatedGroup) throws OSS4HomeJpaException {
		if (this.id == updatedGroup.id) {
			this.setName(updatedGroup.name);
			
			Iterator<DeviceInfo> iterator = this.getDeviceInfos().iterator();
			
			// L�sche alle DeviceInfos die in der neuen Gruppe nicht enthalten sind.
			while (iterator.hasNext()) {
				DeviceInfo deviceInfo = (DeviceInfo) iterator.next();
				if(updatedGroup.getDeviceInfo(deviceInfo.getId()) == null){
					DbManager.getInstance().deleteDeviceInfo(deviceInfo);
					iterator.remove();
				}
			}
			
			// Update vorhandene DeviceInfos und f�ge neue hinzu
			for (DeviceInfo deviceInfo : updatedGroup.getDeviceInfos()) {
				
				DeviceInfo oldDeviceInfo = this.getDeviceInfo(deviceInfo.getId());
				if(oldDeviceInfo == null)
				{
					this.deviceInfos.add(deviceInfo);
				}
				else {
					oldDeviceInfo = deviceInfo;
				}
				
			}
		}
		
	}

	public List<DeviceInfo> getDeviceInfos() {
		return deviceInfos;
	}

	public void setDeviceInfos(List<DeviceInfo> deviceInfos) {
		this.deviceInfos = deviceInfos;
	}
	
	public GroupConfigDTO getAsGroupConfigDTO(){
		GroupConfigDTO dto = new GroupConfigDTO();
		dto.setId(new Integer(this.getId()).toString());
		dto.setName(this.getName());
		dto.setDevices(this.getDeviceInfosAsDTOs());
		return dto;
	}

	private List<DeviceInfoDTO> getDeviceInfosAsDTOs() {
		List<DeviceInfoDTO> list = new Vector<DeviceInfoDTO>();
		for (DeviceInfo deviceInfo : deviceInfos) {
			list.add(deviceInfo.getAsDeviceInfoDTO());
		}
		return list;
	}
	
	public void deleteDeviceInfosFromController(int id) throws OSS4HomeJpaException
	{
		Iterator<DeviceInfo> iter = deviceInfos.iterator();
		while(iter.hasNext())
		{
			DeviceInfo info = iter.next();
			if(info.getControllerId() == id)
			{
				iter.remove();
			}
		}
		DbManager.getInstance().updateGroup(this);
	}
	

}
