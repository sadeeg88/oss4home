//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;

import java.util.List;
import java.util.Vector;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.layout.PatternLayout;

import de.oss4home.cc.dto.AutomationRuleDTO;
import de.oss4home.cc.dto.ControllerDTO;
import de.oss4home.cc.dto.DeviceConfigDTO;
import de.oss4home.cc.dto.DeviceInfoDTO;
import de.oss4home.cc.dto.DeviceTyp;
import de.oss4home.cc.dto.DeviceValueDTO;
import de.oss4home.cc.dto.EventLogDTO;
import de.oss4home.cc.dto.GroupConfigDTO;
import de.oss4home.cc.dto.GroupValueDTO;
import de.oss4home.cc.dto.MailTemplateDTO;
import de.oss4home.cc.dto.SystemSettingDTO;
import de.oss4home.cc.dto.GroupValueDTO.GroupValue;
import de.oss4home.exceptions.OSS4HomeControllerWSException;
import de.oss4home.exceptions.OSS4HomeException;
import de.oss4home.exceptions.OSS4HomeJpaException;
import de.oss4home.bc.dto.BcDeviceConfigDTO;
import de.oss4home.bc.dto.BcDeviceValueDTO;

/**
 * Singleton Manager Klasse f�r die Business Logik des OSS4Home Systems
 * 
 * @author Kim
 *
 */
public class OSS4HomeManager {
	private  Logger logger = null;
	private boolean debug = true;
	
	private static OSS4HomeManager instance = null;
	private static int syncCount = 0;
	
	private ControllerManager controllerManager = null;
	private GroupManager groupManager = null;
	private AutomationManager automationManager = null;
	private SystemSettings systemSettings = null;
	private EventLog eventLog = null;

	private OSS4HomeManager()   {
	    try {
	    	logger = org.apache.logging.log4j.LogManager.getRootLogger();
	      } catch( Exception ex ) {
	        System.out.println( ex );
	      }
		
		try
		{
			controllerManager = new ControllerManager();
			groupManager = new GroupManager();
			automationManager = new AutomationManager();
			systemSettings = new SystemSettings();
			eventLog = new EventLog();
		}catch(Exception ex)
		{
		   logger.log(Level.ERROR,  ex);	
		}
		
	}

	
	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}


	public static OSS4HomeManager getInstance()   {
		synchronized (OSS4HomeManager.class) {
			if (instance == null) {
				instance = new OSS4HomeManager();
			}

		}
		return instance;
	}
	
	
	public void writeEventLog(String message)
	{
		eventLog.addLog(message);
	}
	
	public void writeDebugEventLog(String message)
	{
		if(debug)
			eventLog.addLog(message);
	}
	
	public EventLogDTO getEventLog()
	{
		return eventLog.getLogs();
	}



	/**
	 * Legt einen neuen Controller mit den Werten des �bergebenen ControllerDTOs
	 * an.
	 * 
	 * @param controllerDTO
	 * @throws OSS4HomeJpaException 
	 */
	public void addController(ControllerDTO controllerDTO) throws OSS4HomeJpaException {
		Controller con_controller = new Controller();
		con_controller.setDataFromControllerDTO(controllerDTO);
		controllerManager.addController(con_controller);
	}

	/**
	 * Updated einen Controller mit den Werten des �bergebenen ControllerDTOs.
	 * 
	 * @param dto
	 * @throws OSS4HomeJpaException 
	 */
	public void updateController(ControllerDTO dto) throws OSS4HomeJpaException {
		Controller con_controller = controllerManager.getController(Integer.parseInt(dto.getId()));
		con_controller.setDataFromControllerDTO(dto);
		con_controller.saveToDb();
	}

	public List<ControllerDTO> getControllers() {
		List<ControllerDTO> list = new Vector<ControllerDTO>();
		for (Controller controller : controllerManager.getAllController()) {
			list.add(controller.getAsControllerDTO());
		}
		return list;

	}

	/**
	 * L�scht den Controller dessen Id gleich der �bergebenen Id entspricht.
	 * 
	 * @param id
	 * @throws OSS4HomeException 
	 */
	public void deleteController(int id) throws OSS4HomeException {
		automationManager.deleteRulesInfoFromController(id);
		groupManager.deleteDeviceInfosFromController(id);
		controllerManager.deleteController(id);
	}

	/**
	 * Updated die Config und Values aller Devices mit den gelieferten Werten
	 * des jeweiligen OneWireControllers.
	 * 
	 * @throws OSS4HomeJpaException
	 */
	public void sync() throws OSS4HomeJpaException {
		
		if(syncCount== 0)
			this.syncDeviceConfig();
		this.syncDeviceValues(true);
		automationManager.doAuto();
		if(syncCount > 100)
		{
			syncCount = 0;
		}
		else
			syncCount++;
	}

	/**
	 * Updated die Values aller Devices mit den gelieferten Werten des
	 * jeweiligen OneWireControllers.
	 * 
	 * @throws OSS4HomeJpaException
	 */
	private void syncDeviceValues(boolean system) throws OSS4HomeJpaException {
		List<Controller> controllerList = this.controllerManager.getAllController();
		for (Controller controller : controllerList) {
			List<BcDeviceValueDTO> deviceValues;
			try {
				deviceValues = controller.getControllerWsHandler().getDeviceValue();
				for (BcDeviceValueDTO deviceValueDTO : deviceValues) {
					Device currentdevice = controller.getDevice(deviceValueDTO.getId());
					if (currentdevice != null) {
						currentdevice.setValueFromController(deviceValueDTO,system);	
					}
				}
			} catch (OSS4HomeControllerWSException e) {
				OSS4HomeManager.getInstance().getLogger().log(Level.ERROR, e);
			}
			
		}
	}

	/**
	 * Updated die Config aller Devices mit den gelieferten Werten des
	 * jeweiligen OneWireControllers.
	 * 
	 * @throws OSS4HomeJpaException
	 */
	public void syncDeviceConfig() throws OSS4HomeJpaException {
		List<Controller> controllerList = this.controllerManager.getAllController();
		if(controllerList == null)
		{
			return;
		}
		for (Controller controller : controllerList) {
			List<BcDeviceConfigDTO> deviceConfigs;
			try {
				deviceConfigs = controller.getControllerWsHandler().getDeviceConfig();
				for (BcDeviceConfigDTO deviceConfigDTO : deviceConfigs) {
					Device currentDevice = controller.getDevice(deviceConfigDTO.getId());
					if (currentDevice == null) {
						currentDevice = new Device();
						currentDevice.setId(deviceConfigDTO.getId());
						currentDevice.setController(controller);
						currentDevice.setConfigFromController(deviceConfigDTO);
						controller.addDevice(currentDevice);
					} else {
						currentDevice.setConfigFromController(deviceConfigDTO);
						currentDevice.saveToDb();
					}
				}
			} catch (OSS4HomeControllerWSException e) {
				// TODO Loggin
			}

		}
	}

	/**
	 * Gibt eine Liste der DeviceConfigDTOs aller Devices zur�ck.
	 * 
	 * @return
	 */
	public List<DeviceConfigDTO> getDeviceConfigs() {
		List<DeviceConfigDTO> dtoList = new Vector<DeviceConfigDTO>();
		for (Controller controller : controllerManager.getAllController()) {
			for (Device device : controller.getDevices()) {
				dtoList.add(device.getAsDeviceConfigDTO());
			}
		}
		return dtoList;
	}

	/**
	 * Gibt eine Liste der DeviceValueDTOs aller Devices zur�ck die Aktiv sind.
	 * 
	 * @return
	 */
	public List<DeviceValueDTO> getDevicesValues() {
		List<DeviceValueDTO> dtoList = new Vector<DeviceValueDTO>();
		for (Controller controller : controllerManager.getAllController()) {
			for (Device device : controller.getDevices()) {
				if (device.isActive()) {
					dtoList.add(device.getAsDeviceValueDTO());
				}
			}
		}
		return dtoList;
	}

	/**
	 * Gibt eine Liste von Device DTOs zur�ck deren Typ dem �bergebenen Typ
	 * entspricht und aktiv sind oder alle Devices falls der �bergebene Typ
	 * ung�ltig ist.
	 * 
	 * @param typeString
	 * @return
	 */
	public List<DeviceValueDTO> getDevicesValues(String typeString) {
		DeviceTyp deviceTyp;
		if (typeString.equals(DeviceTyp.RELAIS.toString())) {
			deviceTyp = DeviceTyp.RELAIS;
		} else if (typeString.equals(DeviceTyp.SENSOR.toString())) {
			deviceTyp = DeviceTyp.SENSOR;
		} else if (typeString.equals(DeviceTyp.SWITCH.toString())) {
			deviceTyp = DeviceTyp.SWITCH;
		} else { // Wenn typeString nicht g�ltig ist rufe getDeviceValues auf.
			return this.getDevicesValues();
		}
		List<DeviceValueDTO> dtoList = new Vector<DeviceValueDTO>();
		for (Controller controller : controllerManager.getAllController()) {
			for (Device device : controller.getDevices()) {
				if (device.getTyp() == deviceTyp && device.isActive()) { 
					dtoList.add(device.getAsDeviceValueDTO());
				}
			}
		}
		return dtoList;
	}

	public void setDeviceValue(DeviceValueDTO dto) throws OSS4HomeException {
		setDeviceValue(dto,true);
	}
	
	/**
	 * Setzt den Wert eines BcDevices auf den Wert des �bergebenen
	 * DeviceValueDTOs @param dto @return @throws OSS4HomeException @throws
	 */
	public void setDeviceValue(DeviceValueDTO dto,boolean reload) throws OSS4HomeException {
		controllerManager.getController(Integer.parseInt(dto.getControllerId())).getDevice(dto.getId())
				.setValueFromClient(dto);
		if(reload)
			syncDeviceValues(!reload);
	}

	/**
	 * 
	 * @param dto
	 * @return @throws OSS4HomeException @throws
	 */
	public void setDeviceConfig(DeviceConfigDTO dto) throws OSS4HomeException {
		controllerManager.getController(Integer.parseInt(dto.getControllerId())).getDevice(dto.getId())
				.setConfigFromClient(dto);
	}

	/**
	 * Returns a Device or Null if either the Controller or the Device can not
	 * be found
	 * 
	 * @param deviceId
	 * @param controllerId
	 * @return
	 */
	public Device getDevice(String deviceId, String controllerId) {
		Controller controller = this.controllerManager.getController(Integer.parseInt(controllerId));
		if (controller == null) {
			return null;
		} else {
			Device device = controller.getDevice(deviceId);
			if (device == null) {
				return null;
			} else {
				return device;
			}
		}

	}

	public ControllerManager getControllerManager() {
		return controllerManager;
	}

	/**
	 * Liefert eine Liste mit allen GroupConfigDTOs
	 * 
	 * @return
	 */
	public List<GroupConfigDTO> getGroupConfigs() {
		List<GroupConfigDTO> groups = groupManager.getGroupConfigDTOs();
		for(GroupConfigDTO group : groups )
		{
			for (DeviceInfoDTO value : group.getDevices()) {
				value.setName(controllerManager.getController(Integer.parseInt(value.getControllerId())).getDevice(value.getDeviceId()).getName());
			}
		}
		return groups;
	}

	/**
	 * Legt eine neue Gruppe aus den Werten des �bergebenen DTOs an und legt
	 * diese im GroupManager ab.
	 * 
	 * @param groupDTO
	 * @throws OSS4HomeJpaException
	 */
	public void addGroup(GroupConfigDTO groupDTO) throws OSS4HomeJpaException {
		Group group = new Group();
		group.setName(groupDTO.getName());
		List<DeviceInfo> list = new Vector<DeviceInfo>();
		for (DeviceInfoDTO dto : groupDTO.getDevices()) {
			DeviceInfo info = new DeviceInfo();
			info.setDeviceId(dto.getDeviceId());
			info.setControllerId(Integer.parseInt(dto.getControllerId()));
			info.setGroup(group);
			list.add(info);
		}
		group.setDeviceInfos(list);
		groupManager.addGroup(group);

	}

	/**
	 * Sagt dem GroupManager er soll die jeweilige Gruppe mit dem �bergebenen
	 * DTO Updaten.
	 * 
	 * @param groupDTO
	 * @throws OSS4HomeJpaException
	 */
	public void updateGroupConfig(GroupConfigDTO groupDTO) throws OSS4HomeJpaException {
		groupManager.updateGroup(groupDTO);

	}

	/**
	 * Sagt dem GroupManager das er die Gruppe deren Id gleich der �bergebenen
	 * Id ist, l�schen soll.
	 * 
	 * @param id
	 * @throws OSS4HomeException 
	 */
	public void deleteGroup(int id) throws OSS4HomeException {
		automationManager.deleteAutoActionFromGroup(id);
		groupManager.deleteGroup(id);
	}

	/**
	 * Gibt eine Liste mit GroupValueDTOs zur�ck
	 * 
	 * @return
	 */
	public List<GroupValueDTO> getGroupValues() {
		List<GroupValueDTO> dtoList = new Vector<GroupValueDTO>();

		List<Group> groups = groupManager.getGroups();
		// Wir legen f�r jede Gruppe eine GroupValueDTO an.
		for (Group group : groups) {
			GroupValueDTO dto = new GroupValueDTO();
			dto.setId(new Integer(group.getId()).toString());
			dto.setName(group.getName());
			GroupValue groupvalue = null;
			List<DeviceValueDTO> devlist = new Vector<DeviceValueDTO>();

			// Wir pr�fen die Werte der Devices(Relais) um den GroupValue
			// festzulegen
			for (DeviceInfo deviceInfo : group.getDeviceInfos()) {
				Device device = controllerManager.getController(deviceInfo.getControllerId())
						.getDevice(deviceInfo.getDeviceId());
				devlist.add(device.getAsDeviceValueDTO());
				
				if(groupvalue != GroupValue.UNDEFINE)
				{

				// Wenn das Device kein Relais ist, �berspringen wir es
				if (device.getTyp() != DeviceTyp.RELAIS) {
					continue;
				}
				// Im ersten durchlauf wird der GroupValue durch den ersten Wert
				// festgelegt.
				if (groupvalue == null) {
					if (device.getValue().toLowerCase().equals((GroupValue.OFF).toString().toLowerCase())) {
						groupvalue = GroupValue.OFF;
					} else if (device.getValue().toLowerCase().equals((GroupValue.ON).toString().toLowerCase())) {
						groupvalue = GroupValue.ON;
					}
				} else {
					// Wenn wir auf ON sind und ein Device ist OFF, setzten wir
					// UNDEFINE und sind fertig f�r die Gruppe
					if (groupvalue == GroupValue.ON) {
						if (device.getValue().toLowerCase().equals((GroupValue.OFF).toString().toLowerCase())) {
							groupvalue = GroupValue.UNDEFINE;
							continue;
						}
					}
					// Hier der Fall das wir auf OFF sind
					else {
						if (device.getValue().toLowerCase().equals((GroupValue.ON).toString().toLowerCase())) {
							groupvalue = GroupValue.UNDEFINE;
							continue;
						}
					}
				
				}
				}

				dto.setDevices(devlist);
			} // End For DeviceInfos

			// Wenn die Gruppe gar keine Devices enthielt setzen wir den Wert
			// auf UNDEFINE
			if (groupvalue == null || groupvalue == GroupValue.UNDEFINE) {
				groupvalue = GroupValue.OFF;
			}
			dto.setValue(groupvalue);
			// Das GroupValueDTO zu unserer Liste hinzuf�gen
			dtoList.add(dto);
		} // Ende For Groups
		return dtoList;
	}

	/**
	 * �ndert den Wert aller Devices der Gruppe
	 * 
	 * @param groupValueDTO
	 * @throws OSS4HomeException
	 */
	public void setGroupValue(GroupValueDTO groupValueDTO) throws OSS4HomeException {
		Group group = groupManager.getGroup(Integer.parseInt(groupValueDTO.getId()));
		
		 
		if(groupValueDTO.getValue() == GroupValue.TOGGLE)
		{
			boolean allOn = true;
			for (DeviceInfo deviceInfo : group.getDeviceInfos()) {
				Device curdev = getDevice(deviceInfo.getDeviceId(), new Integer(deviceInfo.getControllerId()).toString());
				if(curdev != null)
				{
					if(curdev.getValue() == "OFF")
						allOn = false;
				}
			}
			if(allOn)
				groupValueDTO.setValue(GroupValue.OFF);
			else
				groupValueDTO.setValue(GroupValue.ON);
		
		}
		
		
		for (DeviceInfo deviceInfo : group.getDeviceInfos()) {
			DeviceValueDTO dto = new DeviceValueDTO();
			dto.setId(deviceInfo.getDeviceId());
			dto.setValue(groupValueDTO.getValue().toString());
			dto.setControllerId(new Integer(deviceInfo.getControllerId()).toString());
			setDeviceValue(dto);
		}
		syncDeviceValues(false);

	}

	/**
	 * Gibt eine Gruppe zur�ck deren Id der �bergebenen Id entspricht.
	 * 
	 * @param id
	 * @return
	 */
	public Group getGroup(int id) {
		return this.groupManager.getGroup(id);
	}

	public List<AutomationRuleDTO> getAutomatioinRules() {
		return this.automationManager.getAutomationRuleDTOs();
	}

	public void addAutomationRule(AutomationRuleDTO automationDTO) throws OSS4HomeJpaException {
		this.automationManager.addAutomationRule(automationDTO);

	}

	public void updateAutomationRule(AutomationRuleDTO automationDTO) throws OSS4HomeJpaException {
		this.automationManager.updateAutomationRule(automationDTO);
		
	}

	/**
	 * L�scht eine AutomationRule deren Id der �bergebenen entspricht
	 * @param id
	 * @throws OSS4HomeJpaException
	 */
	public void deleteAutomationRule(int id) throws OSS4HomeJpaException {
		automationManager.deleteAutomationRule(id);
	}

	public List<MailTemplateDTO> getMailTemplates() {
		return this.automationManager.getMailTemplateDTOs();
	}

	public void deleteMailTemplate(int id) throws OSS4HomeException {
		this.automationManager.deleteMailTemplate(id);
		
	}

	public void addMailTemplate(MailTemplateDTO dto) throws OSS4HomeJpaException {
		this.automationManager.addMailTemplate(dto);
		
	}

	public void updateMailTemplate(MailTemplateDTO dto) throws OSS4HomeJpaException {
		this.automationManager.updateMailTemplate(dto);
		
	}

	public MailTemplate getMailTemplate(int id) {
		return this.automationManager.getMailTemplate(id);
	}

	public SystemSettingDTO getSystemSettings() {
		SystemSettingDTO systemSetting = new SystemSettingDTO();
		systemSetting.setSmtpHost(this.systemSettings.getSetting("mail.smtpHost"));
		systemSetting.setPort(this.systemSettings.getSetting("mail.port"));
		systemSetting.setFrom(this.systemSettings.getSetting("mail.from"));
		systemSetting.setUserName(this.systemSettings.getSetting("mail.user"));
		systemSetting.setPassword(this.systemSettings.getSetting("mail.password"));
		systemSetting.setAuth(Boolean.parseBoolean(this.systemSettings.getSetting("mail.auth") ));
		systemSetting.setSsl(Boolean.parseBoolean(this.systemSettings.getSetting("mail.ssl")));
		return systemSetting;
	}
	
	public void setSystemSettings(SystemSettingDTO settings) throws OSS4HomeJpaException {
		this.systemSettings.setSetting("mail.smtpHost",settings.getSmtpHost());
		this.systemSettings.setSetting("mail.port",settings.getPort());
		this.systemSettings.setSetting("mail.user",settings.getUserName());
		this.systemSettings.setSetting("mail.password",settings.getPassword());
		this.systemSettings.setSetting("mail.auth",Boolean.toString(settings.isAuth()));
		this.systemSettings.setSetting("mail.ssl",Boolean.toString(settings.isSsl()));
		this.systemSettings.setSetting("mail.from",settings.getFrom());
	}

}
