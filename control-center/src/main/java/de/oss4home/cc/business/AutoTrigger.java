//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;


import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.databind.deser.std.DateDeserializers.CalendarDeserializer;

import de.oss4home.cc.dto.AutoTriggerDTO;
import de.oss4home.cc.dto.AutomationTyp;
import de.oss4home.cc.dto.DeviceTyp;
import de.oss4home.exceptions.OSS4HomeException;

/**
 * Class AutoTrigger
 */
@Entity
public class AutoTrigger {

	//
	// Fields
	//
	@Id
	@TableGenerator(name = "triggerGen", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "triggerGen")
	private int id;
	private AutomationTyp typ;
	private String relationalOperator;
	private String value;
	private String deviceid;
	private int controllerid;

	@ManyToOne
	private AutomationRule rule;

	//
	// Constructors
	//
	public AutoTrigger() {
	};

	//
	// Methods
	//

	//
	// Accessor methods
	//

	/**
	 * Set the value of typ
	 * 
	 * @param newVar
	 *            the new value of typ
	 */
	public void setTyp(de.oss4home.cc.dto.AutomationTyp newVar) {
		typ = newVar;
	}

	/**
	 * @return the rule
	 */
	public AutomationRule getRule() {
		return rule;
	}

	/**
	 * @param rule the rule to set
	 */
	public void setRule(AutomationRule rule) {
		this.rule = rule;
	}

	/**
	 * Get the value of typ
	 * 
	 * @return the value of typ
	 */
	public de.oss4home.cc.dto.AutomationTyp getTyp() {
		return typ;
	}

	/**
	 * Set the value of relationalOperator
	 * 
	 * @param newVar
	 *            the new value of relationalOperator
	 */
	public void setRelationalOperator(String newVar) {
		relationalOperator = newVar;
	}

	/**
	 * Get the value of relationalOperator
	 * 
	 * @return the value of relationalOperator
	 */
	public String getRelationalOperator() {
		return relationalOperator;
	}

	/**
	 * Set the value of value
	 * 
	 * @param newVar
	 *            the new value of value
	 */
	public void setValue(String newVar) {
		value = newVar;
	}

	/**
	 * Get the value of value
	 * 
	 * @return the value of value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Set the value of deviceid
	 * 
	 * @param newVar
	 *            the new value of deviceid
	 */
	public void setDeviceid(String newVar) {
		deviceid = newVar;
	}

	/**
	 * Get the value of deviceid
	 * 
	 * @return the value of deviceid
	 */
	public String getDeviceid() {
		return deviceid;
	}

	/**
	 * Set the value of controllerid
	 * 
	 * @param newVar
	 *            the new value of controllerid
	 */
	public void setControllerid(int newVar) {
		controllerid = newVar;
	}

	/**
	 * Get the value of controllerid
	 * 
	 * @return the value of controllerid
	 */
	public int getControllerid() {
		return controllerid;
	}

	/**
	 * Set the value of id
	 * 
	 * @param newVar
	 *            the new value of id
	 */
	public void setId(int newVar) {
		id = newVar;
	}

	/**
	 * Get the value of id
	 * 
	 * @return the value of id
	 */
	public int getId() {
		return id;
	}

	//
	// Other methods
	//

	/**
	 * Updatet den AutoTrigger mit den Werten des DTOs
	 * 
	 * @param dto
	 */
	public void setFromAutoTriggerDTO(AutoTriggerDTO dto) {
		if(dto.getType() == AutomationTyp.SENSOR 
				||dto.getType() == AutomationTyp.SWITCHRELAIS)
		this.setControllerid(Integer.parseInt(dto.getContollerId()));
		this.setDeviceid(dto.getDeviceId());
		this.setRelationalOperator(dto.getRelationalOperator());
		this.setValue(dto.getValue());
		this.setTyp(dto.getType());
		try {
			this.id = Integer.parseInt(dto.getId());
		} catch (Exception e) {
			// TODO: handle exception
		}
		// thi
	}

	/**
	 * Gibt den AutoTrigger als AutoTriggerDTO zur�ck
	 * 
	 * @return
	 */
	public AutoTriggerDTO getAsAutoTriggerDTO() {
		AutoTriggerDTO dto = new AutoTriggerDTO();
		dto.setContollerId(new Integer(this.getControllerid()).toString());
		dto.setDeviceId(this.getDeviceid());
		dto.setId(new Integer(this.getId()).toString());
		dto.setRelationalOperator(this.getRelationalOperator());
		dto.setType(this.getTyp());
		dto.setValue(this.getValue());
		dto.setName(this.getName());
		return dto;
	}

	/**
	 * Pr�ft ob die bedingung des Triggers erf�llt ist.
	 * 
	 * @return
	 */
	public boolean isTriggered() {
		try {
			switch (this.getTyp()) {
			case SENSOR:
				return this.sensorTrigger();
			case SWITCHRELAIS:
				return this.switchRelaisTrigger();
			case TIME:
				return this.timeTrigger();
			default:
				return false;
			}
		} catch (OSS4HomeException e) {
			return false;
		}
	}

	/**
	 * TODO Sascha
	 * @return
	 */
	private boolean timeTrigger() {
		
		String[] data = value.split(" ");
		if(data.length == TimerTypes.values().length)
		{
			boolean state = true;
			for (TimerTypes type : TimerTypes.values()) {
				state = state && timeTrigger(data, type);				
			}
			return state;
		}

		return false;
		
	}
	
	private boolean timeTrigger(String [] data, TimerTypes type) {
		String dataCur =  data[type.getPos()];
		if(dataCur.contains("*"))
			return true;
		String[] numbersASString = dataCur.split(",");
		int []  numbers = new int[numbersASString.length];
		for(int x = 0; x < numbersASString.length; x++)
		{
			try
			{
			numbers[x] = Integer.parseInt(numbersASString[x]);
			}catch(Exception ex)
			{
				numbers[x] = -1;
			}
		}
		int currentValue = -1;
		Calendar cal = GregorianCalendar.getInstance( TimeZone.getTimeZone("CET") );
		if(type == TimerTypes.MIN)
		{
		    currentValue = cal.get(Calendar.MINUTE);
		}
		else if(type == TimerTypes.HOUR)
		{
			currentValue = cal.get(Calendar.HOUR_OF_DAY);
		}
		else if(type == TimerTypes.DAY)
		{
			currentValue = cal.get(Calendar.DAY_OF_MONTH);
		}
		else if(type == TimerTypes.MONTH)
		{
			currentValue = cal.get(Calendar.MONTH) + 1;
		}
		else if(type == TimerTypes.DAYOFWEEK)
		{
			currentValue = cal.get(Calendar.DAY_OF_WEEK) -1;
			if(currentValue == 0)
			{
				currentValue = 7;
			}
		}
		
		for (int i : numbers) {
			if(i == currentValue)
				return true;
			
		}
		return false;
		
		
		
		
	}
	
	


	/**
	 * Pr�ft bedingung f�r einen Schalter/Relais
	 * 
	 * @return
	 * @throws OSS4HomeException
	 */
	private boolean switchRelaisTrigger() throws OSS4HomeException {
		Device device;
		switch (this.getValue()) {
		case "ON":
			device = OSS4HomeManager.getInstance().getDevice(this.getDeviceid(),
					new Integer(this.getControllerid()).toString());
			if (device != null && device.getTyp() != DeviceTyp.SENSOR && device.isActive()) {
				return device.getValue().equals("ON");
			} else {
				return false;
			}
		case "OFF":
			device = OSS4HomeManager.getInstance().getDevice(this.getDeviceid(),
					new Integer(this.getControllerid()).toString());
			if (device != null && device.getTyp() != DeviceTyp.SENSOR && device.isActive()) {
				return device.getValue().equals("OFF");
			} else {
				return false;
			}
		case "TOGGLE":
			device = OSS4HomeManager.getInstance().getDevice(this.getDeviceid(),
					new Integer(this.getControllerid()).toString());
			if (device != null && device.getTyp() != DeviceTyp.SENSOR && device.isActive()) {
				return device.isWasToggled();
			} else {
				return false;
			}

		default:
			
			//break;
		}
		return false;
	}

	/**
	 * Pr�ft bedinung f�r einen Sensor
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean sensorTrigger() throws OSS4HomeException {
		Device device;
		double triggerValue = Double.parseDouble(this.getValue());
		double actualValue;

		switch (this.getRelationalOperator()) {
		case "=":
			device = OSS4HomeManager.getInstance().getDevice(this.getDeviceid(),
					new Integer(this.getControllerid()).toString());
			// Pr�fen auf null und DeviceTyp
			if (device != null && device.getTyp() == DeviceTyp.SENSOR && device.isActive()) {
				try{
				actualValue = Double.parseDouble(device.getValue());
				}catch(Exception ex)
				{
					return false;
				}
				if (actualValue == triggerValue) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		case "<>":
			device = OSS4HomeManager.getInstance().getDevice(this.getDeviceid(),
					new Integer(this.getControllerid()).toString());
			// Pr�fen auf null und DeviceTyp
			if (device != null && device.getTyp() == DeviceTyp.SENSOR && device.isActive()) {
				try{
					actualValue = Double.parseDouble(device.getValue());
				}catch(Exception ex)
				{
					return false;
				}
				if (actualValue != triggerValue) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		case "<":
			device = OSS4HomeManager.getInstance().getDevice(this.getDeviceid(),
					new Integer(this.getControllerid()).toString());
			// Pr�fen auf null und DeviceTyp
			if (device != null && device.getTyp() == DeviceTyp.SENSOR && device.isActive()) {
				try{
				actualValue = Double.parseDouble(device.getValue());
				}catch(Exception ex)
				{
					return false;
				}
				if (actualValue < triggerValue) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		case ">":
			device = OSS4HomeManager.getInstance().getDevice(this.getDeviceid(),
					new Integer(this.getControllerid()).toString());
			// Pr�fen auf null und DeviceTyp
			if (device != null && device.getTyp() == DeviceTyp.SENSOR && device.isActive()) {
				try
				{
				actualValue = Double.parseDouble(device.getValue());
				}catch(Exception ex)
				{
					return false;
				}
				if (actualValue > triggerValue) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		default:
			return false;
		}
	}

	/**
	 * Updated den AutoTrigger mit den werten des �bergebenen AutoTrigger. Wird
	 * vom DbManager verwendet.
	 * 
	 * @param updatedAutoTrigger
	 */
	public void updateDbRelevantData(AutoTrigger updatedAutoTrigger) {
		if (this.getId() == updatedAutoTrigger.getId()) {
			this.setControllerid(updatedAutoTrigger.getControllerid());
			this.setDeviceid(updatedAutoTrigger.getDeviceid());
			this.setTyp(updatedAutoTrigger.getTyp());
			this.setRelationalOperator(this.getRelationalOperator());
		}

	}

	public String getName() {
		String name = "";
		try {
			switch (this.getTyp()) {
			case SENSOR:
			case SWITCHRELAIS:
				Device dev  = OSS4HomeManager.getInstance().getDevice(deviceid, new Integer(controllerid).toString());
				if (dev != null) {
						name = "Device: " + dev.getName();
				}
				break;
			case GROUP:
				
			break;
			case MAIL:
				name = "MAIL";
			break;
			case TIME:
				name = "Zeitsteuerung";
			break;
			}
		} catch (Exception ex) {

		}
		return name;

	}

}
