//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

import de.oss4home.cc.data.DbManager;
import de.oss4home.cc.dto.AutoActionDTO;
import de.oss4home.cc.dto.AutoTriggerDTO;
import de.oss4home.cc.dto.AutomationRuleDTO;
import de.oss4home.cc.dto.AutomationTyp;
import de.oss4home.exceptions.OSS4HomeException;
import de.oss4home.exceptions.OSS4HomeJpaException;

/**
 * Class AutomationRule
 */
@Entity
public class AutomationRule {

	//
	// Fields
	//
	@Id
	@TableGenerator(name = "ruleGen", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "ruleGen")
	private int id;
	private String name;
	@OneToMany(mappedBy = "rule", fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private List<AutoTrigger> triggers;
	@OneToMany(mappedBy = "rule", fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private List<AutoAction> actions;
	private Boolean active;
	@Transient
	private Boolean lastState = false;

	//
	// Constructors
	//
	public AutomationRule() {
	};

	//
	// Methods
	//

	//
	// Accessor methods
	//

	/**
	 * Set the value of triggers
	 * 
	 * @param newVar
	 *            the new value of triggers
	 */
	public void setTriggers(List<AutoTrigger> newVar) {
		triggers = newVar;
	}

	/**
	 * Get the value of triggers
	 * 
	 * @return the value of triggers
	 */
	public List<AutoTrigger> getTriggers() {
		return triggers;
	}

	/**
	 * Set the value of actions
	 * 
	 * @param newVar
	 *            the new value of actions
	 */
	public void setActions(List<AutoAction> newVar) {
		actions = newVar;
	}

	/**
	 * Get the value of actions
	 * 
	 * @return the value of actions
	 */
	public List<AutoAction> getActions() {
		return actions;
	}

	/**
	 * Set the value of lastState
	 * 
	 * @param newVar
	 *            the new value of lastState
	 */
	public void setLastState(Boolean newVar) {
		lastState = newVar;
	}

	/**
	 * Get the value of lastState
	 * 
	 * @return the value of lastState
	 */
	public Boolean getLastState() {
		return lastState;
	}

	/**
	 * Set the value of id
	 * 
	 * @param newVar
	 *            the new value of id
	 */
	public void setId(int newVar) {
		id = newVar;
	}

	/**
	 * Get the value of id
	 * 
	 * @return the value of id
	 */
	public int getId() {
		return id;
	}

	//
	// Other methods
	//

	/**
	 * @param dto
	 */
	public void applyAutomationRuleDTO(AutomationRuleDTO dto) {
		
			this.setName(dto.getName());
			this.setLastState(false);
			this.setActive(dto.isActive());
			
			List<AutoTrigger> autoTriggerList = new Vector<AutoTrigger>();
			List<AutoAction> autoActionList = new Vector<AutoAction>();
			// F�ge Trigger hinzu
			for (AutoTriggerDTO triggerDTO : dto.getTrigger()) {
				AutoTrigger trigger = new AutoTrigger();
				trigger.setFromAutoTriggerDTO(triggerDTO);
				trigger.setRule(this);
				autoTriggerList.add(trigger);
			}
			this.setTriggers(autoTriggerList);
			
			// F�ge Rules hinzu
			for (AutoActionDTO actionDTO : dto.getActions()) {
				AutoAction action = new AutoAction();
				action.setFromAutoActionDTO(actionDTO);
				action.setRule(this);
				autoActionList.add(action);
			}
			this.setActions(autoActionList);
			
			
		
	}

	/**
	 * @return AutomationRuleDTO
	 */
	public AutomationRuleDTO getAsAutomationRuleDTO() {
		AutomationRuleDTO dto = new AutomationRuleDTO();
		dto.setActions(this.getAutoActionDTOs());
		dto.setTrigger(this.getAutoTriggerDTOs());
		dto.setName(this.getName());
		dto.setId(new Integer(this.getId()).toString());
		dto.setActive(this.isActive());
		return dto;
	}

	/**
	 * TODO
	 */
	public void doAuto() {
		for (AutoTrigger trigger : this.getTriggers()) {
			// Wenn einer der Trigger nicht erf�llt ist, beendet die Methode
			if(!trigger.isTriggered()){
				lastState = false;
				return;
			}
		}
		
		if(!lastState){
		
			OSS4HomeManager.getInstance().writeEventLog("Regel:"+this.name+" aktiv");
			lastState = true;
			
		for (AutoAction autoAction : this.getActions()) {
			if(!autoAction.doAction())
			{
				lastState = false;
			}
			OSS4HomeManager.getInstance().writeEventLog("Device:"+autoAction.getName()+" auf "+autoAction.getValue()+" geschalten");
			
		}
		
		
		}
		
	
		
	}

	/**
	 * Gibt den AutoTrigger zur�ck dessen Id der �bergebenen entspricht
	 * @param id
	 * @return
	 */
	public AutoTrigger getAutoTrigger(int id) {
		for (AutoTrigger trigger : this.getTriggers()) {
			if (trigger.getId() == id) {
				return trigger;
			}
		}
		return null;
	}

	/**
	 * Gibt die AutoAction zur�ck dessen Id der �bergebenen entspricht
	 * @param id
	 * @return
	 */
	public AutoAction getAutoAction(int id) {
		for (AutoAction action : this.getActions()) {
			if (action.getId() == id) {
				return action;
			}
		}
		return null;
	}

	/**
	 * Updated die AutomationRule mit den Werten der �bergebenen Automation
	 * Rule. Wird vom DbManager aufgerufen.
	 * 
	 * @param updatedRule
	 * @throws OSS4HomeJpaException 
	 */
	public void updateDbRelevantData(AutomationRule updatedRule) throws OSS4HomeJpaException {
		if (this.getId() == updatedRule.id) {

			Iterator<AutoTrigger> autoTriggerIterator = this.getTriggers().iterator();
			Iterator<AutoAction> autoActionIterator = this.getActions().iterator();

			// L�sche alle AutoTrigger die in der neuen AutomationRule nicht
			// enthalten sind.
			while (autoTriggerIterator.hasNext()) {
				AutoTrigger autoTrigger = (AutoTrigger) autoTriggerIterator.next();
				if (updatedRule.getAutoTrigger(autoTrigger.getId()) == null) {
					DbManager.getInstance().deleteAutoTrigger(autoTrigger.getId());
					autoTriggerIterator.remove();
				}
			}

			// Update vohandene AutoTrigger und f�ge neue hinzu
			for (AutoTrigger newAutoTrigger : updatedRule.getTriggers()) {

				AutoTrigger oldAutoTrigger = this.getAutoTrigger(newAutoTrigger.getId()); //inlast
				// Wenn es den AutoTrigger nicht gibt, ist dieser neu und wird
				// zur Liste hinzugef�gt.
				if (oldAutoTrigger == null) {
					this.triggers.add(newAutoTrigger);
				}
				// Wenn es den AutoTrigger schon gibt, werden dessen Werte
				// �bernommen.
				else {
					oldAutoTrigger.updateDbRelevantData(newAutoTrigger);//in
				}

			}

			// L�sche alle AutoAction die in der neuen AutomationRule nicht
			// enthalten sind.
			while (autoActionIterator.hasNext()) {
				AutoAction autoAction = (AutoAction) autoActionIterator.next();
				if (updatedRule.getAutoAction(autoAction.getId()) == null) {
					DbManager.getInstance().deleteAutoRule(autoAction.getId());
					autoActionIterator.remove();
				}
			}

			// Update vohandene AutoActions und f�ge neue hinzu
			for (AutoAction newAutoAction : updatedRule.getActions()) {

				AutoAction oldAutoAction = this.getAutoAction(newAutoAction.getId());
				// Wenn es den AutoTrigger nicht gibt, ist dieser neu und wird
				// zur Liste hinzugef�gt.
				if (oldAutoAction == null) {
					this.actions.add(newAutoAction);
				}
				// Wenn es den AutoTrigger schon gibt, werden dessen Werte
				// �bernommen.
				else {
					oldAutoAction.updateDbRelevantData(newAutoAction);
				}

			}
		}
	}

	/**
	 * Speichert das Objekt in der Datenbank und gibt es zur�ck
	 * @throws OSS4HomeJpaException 
	 */
	public AutomationRule saveToDb() throws OSS4HomeJpaException {

		return DbManager.getInstance().updateAutoRule(this);

	}

	/**
	 * Gibt eine Liste von AutoActionDTOs aller AutoActions dieser Klasse zur�ck
	 * @return
	 */
	private List<AutoActionDTO> getAutoActionDTOs() {
		List<AutoActionDTO> dtoList = new Vector<AutoActionDTO>();
		for (AutoAction autoAction : actions) {
			AutoActionDTO dto = autoAction.getAsAutoActionDTO();
			dtoList.add(dto);
		}

		return dtoList;
	}

	/**
	 * Gibt eine Liste von AutoTriggerDTOs aller AutoTrigger dieser Klasse zur�ck
	 * @return
	 */
	private List<AutoTriggerDTO> getAutoTriggerDTOs() {
		List<AutoTriggerDTO> dtoList = new Vector<AutoTriggerDTO>();
		for (AutoTrigger autoTrigger : triggers) {
			AutoTriggerDTO dto = autoTrigger.getAsAutoTriggerDTO();
			dtoList.add(dto);
		}
		return dtoList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
	public void  deleteAutoActionFromMailTemplate(int mail) throws OSS4HomeException
	{
		Iterator<AutoAction> actionIter = actions.iterator();
		while(actionIter.hasNext())
		{
			AutoAction action = actionIter.next();
			if(action.getTyp() == AutomationTyp.MAIL
					&& action.getValue().equals(new Integer(mail).toString()))
			{
				actionIter.remove();
			}
			
		}
		
	}
	
	public void  deleteAutoActionFromGroup(int group) throws OSS4HomeException
	{
		Iterator<AutoAction> actionIter = actions.iterator();
		while(actionIter.hasNext())
		{
			AutoAction action = actionIter.next();
			if(action.getTyp() == AutomationTyp.GROUP
					&& action.getGroupId() == group)
			{
				actionIter.remove();
			}
			
		}
		
	}
	public void  deleteRulesInfoFromController(int controller) throws OSS4HomeException
	{
		Iterator<AutoTrigger> triggerIter = triggers.iterator();
		Iterator<AutoAction> actionIter = actions.iterator();
		while(actionIter.hasNext())
		{
			AutoAction action = actionIter.next();
			if((action.getTyp() == AutomationTyp.SENSOR
					|| action.getTyp() == AutomationTyp.SWITCHRELAIS)
					&& action.getControllerId() == controller)
			{
				actionIter.remove();
			}
			
		}
		
		while(triggerIter.hasNext())
		{
			AutoTrigger trigger = triggerIter.next();
			if((trigger.getTyp() == AutomationTyp.SENSOR
					|| trigger.getTyp() == AutomationTyp.SWITCHRELAIS)
					&& trigger.getControllerid() == controller)
			{
				triggerIter.remove();
				this.active = false;
			}
			
		}
		saveToDb();
	}
}
