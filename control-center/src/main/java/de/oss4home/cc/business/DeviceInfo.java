//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;

import de.oss4home.cc.dto.DeviceInfoDTO;

/**
 * Stellvertreter Klasse f�r die Relation von Group und Device.
 * @author Kim
 *
 */
@Entity
public class DeviceInfo {

	@Id
	@TableGenerator(name="groupInfoGen", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator="groupInfoGen")
	private int id;
	
	private String deviceId;
	private int controllerId;
	
	@ManyToOne
	private Group parent;
	
	public DeviceInfo(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public int getControllerId() {
		return controllerId;
	}

	public void setControllerId(int controllerId) {
		this.controllerId = controllerId;
	}

	public Group getGroup() {
		return parent;
	}

	public void setGroup(Group group) {
		this.parent = group;
	}

	public DeviceInfoDTO getAsDeviceInfoDTO() {
		DeviceInfoDTO dto = new DeviceInfoDTO();
		dto.setId(new Integer(this.getId()).toString());
		dto.setDeviceId(getDeviceId());
		dto.setControllerId(new Integer(this.getControllerId()).toString());
		return dto;
	}

	public void updateDbRelevantData(DeviceInfo updatedDeviceInfo){
		if (this.getId() == updatedDeviceInfo.getId()) {
			this.setControllerId(updatedDeviceInfo.getId());
			this.setDeviceId(updatedDeviceInfo.getDeviceId());
		}
	}
}
