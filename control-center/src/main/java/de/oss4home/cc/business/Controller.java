//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;

import java.util.List;
import java.util.Vector;

import de.oss4home.cc.data.DbManager;
import de.oss4home.cc.dto.ControllerDTO;
import de.oss4home.cc.wshandler.ControllerWsHandler;
import de.oss4home.exceptions.OSS4HomeJpaException;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

/**
 * Klasse zum verwalten eines OSS4Home Controllers
 * @author Kim
 *
 */
@Entity
public class Controller {
	
	@Id
	@TableGenerator(name="conGen", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator="conGen")
	private int id; //SDB
	private String connectionString; //SDB
	private boolean  active; //SDB
	private String name; //SDB
	@Transient
	private ControllerWsHandler controllerWsHandler;
	@OneToMany(mappedBy="controller", fetch=FetchType.EAGER, cascade=CascadeType.PERSIST)
	private List<Device> devices = null; //SDB
	
	
	public Controller() {
		devices = new Vector<Device>();
	}
	
	/**
	 * F�gt ein Device zu diesem Controller hinzu.
	 * @param device
	 */
	public void addDevice(Device device)
	{
		if (device.getController() != this ) {
			device.setController(this);
		}
		devices.add(device);
	}
	
	/**
	 * Liefert ein Devices des Controllers zur�ck die �bergebene Id besitzt oder null falls es nicht existiert.
	 * @param id
	 * @return
	 */
	public Device getDevice(String id)
	{
		for (Device device : devices) {
			if(device.getId().equals(id))
				return device;
		}
		return null;
				
	}
		
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the connectionString
	 */
	public String getConnectionString() {
		return connectionString;
	}
	/**
	 * @param connectionString the connectionString to set
	 */
	public void setConnectionString(String connectionString) {
		this.connectionString = connectionString;
		if(controllerWsHandler != null)
			 controllerWsHandler = null;
		controllerWsHandler = new ControllerWsHandler(connectionString);
		
	}
	/**
	 * @return the activ
	 */
	public boolean isActive() {
		return  active;
	}
	/**
	 * @param  active the  active to set
	 */
	public void setActive(boolean active) {
		this.active =  active;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the controllerWsHandler
	 */
	public ControllerWsHandler getControllerWsHandler() {
		if(controllerWsHandler == null)
			controllerWsHandler = new ControllerWsHandler(connectionString);
		return controllerWsHandler;
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	/**
	 * Updated den Controller mit den Daten des �bergebenen "updatedController". Wird von der DbManager Klasse
	 * aufgerufen.
	 * @param updatedController
	 */
	public void updateDbRelevantData(Controller updatedController) {
		if (this.id == updatedController.getId()) {
			this.setName(updatedController.getName());
			this.setConnectionString(updatedController.getConnectionString());
			this.setActive(updatedController.isActive());
			//this.setDevices(updatedController.getDevices());
			for(Device device: updatedController.getDevices())
			{
				Device old = this.getDevice(device.getId());
				if(old == null)
				{
					this.devices.add(device);
				}
				else
				{
					old.updateDbRelevantData(device);
				}
				
			}
		}
		
	}
	
	/**
	 * F�gt die Werte des ControllerDTO zu dem Controller hinzu.
	 * @param controller
	 */
	public void setDataFromControllerDTO(ControllerDTO controller)
	{
		setConnectionString(controller.getConnectionString());
		setName(controller.getName());
		setActive(controller.getActive());
	}
	
	/**
	 * Erzeugt ein ControllerDTO mit den Werten des Controller.
	 * @return
	 */
	public ControllerDTO getAsControllerDTO()
	{
		ControllerDTO dto = new ControllerDTO();
		dto.setId(new Integer(getId()).toString());
		dto.setActive(isActive());
		dto.setConnectionString(getConnectionString());
		dto.setName(getName());
		return dto;
	}
	
	/**
	 * Speichert den Controller in der Datenbank.
	 * @return 
	 * @throws OSS4HomeJpaException 
	 */
	public void saveToDb() throws OSS4HomeJpaException
	{
		DbManager.getInstance().updateController(this);
	}
	

}
