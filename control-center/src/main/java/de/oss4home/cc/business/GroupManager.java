//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import de.oss4home.cc.data.DbManager;
import de.oss4home.cc.dto.DeviceInfoDTO;
import de.oss4home.cc.dto.GroupConfigDTO;
import de.oss4home.exceptions.OSS4HomeJpaException;

/**
 * Diese Klasse verwaltet die Gruppen
 * @author Kim
 *
 */
public class GroupManager {
	
	private List<Group> groups = new Vector<Group>();
	
	public GroupManager() throws OSS4HomeJpaException{
		this.getAllGroupsFromDB();
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}
	
	/**
	 * Holt sich alle Gruppen aus der Datenbank.
	 * @throws OSS4HomeJpaException 
	 */
	private void getAllGroupsFromDB() throws OSS4HomeJpaException{
		groups.clear();
		groups = DbManager.getInstance().getGroups();		
	}
	
	/**
	 * L�scht die Gruppe, deren Id der �bergebenen entspricht, aus der Datenbank und aus dem GroupManager
	 * @param id
	 * @throws OSS4HomeJpaException 
	 */
	public void deleteGroup(int id) throws OSS4HomeJpaException{
		Iterator<Group> iterartor = groups.iterator();
		while (iterartor.hasNext()) {
			Group group = (Group) iterartor.next();
			if (group.getId() == id) {
				DbManager.getInstance().deleteGroup(group);
				iterartor.remove();
				break;
			}
			
		}
	}
	
	/**
	 * F�gt eine neue Gruppe zum GroupManager hinzu und speichert die neue Gruppe in der Datenbank.
	 * @param group
	 * @throws OSS4HomeJpaException 
	 */
	public void addGroup(Group group) throws OSS4HomeJpaException{
		if(this.getGroup(group.getId()) != null){ // Wir tun nichts wenn es schon eine Gruppe mit der selben Id gibt
			return;
		}
		// Speichert die Gruppe in der DB und im GroupManager.
		this.getGroups().add(DbManager.getInstance().addGroup(group));
	}
	
	/**
	 * Gibt die Gruppe zur�ck deren Id der �bergebenen entspricht oder null, wenn es keine solche Gruppe gibt.
	 * @param id
	 * @return
	 */
	public Group getGroup(int id){
		for (Group group : groups) {
			if (group.getId() == id) {
				return group;
			}
		}
		return null;
	}

	/**
	 * Updated die Gruppe mit den Werten aus dem GroupConfigDTO
	 * @param groupConfigDTO
	 * @throws OSS4HomeJpaException 
	 */
	public void updateGroup(GroupConfigDTO groupConfigDTO) throws OSS4HomeJpaException{
		Group group = this.getGroup(Integer.parseInt(groupConfigDTO.getId()));
		if(group != null){
			group.setName(groupConfigDTO.getName()); // Name Updaten
			List<DeviceInfo> deviceInfoList = new Vector<DeviceInfo>();
			// DeviceInfos Updaten
			for (DeviceInfoDTO deviceInfoDTO : groupConfigDTO.getDevices()) {
				DeviceInfo deviceInfo = new DeviceInfo();
				deviceInfo.setControllerId(Integer.parseInt(deviceInfoDTO.getControllerId()));
				deviceInfo.setDeviceId(deviceInfoDTO.getDeviceId());
				deviceInfo.setGroup(group);
				if(deviceInfoDTO.getId()!= null &&!deviceInfoDTO.getId().isEmpty())
				{
				     deviceInfo.setId(Integer.parseInt(deviceInfoDTO.getId()));
				}
				deviceInfoList.add(deviceInfo);
			}
			group.setDeviceInfos(deviceInfoList);
			DbManager.getInstance().updateGroup(group);
		}
	}

	/**
	 * Liefert eine Liste mit den GroupConfigDTOs aller Gruppen.
	 * @return
	 */
	public List<GroupConfigDTO> getGroupConfigDTOs(){
		List<GroupConfigDTO> list = new Vector<GroupConfigDTO>();
		List<Group> groups =  getGroups();
		for (Group group : groups) {
			list.add(group.getAsGroupConfigDTO());
		}
		return list;
	}
	
	public void deleteDeviceInfosFromController(int id) throws OSS4HomeJpaException
	{
		List<Group> groups =  getGroups();
		for (Group group : groups) {
			group.deleteDeviceInfosFromController(id);
		}
		
	}
}
