//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------


package de.oss4home.cc.business;

import javax.persistence.Entity;
import javax.persistence.Id;

import de.oss4home.cc.data.DbManager;
import de.oss4home.exceptions.OSS4HomeJpaException;

@Entity
public class Setting {
	
	@Id
	private String settingkey;
	
	private String settingvalue;
	
	/**
	 * @return the key
	 */
	public String getKey() {
		return settingkey;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.settingkey = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return settingvalue;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.settingvalue = value;
	}

	public void save() throws OSS4HomeJpaException {
		DbManager.getInstance().updateSetting(this);
	}
	

}
