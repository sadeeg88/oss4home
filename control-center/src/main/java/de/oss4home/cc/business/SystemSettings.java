//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import de.oss4home.cc.data.DbManager;
import de.oss4home.exceptions.OSS4HomeJpaException;


public class SystemSettings {
	
	private ConcurrentHashMap<String,Setting> settings = new ConcurrentHashMap<String,Setting>();
	
	public SystemSettings() throws OSS4HomeJpaException
	{
		List<Setting> list = DbManager.getInstance().getSettings();
		if (list != null) {
			for (Setting setting : list) {
				this.settings.put(setting.getKey(), setting);
			}
		}
		
	}
	
	public String getSetting(String key)
	{
		Setting currentSetting =  settings.get(key);
		if(currentSetting != null)
			return currentSetting.getValue();
		return "";
	}
	
	public void setSetting(String key , String value) throws OSS4HomeJpaException
	{
		Setting currentSetting =  settings.get(key);
		if(currentSetting != null)
		{
			currentSetting.setValue(value);
			currentSetting.save();
		}
		else
		{
			currentSetting = new Setting();
			currentSetting.setKey(key);
			currentSetting.setValue(value);
			settings.put(key, currentSetting);
			currentSetting.save();
		}
	}
	


	
	
	





}
