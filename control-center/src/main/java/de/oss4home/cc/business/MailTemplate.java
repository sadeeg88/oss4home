//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.TableGenerator;

import de.oss4home.cc.data.DbManager;
import de.oss4home.cc.dto.MailTemplateDTO;
import de.oss4home.exceptions.OSS4HomeJpaException;

/**
 * Class MailTemplate
 */
@Entity
public class MailTemplate {

	//
	// Fields
	//
	
	@Id
	@TableGenerator(name = "mailGen", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "mailGen")
	private int id;
	private String name;
	private String toMail;
	private String subject;
	
	@Lob
	private String body;

	//
	// Constructors
	//
	public MailTemplate() {
	};

	//
	// Methods
	//

	//
	// Accessor methods
	//

	/**
	 * Set the value of id
	 * 
	 * @param newVar
	 *            the new value of id
	 */
	public void setId(int newVar) {
		id = newVar;
	}

	/**
	 * Get the value of id
	 * 
	 * @return the value of id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Set the value of name
	 * 
	 * @param newVar
	 *            the new value of name
	 */
	public void setName(String newVar) {
		name = newVar;
	}

	/**
	 * Get the value of name
	 * 
	 * @return the value of name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the value of subject
	 * 
	 * @param newVar
	 *            the new value of subject
	 */
	public void setSubject(String newVar) {
		subject = newVar;
	}

	/**
	 * Get the value of subject
	 * 
	 * @return the value of subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Set the value of body
	 * 
	 * @param newVar
	 *            the new value of body
	 */
	public void setBody(String newVar) {
		body = newVar;
	}

	/**
	 * Get the value of body
	 * 
	 * @return the value of body
	 */
	public String getBody() {
		return body;
	}

	//
	// Other methods
	//

	/**
	 * Updated das MailTempled mit den Werten des übergebenen
	 * @param updatedMail
	 */
	public void updateDbRelevantData(MailTemplate updatedMail){
		if (this.getId() == updatedMail.getId()) {
			this.setBody(updatedMail.getBody());
			this.setName(updatedMail.getName());
			this.setSubject(updatedMail.getSubject());
			this.setToMail(updatedMail.getToMail());
		}
	}

	public String getToMail() {
		return toMail;
	}

	public void setToMail(String toMail) {
		this.toMail = toMail;
	}

	public MailTemplateDTO getAsMailTemplateDTO() {
		MailTemplateDTO dto = new MailTemplateDTO();
		dto.setBody(this.getBody());
		dto.setId(new Integer(this.getId()).toString());
		dto.setName(this.getName());
		dto.setSubject(this.getSubject());
		dto.setTo(this.getToMail());
		return dto;
		
	}
	
	/**
	 * Updated das MailTemplate mit Werten aus dem übergebenen DTO.
	 * @param dto
	 */
	public void updateWithMailTemplateDTO(MailTemplateDTO dto){
		if(this.getId() == Integer.parseInt(dto.getId())){
			this.setBody(dto.getBody());
			this.setName(dto.getName());
			this.setSubject(dto.getSubject());
			this.setToMail(dto.getTo());
		}
	}

	public void saveToDb() throws OSS4HomeJpaException {
		DbManager.getInstance().updateMailTemplate(this);
		
	}
}
