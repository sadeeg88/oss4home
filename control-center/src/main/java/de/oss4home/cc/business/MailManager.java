//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;


import java.security.GeneralSecurityException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.Level;

import com.sun.mail.util.MailSSLSocketFactory;

import de.oss4home.cc.dto.SystemSettingDTO;

public class MailManager {
	static void sendMail(SystemSettingDTO setting, MailTemplate template) throws AddressException, MessagingException, GeneralSecurityException
	{
		    Properties props = new Properties();
		    final String username = setting.getUserName();
		    final String password = setting.getPassword();
		    
			props.put("mail.smtp.host", setting.getSmtpHost());
			if(setting.isSsl())
			{
		        MailSSLSocketFactory socketFactory;
			
					socketFactory = new MailSSLSocketFactory();
					socketFactory.setTrustAllHosts(true);
					props.put("mail.imaps.ssl.socketFactory", socketFactory);
					props.put("mail.smtp.socketFactory.port", setting.getPort());

			
	
			}
			props.put("mail.smtp.auth", setting.isAuth() ? "true" :"false");
			props.put("mail.smtp.port", setting.getPort());


		    Session session = Session.getInstance(props,
		            new javax.mail.Authenticator() {
		                protected PasswordAuthentication getPasswordAuthentication() {
		                    return new PasswordAuthentication(username, password);
		                }
		            });
		    session.setDebug(true);
		    
		

		        Message message = new MimeMessage(session);
		        message.setFrom(new InternetAddress(setting.getFrom()));
		        message.setRecipients(Message.RecipientType.TO,
		                InternetAddress.parse(template.getToMail()));
		        message.setSubject(template.getSubject());
		        message.setText(template.getBody());
		        
		        Transport.send(message);
		}
		
	}


