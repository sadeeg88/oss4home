//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------


package de.oss4home.cc.business;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import de.oss4home.cc.dto.EventLogDTO;

public class EventLog {
	private List<String> logs = new Vector<String>();
	
	public void addLog(String log)
	{
		String message;
		Date d = new Date();

		DateFormat df;
		df = DateFormat.getDateTimeInstance( /* dateStyle */ DateFormat.FULL,
		                                     /* timeStyle */ DateFormat.MEDIUM );
	 
		message = df.format(d)+"\r\n"+log;
		logs.add(0, message);
		if(log.length() > 100)
			logs.remove(100);
	}
	
	public EventLogDTO getLogs()
	{
		String message = "";
		EventLogDTO dto = new EventLogDTO();
		for (String string : logs) {
			message= message + string+ "\r\n\r\n\r\n";			
		}
		dto.setMessages(message);
		return dto;
	}

}
