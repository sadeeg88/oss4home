//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import de.oss4home.cc.data.DbManager;
import de.oss4home.exceptions.OSS4HomeJpaException;


/**
 * Diese Klasse verwaltete die Controller
 * @author Sascha Deeg
 */
public class ControllerManager {
	
	private ConcurrentHashMap<Integer, Controller> data = new ConcurrentHashMap<Integer , Controller>();
	
	public ControllerManager() throws OSS4HomeJpaException {
		this.getControllerFromDB();
	}
	
	
	/**
	 * F�gt eine Liste an Controllern zum ControllerManager
	 * @param controllers
	 * @throws OSS4HomeJpaException 
	 */
	private void getControllerFromDB() throws OSS4HomeJpaException
	{
		this.data.clear();
		List<Controller> controllerList = DbManager.getInstance().getController();
		for (Controller controller : controllerList) {
			this.data.put(controller.getId(), controller);
		}
	}
	
	/**
	 * F�gt einen Controller zur Datenbank und zum ControllerManager hinzu.
	 * @param controller
	 * @throws OSS4HomeJpaException 
	 */
	public void addController(Controller controller) throws OSS4HomeJpaException
	{
		// Falls der Controller schon existiert passiert nichts.
		if(this.getController(controller.getId()) == null){
		DbManager.getInstance().addController(controller);
		this.data.put(controller.getId(), controller);
		}
	}
	
	/**
	 * L�schen aller Controller 
	 */
	private void clear()
	{
		data.clear();
	}
	
	/**
	 * L�schen eines Controllers anhand der ID
	 * @param id
	 * @throws OSS4HomeJpaException 
	 */
	public void deleteController(int id) throws OSS4HomeJpaException
	{
		DbManager.getInstance().deleteController(getController(id));
		data.remove(id);
	}
	
	/**
	 * Abrufen eines Controllers anhand der ID
	 * @param id
	 * @return
	 */
	public Controller getController(int id)
	{
		return data.get(id);
	}
	
	/**
	 * Liefert alle Controller als Liste
	 * @return
	 */
	public List<Controller> getAllController()
	{
		return new Vector<Controller>(data.values());
	}

}
