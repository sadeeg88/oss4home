//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import de.oss4home.cc.data.DbManager;
import de.oss4home.cc.dto.AutomationRuleDTO;
import de.oss4home.cc.dto.MailTemplateDTO;
import de.oss4home.exceptions.OSS4HomeException;
import de.oss4home.exceptions.OSS4HomeJpaException;

/**
 * Class AutomationManager
 */
public class AutomationManager {

	//
	// Fields
	//

	private ConcurrentHashMap<Integer, AutomationRule> autoRules = new ConcurrentHashMap<Integer, AutomationRule>();
	private ConcurrentHashMap<Integer, MailTemplate> mailTemplates = new ConcurrentHashMap<Integer, MailTemplate>();

	//
	// Constructors
	//
	public AutomationManager() throws OSS4HomeJpaException {
		this.loadRulesFromDb();
		this.loadTemplatesFromDb();
	}

	//
	// Methods
	//

	//
	// Accessor methods
	//

	/**
	 * Set the value of autorule
	 * 
	 * @param newVar
	 *            the new value of autorule
	 */
	public void setAutoRule(ConcurrentHashMap<Integer, AutomationRule> newVar) {
		autoRules = newVar;
	}

	/**
	 * Get the value of autorule
	 * 
	 * @return the value of autorule
	 */
	public ConcurrentHashMap<Integer, AutomationRule> getAutoRules() {
		return autoRules;
	}

	/**
	 * Set the value of mailTemplates
	 * 
	 * @param newVar
	 *            the new value of mailTemplates
	 */
	public void setMailTemplates(ConcurrentHashMap<Integer, MailTemplate> newVar) {
		mailTemplates = newVar;
	}

	/**
	 * Get the value of mailTemplates
	 * 
	 * @return the value of mailTemplates
	 */
	public ConcurrentHashMap<Integer, MailTemplate> getMailTemplates() {
		return mailTemplates;
	}

	//
	// Other methods
	//

	/**
	 * TODO
	 */
	public void doAuto() {
		for (AutomationRule rule : this.getAutoRules().values()) {
			if(rule.isActive())
				rule.doAuto();
		}
	}

	/**
	 * L�d alle AutomationRules von der Datenbank
	 * @throws OSS4HomeJpaException
	 */
	private void loadRulesFromDb() throws OSS4HomeJpaException {
		autoRules.clear();
		List<AutomationRule> ruleList = DbManager.getInstance().getAutoRules();
		for (AutomationRule automationRule : ruleList) {
			this.autoRules.put(automationRule.getId(), automationRule);
		}
	}

	/**
	 * L�d alle MailTemplates von der Datenbank
	 * @throws OSS4HomeJpaException
	 */
	private void loadTemplatesFromDb() throws OSS4HomeJpaException {
		mailTemplates.clear();
		List<MailTemplate> templateList = DbManager.getInstance().getMailTemplates();
		for (MailTemplate mailTemplate : templateList) {
			this.mailTemplates.put(mailTemplate.getId(), mailTemplate);
		}

	}

	/**
	 * Gibt eine Liste von AutomationRuleDTOs aller AutomationRules zur�ck
	 * @return
	 */
	public List<AutomationRuleDTO> getAutomationRuleDTOs() {
		List<AutomationRuleDTO> list = new Vector<AutomationRuleDTO>();
		for (AutomationRule rule : this.getAutoRules().values()) {
			list.add(rule.getAsAutomationRuleDTO());
		}
		return list;
	}

	/**
	 * F�gt eine neue AutomationRule zum Manager hinzu, und speichert diesen in
	 * der Datenbank.
	 * 
	 * @param automationDTO
	 * @throws OSS4HomeJpaException
	 */
	public void addAutomationRule(AutomationRuleDTO automationDTO) throws OSS4HomeJpaException {
		if (automationDTO.getId() == null || automationDTO.getId().isEmpty()) {
			AutomationRule rule = new AutomationRule();
			rule.applyAutomationRuleDTO(automationDTO);

			rule.saveToDb();

			try {
				this.getAutoRules().put(rule.getId(), rule);
			} catch (Exception e) {
				return;
			}

		}
	}

	/**
	 * Updated die AutomationRule deren Id der des DTOs entspricht und speichert die �nderung in der Datenbank.
	 * @param automationDTO
	 * @throws OSS4HomeJpaException
	 */
	public void updateAutomationRule(AutomationRuleDTO automationDTO) throws OSS4HomeJpaException {
		AutomationRule ruleToUpdate = this.getAutoRules().get(Integer.parseInt(automationDTO.getId()));
		if (ruleToUpdate != null) {
			ruleToUpdate.applyAutomationRuleDTO(automationDTO);
			ruleToUpdate.saveToDb();
		}

	}
	
	/**
	 * L�scht die AutomationRule aus dem AutomationManager sowie der Datenbank
	 * @param id
	 * @throws OSS4HomeJpaException
	 */
	public void deleteAutomationRule(int id) throws OSS4HomeJpaException {
		DbManager.getInstance().deleteAutoRule(this.getAutoRules().remove(id).getId());
	}

	public List<MailTemplateDTO> getMailTemplateDTOs(){
		List<MailTemplateDTO> dtoList = new Vector<MailTemplateDTO>();
		for (MailTemplate mailTemplate : this.getMailTemplates().values()) {
			dtoList.add(mailTemplate.getAsMailTemplateDTO());
		}
		return dtoList;
	}

	/**
	 * L�scht ein MailTemplate mit der angeben Id aus der Datenbank und dem AutomationManager.
	 * @param id
	 * @throws OSS4HomeException 
	 */
	public void deleteMailTemplate(int id) throws OSS4HomeException {
		deleteAutoActionFromMailTemplate(id);
		DbManager.getInstance().deleteMailTemplate(id);
		mailTemplates.remove(id);
		
	}

	/**
	 * F�gt ein MailTemplate zum AutomationManager sowie der Datenbank hinzu.
	 * @param dto
	 * @throws OSS4HomeJpaException
	 */
	public void addMailTemplate(MailTemplateDTO dto) throws OSS4HomeJpaException {
		MailTemplate mailTemplate = new MailTemplate();
		mailTemplate.setBody(dto.getBody());
		mailTemplate.setName(dto.getName());
		mailTemplate.setSubject(dto.getSubject());
		mailTemplate.setToMail(dto.getTo());
		
		mailTemplate = DbManager.getInstance().addMailTemplate(mailTemplate);
		
		this.getMailTemplates().put(mailTemplate.getId(), mailTemplate);
		
	}

	public void updateMailTemplate(MailTemplateDTO dto) throws OSS4HomeJpaException {
		int key = Integer.parseInt(dto.getId());
		MailTemplate mailTemplate = this.getMailTemplates().get(key);
		mailTemplate.updateWithMailTemplateDTO(dto);
		mailTemplate.saveToDb();
		
	}

	public MailTemplate getMailTemplate(int id) {
		return this.getMailTemplates().get(id);
	}
	
	public void  deleteRulesInfoFromController(int controller) throws OSS4HomeException
	{
		for (AutomationRule rule : this.getAutoRules().values()) {
			rule.deleteRulesInfoFromController(controller);
			if(rule.getActions().isEmpty()|| rule.getTriggers().isEmpty())
			{
				DbManager.getInstance().deleteAutoRule(rule.getId());
				autoRules.remove(rule.getId());
				
			}
		}
		
	}
	
	public void  deleteAutoActionFromMailTemplate(int mail) throws OSS4HomeException
	{
		for (AutomationRule rule : this.getAutoRules().values()) {
			rule.deleteAutoActionFromMailTemplate(mail);
			if(rule.getActions().isEmpty()|| rule.getTriggers().isEmpty())
			{
				DbManager.getInstance().deleteAutoRule(rule.getId());
				autoRules.remove(rule.getId());
				
			}
		}
		
	}
	
	public void  deleteAutoActionFromGroup(int group) throws OSS4HomeException
	{
		for (AutomationRule rule : this.getAutoRules().values()) {
			rule.deleteAutoActionFromGroup(group);
			if(rule.getActions().isEmpty()|| rule.getTriggers().isEmpty())
			{
				DbManager.getInstance().deleteAutoRule(rule.getId());
				autoRules.remove(rule.getId());
				
			}
		}
		
	}
	
	
}
