//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.business;

import java.security.GeneralSecurityException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;

import de.oss4home.cc.dto.AutoActionDTO;
import de.oss4home.cc.dto.AutomationTyp;
import de.oss4home.cc.dto.DeviceTyp;
import de.oss4home.cc.dto.DeviceValueDTO;
import de.oss4home.cc.dto.GroupValueDTO;
import de.oss4home.cc.dto.GroupValueDTO.GroupValue;
import de.oss4home.exceptions.OSS4HomeException;

/**
 * Class AutoAction
 */
@Entity
public class AutoAction {

	//
	// Fields
	//

	@Id
	@TableGenerator(name = "actionGen", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "actionGen")
	private int id;
	private AutomationTyp typ;
	/**
	 * Kann die ID des MailTempletes || ON || OFF||TOGGLE
	 */
	private String value;
	private int controllerId;
	private String deviceId;
	private int groupId;
	// private String name;

	@ManyToOne
	private AutomationRule rule;

	//
	// Constructors
	//
	public AutoAction() {
	};

	//
	// Methods
	//

	//
	// Accessor methods
	//

	/**
	 * @return the rule
	 */
	public AutomationRule getRule() {
		return rule;
	}

	/**
	 * @param rule the rule to set
	 */
	public void setRule(AutomationRule rule) {
		this.rule = rule;
	}

	/**
	 * Set the value of id
	 * 
	 * @param newVar
	 *            the new value of id
	 */
	public void setId(int newVar) {
		id = newVar;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		String name = "";
		try {
			switch (this.getTyp()) {
			case SENSOR:
			case SWITCHRELAIS:
				Device dev  = OSS4HomeManager.getInstance().getDevice(deviceId, new Integer(controllerId).toString());
				if (dev != null) {
						name = "Device: " + dev.getName();
				}
				break;
			case GROUP:
				Group gr = OSS4HomeManager.getInstance().getGroup(groupId);
				if(gr != null)
				{
					name = gr.getName();
				}
			break;
			case MAIL:
				name = "MAIL";
			break;
			case TIME:
				name = "Zeitsteuerung";
			break;
			}
		} catch (Exception ex) {

		}
		return name;
	}

	/**
	 * Get the value of id
	 * 
	 * @return the value of id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Set the value of typ
	 * 
	 * @param newVar
	 *            the new value of typ
	 */
	public void setTyp(AutomationTyp newVar) {
		typ = newVar;
	}

	/**
	 * Get the value of typ
	 * 
	 * @return the value of typ
	 */
	public AutomationTyp getTyp() {
		return typ;
	}

	/**
	 * Set the value of value Kann die ID des MailTempletes || ON || OFF||TOGGLE
	 * 
	 * @param newVar
	 *            the new value of value
	 */
	public void setValue(String newVar) {
		value = newVar;
	}

	/**
	 * Get the value of value Kann die ID des MailTempletes || ON || OFF||TOGGLE
	 * 
	 * @return the value of value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Set the value of controllerId
	 * 
	 * @param newVar
	 *            the new value of controllerId
	 */
	public void setControllerId(int newVar) {
		controllerId = newVar;
	}

	/**
	 * Get the value of controllerId
	 * 
	 * @return the value of controllerId
	 */
	public int getControllerId() {
		return controllerId;
	}

	/**
	 * Set the value of deviceId
	 * 
	 * @param newVar
	 *            the new value of deviceId
	 */
	public void setDeviceId(String newVar) {
		deviceId = newVar;
	}

	/**
	 * Get the value of deviceId
	 * 
	 * @return the value of deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}

	//
	// Other methods
	//

	/**
	 * Updated die AutoAction mit den Werten des DTOs
	 * 
	 * @param dto
	 */
	public void setFromAutoActionDTO(AutoActionDTO dto) {
		if (dto.getType() == AutomationTyp.SWITCHRELAIS || dto.getType() == AutomationTyp.SENSOR) {
			this.setControllerId(Integer.parseInt(dto.getControllerId()));
			this.setDeviceId(dto.getDeviceId());
		}

		if (dto.getType() == AutomationTyp.GROUP)
			this.setGroupId(Integer.parseInt(dto.getGroupId()));
		this.setTyp(dto.getType());
		this.setValue(dto.getValue());
		try {
			this.id = Integer.parseInt(dto.getId());
		} catch (Exception e) {
			// TODO: handle exception
		}
		// this.setName(dto.getName());

	}

	/**
	 * Gibt die AutoAction als AutoActionDTO zur�ck
	 * 
	 * @return
	 */
	public AutoActionDTO getAsAutoActionDTO() {
		AutoActionDTO dto = new AutoActionDTO();
		dto.setControllerId(new Integer(this.getControllerId()).toString());
		dto.setDeviceId(this.getDeviceId());
		dto.setId(new Integer(this.getId()).toString());
		dto.setType(this.getTyp());
		dto.setValue(this.getValue());
		dto.setName(this.getName());
		dto.setGroupId(new Integer(this.getGroupId()).toString());
		return dto;
	}

	/**
	 * Updated die AutoAction mit den werten des �bergebenen AutoAction. Wird
	 * vom DbManager verwendet.
	 * 
	 * @param updatedAction
	 */
	public void updateDbRelevantData(AutoAction updatedAction) {
		if (this.getId() == updatedAction.getId()) {
			this.setControllerId(updatedAction.getControllerId());
			this.setDeviceId(updatedAction.getDeviceId());
			this.setTyp(updatedAction.getTyp());
			this.setValue(updatedAction.getValue());
		}
	}

	public boolean doAction() {
		boolean bOK = true;
		int count = 0;
		do {
			try {
				switch (this.getTyp()) {
				case SWITCHRELAIS:
					this.relaisAction();
					break;
				case GROUP:
					this.groupAction();
					break;
				case MAIL:
					this.mailAction();
					break;
				default:
					break;
				}
				count++;
			} catch (Exception ex) {
				bOK = false;
			}
		} while (!bOK && count <= 3);
		return bOK;
	}

	private void mailAction() throws AddressException, NumberFormatException, MessagingException, GeneralSecurityException {
		MailManager.sendMail(OSS4HomeManager.getInstance().getSystemSettings()
				, OSS4HomeManager.getInstance().getMailTemplate(Integer.parseInt(this.value)));

	}

	private void groupAction() throws OSS4HomeException {
			GroupValueDTO dto = new GroupValueDTO();
			dto.setId(new Integer(this.getGroupId()).toString());
			if (this.getValue().equals(GroupValue.OFF.toString())) {
				dto.setValue(GroupValue.OFF);
			}
			else if(this.getValue().equals(GroupValue.TOGGLE.toString()))
			{
				dto.setValue(GroupValue.TOGGLE);
			}
			else {
				dto.setValue(GroupValue.ON);
			}
			OSS4HomeManager.getInstance().setGroupValue(dto);
		

	}

	/**
	 * Action f�r ein Relais
	 */
	private void relaisAction() throws OSS4HomeException {
			Device device = OSS4HomeManager.getInstance().getDevice(this.getDeviceId(),
					new Integer(this.getControllerId()).toString());
			DeviceValueDTO dto = new DeviceValueDTO();

			if (device != null && device.getTyp() == DeviceTyp.RELAIS
					&& device.isActive()) {
				if (!this.getValue().equals("TOGGLE")) {
					dto.setId(device.getId());
					dto.setValue(this.getValue());
					device.setValueFromClient(dto);
				} else {
					if (device.getValue().equals("ON")) {
						dto.setId(device.getId());
						dto.setValue("OFF");
						device.setValueFromClient(dto);
					} else if (device.getValue().equals("OFF")) {
						dto.setId(device.getId());
						dto.setValue("ON");
						device.setValueFromClient(dto);
					}
				}
			}

	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
}
