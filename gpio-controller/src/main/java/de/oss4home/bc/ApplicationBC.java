/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Dieses Programm ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiterverbreiten und/oder modifizieren.

    Dieses Programm wird in der Hoffnung, dass es nützlich sein wird, aber
    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Details.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package de.oss4home.bc;

import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import de.oss4home.bc.business.ControllerBusinessUnit;
import de.oss4home.ws.ControllerVersionWS;
import de.oss4home.ws.DevicesWS;
import de.oss4home.util.OSS4HomeResourceConfig;
import de.oss4home.util.Webserver;



public class ApplicationBC {

	private static final URI BASE_URI = URI.create("http://0.0.0.0:5550/");

	
    /*
     * Main Funktion des 1-Wire-Controllers
     */
	public static void main(String[] args) {
		
		boolean stop = false;
		
		
		/*
		 * Configuration Init
		 */
		
		/*
		 * Thread zu abruf der Daten der Daten
		 */
		
			ControllerBusinessUnit.getInstance().configSync();
		
		

		
	
		
		/*Webserver Konfigurieren und Starten*/
		Webserver webserver = new Webserver(BASE_URI, initWebservice());
		try {
			webserver.start();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		/*Gewollte Endlosschleife*/
		while(!stop)
		{
			try {
				ControllerBusinessUnit.getInstance().sync();
				Thread.sleep(5);
			} catch (InterruptedException e) {
			
			}
	
		}
		


	}
	
	/*
	 * Konfiguration des Webservers
	 * Hinzufügen der einzelnen Webservices
	 * */
	private static OSS4HomeResourceConfig initWebservice()
	{
		Set<Class<?>> webservices = new HashSet<Class<?>>();
		webservices.add(ControllerVersionWS.class);
		webservices.add(DevicesWS.class);
		return OSS4HomeResourceConfig.init(webservices);
	}

}
