package de.oss4home.bc.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import com.pi4j.io.gpio.*;
import com.pi4j.system.SystemInfo;
import com.pi4j.system.SystemInfo.BoardType;

import de.oss4home.bc.Interfaces.IDeviceConfig;
import de.oss4home.bc.Interfaces.IDeviceValue;
import de.oss4home.bc.dto.BcDeviceConfigDTO;
import de.oss4home.cc.dto.DeviceTyp;
import de.oss4home.exceptions.OSS4HomeException;

public class GPIOController {
	
	private IDeviceConfig configContainer = null;
	private IDeviceValue valueContainer = null;
	private GpioController gpio = null;
	private String mainDevice = "";
	BoardType  board = null;
	GpioProvider gpioProvider = null;
	PinProvider pinProvider = null;

	
	public GPIOController(IDeviceValue valueContainer, IDeviceConfig configContainer) throws UnsupportedOperationException, IOException, InterruptedException, OSS4HomeException {
		this.configContainer = configContainer;
		this.valueContainer = valueContainer;
		board = SystemInfo.getBoardType();
		
		switch(board)
		{
		case BananaPi:
			pinProvider = new BananaPiPin();
			gpioProvider = new BananaPiGpioProvider();
		break;
		case BananaPro:
			pinProvider = new BananaProPin();
			gpioProvider = new BananaProGpioProvider();
		break;	
		case RaspberryPi_2B:
		case RaspberryPi_3B:
		case RaspberryPi_A:
		case RaspberryPi_A_Plus:
		case RaspberryPi_Alpha:
		case RaspberryPi_B_Plus:
		case RaspberryPi_B_Rev1:
		case RaspberryPi_B_Rev2:
		case RaspberryPi_ComputeModule:
		case RaspberryPi_Zero:
			gpioProvider = new RaspiGpioProvider();
			pinProvider = new RaspiPin();
		break;
		default:
			throw new OSS4HomeException("Board not Supported");
		}
		
	}
	
	public void deviceConfig()
	{
	
		
		Pin[] pins =PinProvider.allPins(PinMode.DIGITAL_INPUT,PinMode.DIGITAL_OUTPUT);
		for (Pin pin : pins) {
			BcDeviceConfigDTO config =configContainer.getDeviceConfig(String.valueOf(pin.getAddress()));
			if(config == null)
			{
				config = new BcDeviceConfigDTO();
				config.setId(String.valueOf(pin.getAddress()));
				config.setActive(false);
				//TODO: Aufboren auf possible States
				if(pin.getSupportedPinModes().contains(PinMode.DIGITAL_OUTPUT))
				{
					config.setType(DeviceTyp.RELAIS);
				}
				else
				{
					config.setType(DeviceTyp.SWITCH);
				}
			}
			
			
		}
		
		
		
	}

}
