/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Dieses Programm ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiterverbreiten und/oder modifizieren.

    Dieses Programm wird in der Hoffnung, dass es nützlich sein wird, aber
    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Details.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */
package de.oss4home.bc.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.xml.bind.annotation.*;

import de.oss4home.bc.Interfaces.IDeviceConfig;
import de.oss4home.bc.dto.BcDeviceConfigDTO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SystemDeviceConfigController implements IDeviceConfig,Serializable {
	
	

	private static final long serialVersionUID = 329892001172824983L;
	
		
	@XmlElement
	private List<BcDeviceConfigDTO> devices = new ArrayList<BcDeviceConfigDTO>();
	
	private SystemDeviceConfigController()
	{
		
	}
	
	
	public static SystemDeviceConfigController loadFromFile()
	{
		SystemDeviceConfigController instance = null;
		if(instance == null)
		{
			synchronized (SystemDeviceConfigController.class) {
				try {
					instance = load();
				} catch (FileNotFoundException e) {
	
					e.printStackTrace();
				} catch (JAXBException e) {

				}
				if(instance == null)
					instance = new SystemDeviceConfigController();
				
			}
		}
		return instance;
	}

	public List<BcDeviceConfigDTO> getDevices() {
		//Damit die Orginal Liste nicht verändert werden kann
		//sondern nur die Werte der Elemente
		return new Vector<BcDeviceConfigDTO>(devices);
	}
	
	
	
	public void save() throws FileNotFoundException, JAXBException
	{
		OutputStream out = new FileOutputStream("SystemDeviceInfoController.xml"); 
		OutputStreamWriter writer = new OutputStreamWriter(out);
		JAXBContext context = JAXBContext.newInstance(SystemDeviceConfigController.class);
		Marshaller m = context.createMarshaller();
		m.marshal(this, writer);
	}
	
	
	private static SystemDeviceConfigController load() throws JAXBException, FileNotFoundException
	{
		InputStream in = new FileInputStream("SystemDeviceInfoController.xml"); 
		InputStreamReader input = new InputStreamReader(in);
		JAXBContext context = JAXBContext.newInstance(SystemDeviceConfigController.class);
		Unmarshaller m = context.createUnmarshaller();
		return (SystemDeviceConfigController)m.unmarshal(input);
		
	}


	public void addUpdateDeviceConfig(BcDeviceConfigDTO config) {
		//Gibt es das Element schon, dann nur Updaten
		BcDeviceConfigDTO deviceThis = getDeviceConfig(config.getId());
		if(deviceThis  != null) 
		{
			//Nur wenn es nicht das gleiche Object ist
			if(deviceThis != config)
			{
				try {
					deviceThis.setActive(config.getActive());
					deviceThis.setType(config.getType());
				} catch (Exception e) {
					// TODO Auto-generated catch block
				}
			}
		}
		else
		{
			devices.add(config);
		}
		try {
			save();
		} catch (FileNotFoundException e) {
			// TODO Logging
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Logging
		}
		
		
	}


	public List<BcDeviceConfigDTO> getAllDevicesConfig() {
		//Damit die Orginal Liste nicht verändert werden kann
		//sondern nur die Werte der Elemente
		return new Vector<BcDeviceConfigDTO>(devices);
	}


	public BcDeviceConfigDTO getDeviceConfig(String Id) {
		Iterator<BcDeviceConfigDTO> iter = devices.iterator();
		while(iter.hasNext())
		{
			BcDeviceConfigDTO cur = (BcDeviceConfigDTO) iter.next();
			 if(cur.getId().equals(Id))
				 return cur;
		}
		return null;
	}

	

}
