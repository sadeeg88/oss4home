/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Dieses Programm ist Freie Software: Sie k�nnen es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    ver�ffentlichten Version, weiterverbreiten und/oder modifizieren.

    Dieses Programm wird in der Hoffnung, dass es n�tzlich sein wird, aber
    OHNE JEDE GEW�HRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gew�hrleistung der MARKTF�HIGKEIT oder EIGNUNG F�R EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License f�r weitere Details.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package de.oss4home.bc.business;

import java.util.List;

import org.apache.logging.log4j.Logger;


import de.oss4home.bc.data.DeviceValue;
import de.oss4home.bc.data.SystemDeviceConfigController;
import de.oss4home.bc.dto.BcDeviceConfigDTO;
import de.oss4home.bc.dto.BcDeviceValueDTO;
import de.oss4home.cc.dto.DeviceTyp;

/**
 * Steuerungsklasse  (Singelton) f�r den One-Wire-Controller
 * @author Sascha Deeg
 *
 */
public class ControllerBusinessUnit {
	
	private  Logger logger = null;
	
	//Ben�tigte Objekte
	//private OneWireController oneWireController = null;
	private SystemDeviceConfigController systemDeviceConfigController = null;
	private DeviceValue deviceValue = null;
	
	//Singelton Instance
	private static ControllerBusinessUnit instance = null;
	
	/*
	 * Get Funktion f�r die Singelton Instanz
	 */
	public static ControllerBusinessUnit getInstance()
	{
		if(instance == null)
		{
			synchronized (ControllerBusinessUnit.class) {
					instance = new ControllerBusinessUnit();
				
			}
		}
		return instance;
	}
	
	private ControllerBusinessUnit()
	{
	    try {
	    	logger = org.apache.logging.log4j.LogManager.getRootLogger();
	      } catch( Exception ex ) {
	        System.out.println( ex );
	      }
		deviceValue = new DeviceValue();
		systemDeviceConfigController = SystemDeviceConfigController.loadFromFile();
		//oneWireController = new OneWireController(deviceValue, systemDeviceConfigController);
		
	}
	
	/*
	 * Aufruf um die Daten von der Hardware abzufragen
	 */
	public void ControllerSync()
	{
		//oneWireController.sync();
	}
	
	
	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/*
	 * Ermittelt die Werte der aktiven Devices
	 * @return Die Werte der Devices
	 */
	public List<BcDeviceValueDTO> getDevicesAllValues()
	{
		return deviceValue.getAllValues();
	}
	
	/*
	 * Ermittelt die Konfiguration der Devices
	 * @return Die Konfiguration der Devices
	 */
	public List<BcDeviceConfigDTO> getAllDevicesConfig()
	{
		return systemDeviceConfigController.getAllDevicesConfig();
	}
	
	
	public BcDeviceConfigDTO getDeviceConfig(String Id)
	{
		return systemDeviceConfigController.getDeviceConfig(Id);
	}
	
	
	/*
	 * Ermittelt neue Devices vom 1-Wire-Controller
	 * und Syncronisiert diese mit der Devicekonfiguration
	 */
	public void configSync()
	{
		//oneWireController.deviceKonfiguration();
		//oneWireController.initDevices();
	}
	
	public void sync()
	{
		//oneWireController.sync();
	}
	
	
	public void setDevice(BcDeviceValueDTO value)
	{
		//oneWireController.setValue(value);
	}
	
	public void setConfig(BcDeviceConfigDTO config)
	{
		systemDeviceConfigController.addUpdateDeviceConfig(config);
		if(config.getType() == DeviceTyp.SWITCH)
		{
			//oneWireController.initDevice(config);
		}
	}
	
	public void InitDevices()
	{
		//oneWireController.initDevices();
	}
	


}
