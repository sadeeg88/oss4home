/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Dieses Programm ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiterverbreiten und/oder modifizieren.

    Dieses Programm wird in der Hoffnung, dass es nützlich sein wird, aber
    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Details.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */
package de.oss4home.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.oss4home.bc.business.ControllerBusinessUnit;
import de.oss4home.bc.dto.BcDeviceConfigDTO;
import de.oss4home.bc.dto.BcDeviceValueDTO;
import de.oss4home.bc.ws.validation.*;
import de.oss4home.controller.interfaces.IControllerDevices;


@Path("devices")
public class DevicesWS implements IControllerDevices {

	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("config")
	public List<BcDeviceConfigDTO> getDeviceConfig() {
		return ControllerBusinessUnit.getInstance().getAllDevicesConfig();
	}

	@PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("config")
	public Response updateDeviceConfig(BcDeviceConfigDTO data) {
	     Validation validation = new Validation();
	     
	     if(!validation.isUpdateDeviceConfigValid(data))
	     {
	    	 
	    	 return Response.notAcceptable(null).build();
	     }
	     
	     ControllerBusinessUnit.getInstance().setConfig(data);
	     
	     return Response.ok().build();
	}
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("values")
	public List<BcDeviceValueDTO> getDeviceValue() {
		return ControllerBusinessUnit.getInstance().getDevicesAllValues();
	}

	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("values")
	public Response setDeviceValue(BcDeviceValueDTO value) {
	    Validation validation = new Validation();
	     if(!validation.isUpdateDeviceValueValid(value))
	     {
	    	 return Response.notAcceptable(null).build();
	     }
	     
	     try
	     {
	    	 ControllerBusinessUnit.getInstance().setDevice(value);
	    	 return Response.ok().build();
	     }
	     catch(Exception ex)
	     {
	    	 return Response.serverError().build();
	     }
	     
	}
	@PUT
	@Path("syncConfig")
	public Response syncDeviceConfig()
	{
		ControllerBusinessUnit.getInstance().configSync();
		return Response.ok().build();
	}


}
