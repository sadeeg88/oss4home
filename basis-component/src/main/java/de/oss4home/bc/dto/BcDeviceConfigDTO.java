//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.bc.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.*;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.oss4home.cc.dto.DeviceTyp;


@XmlRootElement(name="Device")
@XmlAccessorType(XmlAccessType.FIELD)
public class BcDeviceConfigDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5481536489393911477L;

	@XmlElement
	@JsonProperty
	private String id;
	
	@XmlElement
	@JsonProperty
	private DeviceTyp type;
	
	
	//@XmlElement
	//@JsonProperty
	//private Boolean activitySensing = true;
	
	@XmlElement
	@JsonProperty
	private Boolean connected = true;
	
	
	@XmlElement
	@JsonProperty
	private Boolean  active = true;
	
	
	public BcDeviceConfigDTO()
	{
		
	}
	


	
	public String getId() {
		return id;
	}
	public DeviceTyp getType() {
		return type;
	}
	public void setType(DeviceTyp type) {
		this.type = type;
	}


	
	public Boolean getActive() {
		return  active;
	}

	public void setActive(Boolean active) {
		this. active =  active;
	}



//	/**
//	 * @return the sensed
//	 */
//	public Boolean getActivitySensing() {
//		return activitySensing;
//	}
//
//
//
//	/**
//	 * @param sensed the sensed to set
//	 */
//	public void setActivitySensing(Boolean sensed) {
//		this.activitySensing = sensed;
//	}



	/**
	 * @return the connected
	 */
	public Boolean getConnected() {
		return connected;
	}



	/**
	 * @param connected the connected to set
	 */
	public void setConnected(Boolean connected) {
		this.connected = connected;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

}
