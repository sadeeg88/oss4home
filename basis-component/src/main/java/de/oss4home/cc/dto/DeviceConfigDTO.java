//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.cc.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.*;

import com.fasterxml.jackson.annotation.JsonProperty;


@XmlRootElement(name="Device")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeviceConfigDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1680815923009240555L;

	@XmlElement
	@JsonProperty
	private String id;
	
	@XmlElement
	@JsonProperty
	private DeviceTyp type;
	
	@XmlElement
	@JsonProperty
	private String calculationFormula = "x";
	
	@XmlElement
	@JsonProperty
	private String name;
	
	@XmlElement
	@JsonProperty
	private String unit;
	
	@XmlElement
	@JsonProperty
	private Boolean  active = true;
	
	@XmlElement
	@JsonProperty
	private String controllerId;
	
	public DeviceConfigDTO()
	{
		
	}
	
	public DeviceConfigDTO(String Id, DeviceTyp type) throws Exception
	{
	   this.id = Id;
	   this.type = type;
	   
	}
	
	public void copy(DeviceConfigDTO input) throws Exception
	{
		if(input.id != id)
			throw new Exception("Not Allowed");
		
		this.setActive(input.getActive());
		this.setCalculationFormula(input.calculationFormula);
		this.setType(getType());

				
	}
	
	public String getId() {
		return id;
	}
	public DeviceTyp getType() {
		return type;
	}
	public void setType(DeviceTyp type) {
		 
		this.type = type;
	}
	public String getCalculationFormula() {
		return calculationFormula;
	}
	public void setCalculationFormula(String calculationFormula) {
		this.calculationFormula = calculationFormula;
	}

	
	public Boolean getActive() {
		return  active;
	}

	public void setActive(Boolean active) {
		this. active =  active;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the einheit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param unit the einheit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	public String getControllerId() {
		return controllerId;
	}

	public void setControllerId(String controllerId) {
		this.controllerId = controllerId;
	}
	
	
}
