//--------------------------------------------------------------------------------------------------------------------
//Open Source Smart for Home (OSS4Home)
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg, Kim Janek Triebig, Michael Etter
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------

package de.oss4home.controller.interfaces;

import java.util.List;



import javax.ws.rs.core.Response;

import de.oss4home.bc.dto.BcDeviceConfigDTO;
import de.oss4home.bc.dto.BcDeviceValueDTO;

//@Path("devices")
public interface IControllerDevices {
	
	
	
	/*
	 * Die Funktion soll die Werte der Sensoren und Relais und Schaltern liefern
	 * aber nicht Trigger (Taster)
	 */
	//@GET
	//@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	//@Path("Values")
	public List<BcDeviceValueDTO> getDeviceValue();
	
	
	//@PUT
   //@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML}
   //@Path("Values")
	public Response setDeviceValue(BcDeviceValueDTO value);
	
	
	
	
	/*
	 * Die Funktion soll die Konfigurationsdaten der Devices liefern.
	 * Siehe SystemDeviceInfoContoller.getInstance().getDevices()
	 */
	//@GET
	//@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	//@Path("Config")
	public List<BcDeviceConfigDTO> getDeviceConfig();
	
	/*
	 * Die Funktion soll die Konfigurationsdaten der Devices �ndern.
	 * Siehe SystemDeviceInfoContoller.getInstance().addUpdateDevice(DeviceInfo device)
	 *  Pr�fe ob die ID schon exsistiert sonst fehler... getDevice(String) wenn null dann Fehler  
	 */
	//@PUT
	//@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	//@Path("Config")
	public Response updateDeviceConfig(BcDeviceConfigDTO data);
	
	/*
	 * An der Hardware ermitteln ob neue Devices angeschlossen wurden.
	 */
	//@PUT
	//@Path("syncConfig")
	public Response syncDeviceConfig();

	

}
